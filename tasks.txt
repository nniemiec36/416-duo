header - Nayan/Alexa
    – toggle box 
        - demographic map 
        - election results map (default)
    - legend
        - corresponds to which map is selected
body
    - map 
        - need census block geometries - Nicole
            - preprocessingblocks.py
        - table layout -- Nitin
            - skeleton (model after ARS)
            - map takes whole screen
            - table is like on the map (maybe transparent)
general 
    - need to zoom out on map a bit // done
    - reset/refresh button
    - demographic data - Nicole
    - Nitin's UI -- see if we can incorporate something
    - meet at 4-5pm tomorrow

9/13 tasks

header – 
    – toggle for district boundaries – Alexa
    – legend -> add red/blue meaning
    - refresh - Nicole
        – get rid of table (not legend) and zoom out back to 3, center,
        – toggles would be set back to default
body 
    – map
        – table overlay – Nitin

general 
    – demographic data - Nayan 
    - library @5 tomorrow

9/15 tasks
    - need a "leave state" button that zooms back out and changes all visibility of precincts, districts, etc. to 'none' (https://docs.mapbox.com/mapbox-gl-js/example/change-building-color-based-on-zoom-level/)
    – need to figure out why legend stays but table does not
    – need to figure out table
    - need to get one districting plan loaded
    - need another sidebar with adjustable sliders to generate plans 
    - need to show the plan on the map
    

9/18 tasks 
    – get refresh button working – Alexa – Done
    – fix toggle bug for boundaries – Alexa – Done
    – implement hard coded district results – Nicole 
    - add info for district/redistricting plan tabs – Nayan
        – get rid of precincts/county tabs – Nayan 
    - look into a loading screen – Last 
    - get panels to toggle on and off 
    - figure out the data measures – Tuesday/Wednesday office hours problem


9/19 tasks
    – toggle between district and election results – Nicole - Done
    - add info for district/redistricting results – Nayan 
        - figure out what columns/data should be shown
    - look into loading screen – Nitin
        – state tables – Nitin
    - after generating, look into replacing map with new redistricting plan – Alexa
        - and the table list of different plans
    - figure out the  data measures  – office hours
    - get table to toggle on and off 

9/20 tasks
    - add info for district/redistricting - Nayan & Nicole
    - loading screen animation - Nitin
    - replacing map after generating new redistricting plan - Alexa
    - figure out the data measures – office hours – Nayan & Alexa
    - get table to toggle on and off - Nayan 
    – refresh button bug – Nicole

9/21 tasks
    - replacing map after generating new redistricting plan - Alexa
    - figure out the data measures
    - get table to toggle on and off - Nayan

    Efficiency Gap, Polsby Popper, Equal Population (are these the data measures?)

9/22 tasks
    - district table for nc and pa - DONE
    - generate button redesign
    - loading page
    - get table to toggle on and off
    - talk about data measures
    - add congressional election data
        - can't find 2020 pa congressional election data

10/17 tasks
    - gui
        - condense and move functions out of map.js - Alexa and Nicole
    - design review (10/26 - 11/1)
        - activity diagrams
        - sequence diagrams
        - OO (class diagrams)
        - set up the DB (MySQL) - Nayan
        - download the modified python code and look into how we can adjust it - Nitin
        - figure out how we can preprocess the data for the seawulf (and enter it into the DB)
        - how do we want to set up the tables in the DB and how do the tables all map to each other? - Nayan

10/18 tasks
    - gui
         - condense and move functions out of map.js - Alexa and Nicole
    - design review (10/26 - 11/1)
        - activity diagrams –
        - sequence diagrams – Nitin and Nayan
            – sequence diagram for "getTableData()"
            - https://www3.cs.stonybrook.edu/~cse416/Section01/Sample-Sequence%20Diagram-Section%201.jpg
        - OO (class diagrams)
        - figure out how we can preprocess the data for the seawulf (and enter it into the DB)
        - how do we want to set up the tables in the DB and how do the tables all map to each other? - Nayan

11/3 tasks
    - 11/7 final design document
        – need to adjust sequence diagrams from our review – Nitin
            – Server 5, Server 7
        - need to adjust activity diagrams (?) – Nicole & Nitin
            - need to add activity diagram for preprocessing
            - prepro 1, preproc 2
        - need to make a relational data model – Alexa & Nayan
    - 11/14 first build
        - need to implement some of the server code
        - needs to include all our GUI and most domain objects
            - many of the methods can be stubbed
        - need to adjust the GUI for sequence diagrams 
    - need to look into the java library dr. kelly was talking about

11/4 tasks
    - 11/7 final design document
        - adjust lingering sequence diagrams (convex hull to java library) - Nicole
        – finish seawulf activity diagrams - Nitin
        - finish last 2 preprocessing diagrams - Nicole
    - 11/14 first build
        - implementation of sequence diagrams on server code - Nitin & Nayan
        - implementation of python preprocessing scripts - Nicole & Alexa
        - implementation of database tables - Alexa


11/13 tasks
    – GUI - Nayan 
        - Population dropdown w population variable
        - another tab for Box and Whisker data
        - another tab for District info
        --> - pop up for alg progress
    - Client/Server connection
    - preprocessing
    - server code implementation


        - state table - district plan table - district info - compare plan table - box and whisker
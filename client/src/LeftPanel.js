import React from "react";
import './LeftPanel.css';
import $ from 'jquery';
import {timer} from './PopUp'
import { currentState, seawulfPops,chosenPlan, map, setChosenPlan,setCurrentLayerID,getLayer,addGeoJSONLayer,currentLayerID, generatedDistrictsData, enactedDistrictsData, chosenDistrictsData } from "./Map";
import { fillComparisonTable, fillDistrictLoop } from './SideTable'
import { updateCurrentPlanText } from './Header'
// prevents the user from manually entering a number > max or < min (+-10 of given)
$(function () {
    $('.panelInput').change(function() {
        var max = parseFloat($(this).attr('max'));
        var min = parseFloat($(this).attr('min'));
        if ($(this).val() > max) {
            $(this).val(max);
        } else if ($(this).val() < min) {
            $(this).val(min);
        } 
    });      
}); 

function algoPlanOnMap(){
    var plan =  currentState.toUpperCase() +chosenPlan+"M";

    const url = "http://localhost:8080/states/getDistrictPlanForDisplay/" + currentState.toString().toUpperCase()
                + "/" + plan;

    const url2 = "http://localhost:8080/states/getDistTableTest/" + currentState.toString().toUpperCase()
    + "/" + plan;
    var genPopEq;
    var targetPopEq;
    var startPopEqs;
    var startPopEq;
    var popEqMeasures;
    fetch(url2)
    .then(res => res.json())
    .then(logged => {
        console.log(logged);
        Object.assign(generatedDistrictsData, logged);
        console.log("setting generatedDistrictsData")
        console.log(generatedDistrictsData)
    });
    
    console.log(url);
    fetch(url)
    .then(res => res.json())
    .then(logged => {
        const modal = document.getElementById("pop-up")
        const closeBtn = document.getElementById("popupclose")
        modal.style.display = "block";
        closeBtn.addEventListener("click", () => {
          modal.style.display = "none";
        })
        console.log("fetch return");
        console.log(logged)
        var geojson = logged['geojson'];
        console.log(geojson['percents']);
        var algoPops=[];
        if(currentState==='ga'){
            algoPops = [[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
        }else{
            algoPops = [[],[],[],[],[],[],[],[],[],[],[],[],[]];
        }
        console.log(generatedDistrictsData);
        // var genPopEq = Object.values(generatedDistrictsData);
        // // var targetPopEq = genPopEq[0]['districtPlan']['equalPopMeasure'];
        // var startPopEqs = Object.values(chosenDistrictsData);
        // var startPopEq = startPopEqs[0]['districtPlan']['equalPopMeasure'];
        // genPopEq = Object.values(generatedDistrictsData);
        console.log(logged);
        var obj = Object.values(logged);
        targetPopEq = logged[plan]['equalPopMeasure'];
        startPopEqs = Object.values(chosenDistrictsData);
        startPopEq = startPopEqs[0]['districtPlan']['equalPopMeasure'];
        console.log(targetPopEq)
        console.log(startPopEq)
        var step = ((targetPopEq - startPopEq) / 500.0)
        popEqMeasures = [(startPopEq + (0*step)), (startPopEq + (1*step)), (startPopEq + (2*step)), (startPopEq + (3*step)), (startPopEq + (4*step)), (startPopEq + (5*step)), targetPopEq];
        console.log(popEqMeasures);
        
        for(var i =0;i<geojson['features'].length;i++){
            var dist = geojson['features'][i]['properties']['DistrictID'];
            dist = dist -1;
            var pop = geojson['features'][i]['properties']['TOTALPOP'];
            algoPops[dist]+=pop;
        }
        console.log("POPULATIONS "+algoPops);
        console.log("SEAWULF POP "+seawulfPops);
        timer(seawulfPops,algoPops, popEqMeasures);
        //turn off currentLayer
        console.log(currentLayerID);
        document.getElementById('chosen-plan').innerText = "Generating Plan ... ";

        setTimeout(() => {
            console.log("TURNING OFF "+currentLayerID);
            map.current.setLayoutProperty(currentLayerID, 'visibility', 'none');
            map.current.setLayoutProperty(currentLayerID+"_map", 'visibility', 'none');
            
            if(getLayer(currentState+'_plan_'+plan+"M")){
                console.log("exists");
                map.current.setLayoutProperty(currentState+'_plan_'+plan+"M", 'visibility', 'visible');
                map.current.setLayoutProperty(currentState+'_plan_'+plan+"M_map", 'visibility', 'visible');
            }else{
                addGeoJSONLayer('ga', currentState+'_plan_'+plan+"M", 'line', currentState+'_counties_elections_outline', 'district', 'district', geojson, 'visible', 6, 15);
                addGeoJSONLayer('ga', currentState+'_plan_'+plan+"M_map", 'fill', currentState+'_plan_'+plan+"M", 'district', 'redistrColors', geojson, 'visible', 6, 15);    
            }
            setCurrentLayerID(currentState+'_plan_'+plan+"M");
            updateCurrentPlanText("Plan: " + plan)
            setChosenPlan(chosenPlan+"M");
            console.log(currentLayerID);
            fillDistrictLoop(generatedDistrictsData);
            console.log("GENRATED "+generatedDistrictsData)
            console.log(enactedDistrictsData)
            console.log("CHOSEN "+chosenDistrictsData)
            fillComparisonTable();
            document.getElementById('chosen-plan').innerText = "New Plan Generated!";
        },  3000*7);
        
        // fillDistrictLoop(logged);
    });
}

// closes the left panel
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

// created the left panel component
const LeftPanel = () => {
  return (
    <div id="mySidenav" class="sidenav">
        
        <a href="javascript:void(0)" class="closebtn" onClick={closeNav} >&times;</a>
        <br/>
        <a id='chosen-plan'>Please Choose a Plan</a>
        <div class = "slidecontainer" >
            <div>
                <p>Population Equality:</p>
                <input class="panelInput" type="number"  placeholder="0" step="0.01" id='pop-eq-input' ></input>
            </div>
        </div>
        
        <button id = "generate" type="button" class="generate_button" onClick={algoPlanOnMap}>
            <span class="button__text">Start Algorithm</span>
        </button>

        <p id="iterations" class="algMeasure"></p>
        <p id="popEqAlg" class="algMeasure"></p>
        <p id="runTime" class="algMeasure"></p>

      </div>        
  );
};

export default LeftPanel;
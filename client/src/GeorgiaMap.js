import 'mapbox-gl/dist/mapbox-gl.css';
import './Map.css';
import georgiaCounties from './data/georgia/Original Data Files/georgias-counties.geojson';
import georgiaPrecinct from "./data/georgia/processed/GA_PRECINCTS_ALL_INFO.geojson";
import georgiaDistrict from "./data/georgia/Original Data Files/ga_cd.geojson";
import georgiaRedist1 from "./data/redistrictings/georgia/redistr1.geojson";
import georgiaRedist2 from "./data/redistrictings/georgia/redistr2.geojson";
import georgiaRedist3 from "./data/redistrictings/georgia/redistr3.geojson";
import { map, districtView, precinctView, countyView, addGeoJSONLayer, currentState, currentLayerID, setCurrentLayerID } from './Map';
import { ncLayers } from './NCMap';
import {georgiaDistrictLayers} from './Constants';
import { updateCurrentPlanText } from './Header';


export var gaLayers = [
  'ga_counties_elections_outline',
  'ga_counties_elections',
  'ga_precincts',
  'ga_congressional_districts',
  'ga_congressional_districts_map',
  'ga_congress_2020',
  'ga_redistricting_1',
  'ga_redistricting_1_map',
  'ga_redistricting_2',
  'ga_redistricting_2_map',
  'ga_redistricting_3',
  'ga_redistricting_3_map'];

export function openGAMap() {
  addGeoJSONLayer('ga', 'ga_counties_elections_outline', 'line', 'us_states_elections', 'county', 'election', georgiaCounties, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_counties_elections', 'fill', 'ga_counties_elections_outline', 'county', 'election', georgiaCounties, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_precincts', 'line', 'ga_counties_elections_outline', 'precinct', 'election', georgiaPrecinct, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_congressional_districts', 'line', 'ga_counties_elections_outline', 'district', 'election', georgiaDistrict, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_congressional_districts_map', 'fill', 'ga_congressional_districts', 'district', 'districtColors', georgiaDistrict, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_congress_2020', 'fill', 'ga_congressional_districts', 'district', 'cElection', georgiaDistrict, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_redistricting_1', 'line', 'ga_counties_elections_outline', 'district', 'district', georgiaRedist1, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_redistricting_1_map', 'fill', 'ga_redistricting_1', 'district', 'redistrColors', georgiaRedist1, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_redistricting_2', 'line', 'ga_counties_elections_outline', 'district', 'district', georgiaRedist2, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_redistricting_2_map', 'fill', 'ga_redistricting_2', 'district', 'redistrColors', georgiaRedist2, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_redistricting_3', 'line', 'ga_counties_elections_outline', 'district', 'district', georgiaRedist3, 'none', 6, 15);
  addGeoJSONLayer('ga', 'ga_redistricting_3_map', 'fill', 'ga_redistricting_3', 'district', 'redistrColors', georgiaRedist3, 'none', 6, 15);
  console.log(currentLayerID);
}

export function openGeorgia(mapOption) {
  setCurrentLayerID("ga_congressional_districts");
  console.log("id "+currentLayerID);
  const url = "http://localhost:8080/states/getState/" + currentState;
  fetch(url)
    .then(res => res.json())
    .then(logged => {
      // map.addSource('ga_precincts', {
      //   type: 'geojson',
      //   data: logged['precincts']
      //   });
      console.log(logged['precincts']);
      console.log(logged['districts']);
    });
    updateCurrentPlanText('Plan: GA0');
    updateGeorgiaLayers(georgiaDistrictLayers);
}

export function updateGeorgiaLayers(georgia){
  const filteredArray = gaLayers.filter(function (x) {
    return georgia.indexOf(x) < 0;
  });
  var others = ncLayers;
  others = others.concat(filteredArray);
  for (const id of others) {
    map.current.setLayoutProperty(id, 'visibility', 'none');
  }
  for (const id of georgia) {
    map.current.setLayoutProperty(
      id, 'visibility', 'visible'
    );
  }
  if (districtView === false) {
    map.current.setLayoutProperty(
      'ga_congressional_districts', 'visibility', 'none'
    );
  }
  if(precinctView === false) {
    map.current.setLayoutProperty(
      'ga_precincts', 'visibility', 'none'
    );
  }
  if(countyView === false) {
    map.current.setLayoutProperty(
      'ga_counties_elections_outline', 'visibility', 'none'
    );
  }
}
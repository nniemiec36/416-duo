import React from 'react';
import './SideTable.css';
import {map, currentState, setSeawulfPops,setInputs,populationVar, currentPlanID, chosenPlan, getLayer,addGeoJSONLayer,currentLayerID,setCurrentLayerID, currentPlanData, currentDistrictsData, setChosenPlan,generatedDistrictsData, generatedPlanData, setSeawulfPop, enactedPlanData, enactedDistrictsData, chosenDistrictsData, chosenPlanData} from './Map';
import Plotly from 'plotly.js-dist';
import gaBoxData from '../src/data/gaBoxWhisk.json'

import ncBoxData from '../src/data/ncBoxWhisk.json'
import { testSetGenerate, testGetGenerate, testUserParameters } from './FetchCalls';
import fs from 'fs';
import JSONStream from 'JSONStream';
import es from "event-stream";

import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { updateDownloadPath, updateCurrentPlanText } from './Header';

export var currentPlanPath = './data/georgia/plans/district-plan-ga0.geojson';

export function openTable() {
    document.getElementById("table").style.background = "rgba(255, 255, 255, 0.3)";
    document.getElementById('tableLinks').style.display = 'block';
    document.getElementById('State').style.display = 'block';
    document.getElementById('District').style.display = 'none';
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'none';
    
    document.getElementById('boxPlotContainer').style.display = 'none';
    //fillTable();
    // if(currentPlanData === null){
    //     fillTable();
    // }
    fillTable();
    // testing();
    // testingPost();
    setPopulationVariable();
    console.log("CURRENT PLAN IS "+chosenPlan);
    console.log("CURRENT LAYER IS "+currentLayerID);
    console.log(currentDistrictsData);
    console.log(currentPlanData);
    if(Object.keys(currentPlanData).length === 0){
        getPlanTable();
    } else {
        fillPlanTable(currentPlanData)
    }
    if(Object.keys(currentDistrictsData).length === 0){
        fillDistrictTable();
    } else {
        fillDistrictLoop(currentDistrictsData)
    }
}

export function closeTable() {
    document.getElementById("table").style.background = "rgba(255, 255, 255, 0)";
    document.getElementById('tableLinks').style.display = 'none';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'none';
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'none';
    document.getElementById('boxPlotContainer').style.display = 'none';
}

export function openStateTable() {
    document.getElementById('State').style.display = 'block';
    document.getElementById('District').style.display = 'none';
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'none';
    document.getElementById('boxPlotContainer').style.display = 'none';
    fillTable();
}

export function openBoxPlot() {
    document.getElementById('boxPlotContainer').style.display = 'block';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'none';
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'none';
    boxPlot();
    testSetGenerate();
    testUserParameters();
}

export function openDistrictTable() {
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'block';
    document.getElementById('DistrictInfo').style.display = 'none';
    document.getElementById('boxPlotContainer').style.display = 'none';
    console.log(currentPlanData)
    if(Object.keys(currentPlanData).length === 0){
        getPlanTable();
    } else {
        fillPlanTable(currentPlanData)
    }
    // getPlanTable();
    //testGetGenerate();
}

export function openDistrictInfo() {
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'block';
    document.getElementById('boxPlotContainer').style.display = 'none';
    if(Object.keys(currentDistrictsData).length === 0){
        fillDistrictTable();
    } else {
        fillDistrictLoop(currentDistrictsData)
    }
}
  
export function openComparisonTable() {
    document.getElementById('Comparison').style.display = 'block';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'none';
    document.getElementById('boxPlotContainer').style.display = 'none';
}

var getStream = function (path) {
    var blockJSONData = path,
        stream = fs.createReadStream(blockJSONData, { encoding: 'utf8' }),
        parser = JSONStream.parse('*.*');
    return stream.pipe(parser);
};

export function getPlanTable() {

    const url = "http://localhost:8080/states/getDistrictPlansForTable/" + currentState.toString().toUpperCase()
    console.log(url);
    fetch(url)
    .then(res => res.json())
    .then(logged => {
        console.log(logged);
        fillPlanTable(logged);
    })

}

function fillPlanTable(arr){
    console.log(currentPlanData)
    Object.assign(currentPlanData, arr);
    console.log(arr);
    var arrAy = Object.values(arr)
    //arrAy.slice(0, -1); // remove extra added geojson file
    console.log(arrAy)
    for(var i = 1; i <31; i++){
        var cells = document.getElementsByClassName("dist-" + i);
        cells[0].innerText = arrAy[i-1]['id'];
        cells[1].innerText = arrAy[i-1]['equalPopMeasure'];
        cells[2].innerText = arrAy[i-1]['polsbyPopScore'];
        cells[3].innerText = arrAy[i-1]['numOppDistricts'];
        cells[4].innerText = arrAy[i-1]['objFunctScore'];
        cells[5].innerText = arrAy[i-1]['demDistricts'];
        cells[6].innerText = arrAy[i-1]['repDistricts']
    }
}

export function fillDistrictTable() {
    var plan = ''
    var planID='';
    if(currentState==='ga'){
        if(chosenPlan==="none"){
            plan='GA0'
            planID=0
        }else{
            plan = 'GA'+chosenPlan;
            planID=chosenPlan;
        }
    }else{ //nc
        if(chosenPlan==="none"){
            plan='NC0'
            planID=0;
        }else{
            plan = 'NC'+chosenPlan;
            planID=chosenPlan;
        }
    }
    console.log(currentState+" "+plan);

    const url = "http://localhost:8080/states/getDistTableTest/" + currentState.toString().toUpperCase()
                + "/" + plan;
    console.log(url);
    fetch(url)
    .then(res => res.json())
    .then(logged => {
        console.log("fetch return");
        console.log(logged)
        fillDistrictLoop(logged);
        updateCurrentPlanText('Plan: ' + plan)
        setInputs();
    });
}

  export function testing() {
    const url = "http://localhost:8080/states/test/";
    fetch(url)
      .then(res => res.text())
      .then(logged => {
        console.log(logged);
      });
  }

  export function setPopulationVariable() {
    fetch('http://localhost:8080/states/populationVariable/set/', {
        method: 'POST', // or 'PUT'
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({ title: populationVar})
        // body: { title: populationVar}
    })
    .then(response => response.text())
    .then(data => {
        console.log('population_variable_sucess!!' , data);
    });
}

  export function testingPost() {
    fetch('http://localhost:8080/generate/sendTest/', {
        method: 'POST', // or 'PUT'
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({title: "React POST Request Example"})
    })
    .then(response => response.text())
    .then(data => {
        console.log('Success!!! ' , data);
    });
}

  export function fillDistrictLoop(arr) {
    Object.assign(currentDistrictsData, arr);
    console.log(arr);
    var arrAy = Object.values(arr)
    if(arrAy[0]['districtPlan']['id'] === 'GA0' || arrAy[0]['districtPlan']['id'] === 'NC0'){
        Object.assign(enactedDistrictsData, arr);
        console.log("setting enactedDistrictsData")
        console.log(enactedDistrictsData);
    } else if(!arrAy[0]['districtPlan']['id'].includes('M')){
        Object.assign(chosenDistrictsData, arr);
        console.log("setting chosenDistrictsData")
        console.log(chosenDistrictsData);
    }
    arrAy.slice(0, -1); // remove extra added geojson file
    console.log(arrAy)
    var total = 13;
    if(currentState === 'ga'){
        document.getElementById('distr-14').style.display = 'table-row';
        document.getElementById('demo-14').style.display = 'table-row';
        document.getElementById('demo-perc-14').style.display = 'table-row';
        total = 14;
    } else {
        document.getElementById('distr-14').style.display = 'none';
        document.getElementById('demo-14').style.display = 'none';
        document.getElementById('demo-perc-14').style.display = 'none';
        total = 13;
    }
    var i = 0;
    var j = 0;
    console.log(arrAy[0]['districtNum'])
    var planArr = document.getElementsByClassName("planInfo");
    planArr[0].innerText = arrAy[0]['districtPlan']['id']
    planArr[1].innerText = arrAy[0]['districtPlan']['equalPopMeasure'].toFixed(2);
    planArr[2].innerText = arrAy[0]['districtPlan']['polsbyPopScore'].toFixed(2);
    planArr[3].innerText = arrAy[0]['districtPlan']['numOppDistricts']
    planArr[4].innerText = arrAy[0]['districtPlan']['objFunctScore'].toFixed(2);
    currentPlanPath = arrAy[0]['districtPlan']['geojsonPath'];
    updateDownloadPath(currentPlanPath);
    console.log(currentPlanPath);
    var demCount = 0
    var repCount = 0
    
    if(populationVar === 'total'){
        for(var y = 0; y < total; y++){
            var polArr = document.getElementsByClassName('distInfo');
            polArr[i+0].innerText = arrAy[y]['districtNum']
            var popArr = arrAy[y]['populationData'];
            polArr[i+1].innerText = popArr['totalPopulation'].toLocaleString();
            var perc = [(popArr['totalWhite']/popArr['totalPopulation'])*100, (popArr['totalBlack']/popArr['totalPopulation'])*100, (popArr['totalAsian']/popArr['totalPopulation'])*100, (popArr['totalHis']/popArr['totalPopulation'])*100]
            var group = '';
            var ans = ''
            var q = 0;
            if(perc[0] >= 50){
                group = 'White';
                ans = 'No';
                q = 0;
            } else if(perc[1] >= 50){
                group = 'Afr. American';
                ans = 'Yes';
                q = 1;
            } else if(perc[2] >= 50){
                group = 'Asian';
                ans = 'Yes';
                q = 2;
            } else if(perc[3] >= 50){
                group = 'Hispanic';
                ans = 'Yes';
                q = 3;
            } else {
                group = 'N/A';
                ans = 'No';
                q = -1;
            }
            polArr[i+2].innerText = group;
            if(q == -1)
                polArr[i+3].innerText = 'N/A';
            else
                polArr[i+3].innerText = perc[q].toFixed(2) + "%";
            polArr[i+5].innerText = arrAy[y]['polsbyPopScore'].toFixed(2);
            polArr[i+4].innerText = ans;
            i += 6;
            var percArr = document.getElementsByClassName('demoPercInfo');
            var demoArr = document.getElementsByClassName('demoInfo');
            demoArr[j+0].innerText = arrAy[y]['districtNum'];
            percArr[j+0].innerText = arrAy[y]['districtNum'];
            percArr[j+1].innerText = ((popArr['totalWhite'] / popArr['totalPopulation'])*100).toFixed(2) + '%';
            demoArr[j+1].innerText = popArr['totalWhite'].toLocaleString();
            demoArr[j+2].innerText = popArr['totalBlack'].toLocaleString();
            percArr[j+2].innerText = ((popArr['totalBlack'] / popArr['totalPopulation'])*100).toFixed(2) + '%';
            demoArr[j+3].innerText = popArr['totalAsian'].toLocaleString();
            percArr[j+3].innerText = ((popArr['totalAsian'] / popArr['totalPopulation'])*100).toFixed(2) + '%';
            demoArr[j+4].innerText = popArr['totalHis'].toLocaleString();
            percArr[j+4].innerText = ((popArr['totalHis'] / popArr['totalPopulation'])*100).toFixed(2) + '%';
            percArr[j+5].innerText = ((popArr['totalNonHis'] / popArr['totalPopulation'])*100).toFixed(2) + '%';
            demoArr[j+5].innerText = popArr['totalNonHis'].toLocaleString();
            var demPerc = (popArr['demPresTotalVotes'] / popArr['presTotalVotes']) * 100;
            if(demPerc >= 50)
                demCount++;
            else 
                repCount++;
            var repPerc = (popArr['repPresTotalVotes'] / popArr['presTotalVotes']) * 100;
            percArr[j+6].innerText = demPerc.toFixed(2) + '%';
            demoArr[j+6].innerText = popArr['demPresTotalVotes'].toLocaleString();
            percArr[j+7].innerText = repPerc.toFixed(2) + '%';
            demoArr[j+7].innerText = popArr['repPresTotalVotes'].toLocaleString();
            j += 8;
        }
    } else {
        for(var y = 0; y < total; y++){
            var polArr = document.getElementsByClassName('distInfo');
            polArr[i+0].innerText = arrAy[y]['districtNum']
            var popArr = arrAy[y]['populationData'];
            polArr[i+1].innerText = popArr['totalVAPPopulation'].toLocaleString();
            var perc = [(popArr['totalWhite']/popArr['totalPopulation'])*100, (popArr['totalBlack']/popArr['totalPopulation'])*100, (popArr['totalAsian']/popArr['totalPopulation'])*100, (popArr['totalHis']/popArr['totalPopulation'])*100]
            var group = '';
            var ans = ''
            var q = 0;
            if(perc[0] >= 50){
                group = 'White';
                ans = 'No';
                q = 0;
            } else if(perc[1] >= 50){
                group = 'Afr. American';
                ans = 'Yes';
                q = 1;
            } else if(perc[2] >= 50){
                group = 'Asian';
                ans = 'Yes';
                q = 2;
            } else if(perc[3] >= 50){
                group = 'Hispanic';
                ans = 'Yes';
                q = 3;
            } else {
                group = 'N/A';
                ans = 'No';
                q = -1;
            }
            polArr[i+2].innerText = group;
            if(q == -1)
                polArr[i+3].innerText = 'N/A';
            else
                polArr[i+3].innerText = perc[q].toFixed(2) + "%";
            polArr[i+5].innerText = arrAy[y]['polsbyPopScore'].toFixed(2);
            polArr[i+4].innerText = ans;
            i += 6;
            var percArr = document.getElementsByClassName('demoPercInfo');
            var demoArr = document.getElementsByClassName('demoInfo');
            demoArr[j+0].innerText = arrAy[y]['districtNum'];
            percArr[j+0].innerText = arrAy[y]['districtNum'];
            percArr[j+1].innerText = ((popArr['whiteVAP'] / popArr['totalVAPPopulation'])*100).toFixed(2) + '%';
            demoArr[j+1].innerText = popArr['whiteVAP'].toLocaleString();
            demoArr[j+2].innerText = popArr['blackVAP'].toLocaleString();
            percArr[j+2].innerText = ((popArr['blackVAP'] / popArr['totalVAPPopulation'])*100).toFixed(2) + '%';
            demoArr[j+3].innerText = popArr['asianVAP'].toLocaleString();
            percArr[j+3].innerText = ((popArr['asianVAP'] / popArr['totalVAPPopulation'])*100).toFixed(2) + '%';
            percArr[j+4].innerText = ((popArr['hisVAP'] / popArr['totalPopulation'])*100).toFixed(2) + '%';
            demoArr[j+4].innerText = popArr['hisVAP'].toLocaleString();
            percArr[j+4].innerText = ((popArr['nonHisVAP'] / popArr['totalVAPPopulation'])*100).toFixed(2) + '%';
            demoArr[j+5].innerText = popArr['nonHisVAP'].toLocaleString();
            var demPerc = (popArr['demPresTotalVotes'] / popArr['presTotalVotes']) * 100;
            if(demPerc >= 50)
                demCount++;
            else 
                repCount++;
            var repPerc = (popArr['repPresTotalVotes'] / popArr['presTotalVotes']) * 100;
            percArr[j+6].innerText = demPerc.toFixed(2) + '%';
            demoArr[j+6].innerText = popArr['demPresTotalVotes'].toLocaleString();
            percArr[j+7].innerText = repPerc.toFixed(2) + '%';
            demoArr[j+7].innerText = popArr['repPresTotalVotes'].toLocaleString();
            j += 8;

        }
    }
    planArr[5].innerText = demCount;
    planArr[6].innerText = repCount;

  }

export function fillTable(){
    console.log("cur stat "+currentState)
    const url = "http://localhost:8080/states/getPopData/" + currentState;
    console.log(url);
    fetch(url)
    .then(res => res.json())
    .then(logged => {
        console.log(logged);
        console.log(logged.populationData['totalPopulation']);
        var json = getPercentages(logged);
        json = constructJSONTable(json);
        fillLoop(json);
    });
}

export function getPercentages(arr) {
    console.log("populationVar: " + populationVar);
    if(populationVar === 'total'){
        console.log("total true");
        var jsonArr = {
            "totalPopulation": 0,
            "blackPerc": 0,
            "totalBlack": 0,
            "whitePerc": 0,
            "totalWhite": 0,
            "hisPerc": 0,
            "totalHis": 0,
            "nonHisPerc": 0,
            "totalNonHis": 0,
            "asianPerc": 0,
            "totalAsian": 0,
            "totalPresVotes": 0,
            "totalDemPresVotes": 0,
            "totalDemPresPerc": 0,
            "totalRepPresVotes": 0,
            "totalRepPresPerc": 0
        }
        jsonArr['totalPopulation'] = arr.populationData['totalPopulation'];
        console.log(jsonArr['totalPopulation']);
        jsonArr['blackPerc'] = parseFloat((arr.populationData['totalBlack'] / jsonArr['totalPopulation'])*100).toFixed(2);
        console.log(jsonArr['blackPerc']);
        jsonArr['totalBlack'] = arr.populationData['totalBlack'];
        jsonArr['whitePerc'] = parseFloat((arr.populationData['totalWhite'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalWhite'] = arr.populationData['totalWhite'];
        jsonArr['hisPerc'] = parseFloat((arr.populationData['totalHis'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalHis'] = arr.populationData['totalHis'];
        jsonArr['nonHisPerc'] = parseFloat((arr.populationData['totalNonHis'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalNonHis'] = arr.populationData['totalNonHis'];
        jsonArr['asianPerc'] = parseFloat((arr.populationData['totalAsian'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalAsian'] = arr.populationData['totalAsian'];
        jsonArr['totalPresVotes'] =  arr.populationData['presTotalVotes'];
        jsonArr['totalDemPresVotes'] = arr.populationData['demPresTotalVotes'];
        jsonArr['totalDemPresPerc'] = parseFloat((arr.populationData['demPresTotalVotes'] / jsonArr['totalPresVotes'])*100).toFixed(2);
        jsonArr['totalRepPresVotes'] = arr.populationData['repPresTotalVotes'];
        jsonArr['totalRepPresPerc'] = parseFloat((arr.populationData['repPresTotalVotes'] / jsonArr['totalPresVotes'])*100).toFixed(2);
    } else {
        var jsonArr = {
            "totalPopulation": 0,
            "blackPerc": 0,
            "totalBlack": 0,
            "whitePerc": 0,
            "totalWhite": 0,
            "hisPerc": 0,
            "totalHis": 0,
            "nonHisPerc": 0,
            "totalNonHis": 0,
            "asianPerc": 0,
            "totalAsian": 0,
            "totalPresVotes": 0,
            "totalDemPresVotes": 0,
            "totalDemPresPerc": 0,
            "totalRepPresVotes": 0,
            "totalRepPresPerc": 0
        }
        jsonArr['totalPopulation'] = arr.populationData['totalVAPPopulation'];
        console.log(jsonArr['totalPopulation']);
        jsonArr['blackPerc'] = parseFloat((arr.populationData['blackVAP'] / jsonArr['totalPopulation'])*100).toFixed(2);
        console.log(jsonArr['blackPerc']);
        jsonArr['totalBlack'] = arr.populationData['blackVAP'];
        jsonArr['whitePerc'] = parseFloat((arr.populationData['whiteVAP'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalWhite'] = arr.populationData['whiteVAP'];
        jsonArr['hisPerc'] = parseFloat((arr.populationData['hisVAP'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalHis'] = arr.populationData['hisVAP'];
        jsonArr['nonHisPerc'] = parseFloat((arr.populationData['nonHisVAP'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalNonHis'] = arr.populationData['nonHisVAP'];
        jsonArr['asianPerc'] = parseFloat((arr.populationData['asianVAP'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalAsian'] = arr.populationData['asianVAP'];
        jsonArr['totalPresVotes'] =  arr.populationData['presTotalVotes'];
        jsonArr['totalDemPresVotes'] = arr.populationData['demPresTotalVotes'];
        jsonArr['totalDemPresPerc'] = parseFloat((arr.populationData['demPresTotalVotes'] / jsonArr['totalPresVotes'])*100).toFixed(2);
        jsonArr['totalRepPresVotes'] = arr.populationData['repPresTotalVotes'];
        jsonArr['totalRepPresPerc'] = parseFloat((arr.populationData['repPresTotalVotes'] / jsonArr['totalPresVotes'])*100).toFixed(2);
    }
    console.log(jsonArr);
    return jsonArr;
}

export function constructJSONTable(arr){
    var stateHeader;
    var tableHeader;
    var districtNumDem;
    var districtNumRep;
    var districtPercDem;
    var districtPercRep;
    if(currentState === 'ga'){
        stateHeader = "Georgia State (2,679 Precincts)";
        tableHeader = "Georgia Redistricting Plans";
        districtNumDem = 6;
        districtNumRep = 8;
        districtPercDem = (6/14) * 100;
        districtPercRep = (8/14) * 100;
    } else {
        stateHeader = "North Carolina State (2,666 Precincts)";
        tableHeader = "North Carolina Redistricting Plans";
        districtNumDem = 3;
        districtNumRep = 10;
        districtPercDem = (3/13) * 100;
        districtPercRep = (10/13) * 100;
    }
    var jsonArr = {
        "headers":
            {
                "total-votes": "",
                "pop-total": "",
                "state-table-header": stateHeader,
                "district-table-header": tableHeader
            },
        "political": [
            {
                "party": "Democratic",
                "districtNumber":districtNumDem,
                "districtPercent": districtPercDem.toFixed(2) + "%",
                "voteNumber": 0,
                "votePercent": ""
            },
            {
                "party": "Republican",
                "districtNumber": districtNumRep,
                "districtPercent": districtPercRep.toFixed(2) + "%",
                "voteNumber": 0,
                "votePercent": ""
            }
        ],
        "demographic": [
            {
                "ethnicity": "White",
                "population": 0,
                "populationPercent": "",
            },
            {
                "ethnicity": "African American",
                "population": 0,
                "populationPercent": "",
            },
            {
                "ethnicity": "Hispanic",
                "population": 0,
                "populationPercent": "",
            },
            {
                "ethnicity": "Non Hispanic",
                "population": 0,
                "populationPercent": "",
            },
            {
                "ethnicity": "Asian",
                "population": 0,
                "populationPercent": "",
            }
        ]
    }

    jsonArr.headers["total-votes"] = arr['totalPresVotes'].toLocaleString();
    jsonArr.headers["pop-total"] = arr['totalPopulation'].toLocaleString();
    jsonArr.political[1]['voteNumber'] = arr['totalRepPresVotes'].toLocaleString();
    jsonArr.political[1]['votePercent'] = arr['totalRepPresPerc'].toString() + "%";
    jsonArr.political[0]['voteNumber'] = arr['totalDemPresVotes'].toLocaleString();
    jsonArr.political[0]['votePercent'] = arr['totalDemPresPerc'].toString() + "%";
    jsonArr.demographic[0]['population'] = arr['totalWhite'].toLocaleString();
    jsonArr.demographic[0]['populationPercent'] = arr['whitePerc'].toString() + "%";
    jsonArr.demographic[1]['population'] = arr['totalBlack'].toLocaleString();
    jsonArr.demographic[1]['populationPercent'] = arr['blackPerc'].toString() + "%";
    jsonArr.demographic[2]['population'] = arr['totalHis'].toLocaleString();
    jsonArr.demographic[2]['populationPercent'] = arr['hisPerc'].toString() + "%";
    jsonArr.demographic[3]['population'] = arr['totalNonHis'].toLocaleString();
    jsonArr.demographic[3]['populationPercent'] = arr['nonHisPerc'].toString() + "%";
    jsonArr.demographic[4]['population'] = arr['totalAsian'].toLocaleString();
    jsonArr.demographic[4]['populationPercent'] = arr['asianPerc'].toString() + "%";
    console.log(jsonArr);
    return jsonArr;
}

  function fillLoop(arr) {
    console.log("arr: " + arr);
    var jsonData = arr;
    var texts = Object.values(jsonData['headers']);
    console.log("texts: " + JSON.stringify(texts));
    document.getElementById('total-votes').innerText = "Total Votes: " + texts[0];
    document.getElementById('pop-total').innerText = "Total Population: " + texts[1].toLocaleString();
    document.getElementById('state-table-header').innerText = texts[2];
    document.getElementById('district-table-header').innerText = texts[3];
    var polArr = document.getElementsByClassName('pol-1');
    var i = 0;
    var j = 0;
    var k = 0;
    var values = '';
    for (k = 0; k < 2; k++) {
      values = Object.values(jsonData.political[k]);
      for (j = 0; j < values.length; j++) {
        polArr[i].innerText = values[j];
        i++;
      }
    }
    polArr = document.getElementsByClassName('dem-1');
    i = 0;
    for (k = 0; k < 5; k++) {
      values = Object.values(jsonData.demographic[k]);
      for (j = 0; j < values.length; j++) {
        polArr[i].innerText = values[j];
        i++;
      }
    }
  }

const getFromIndex = (array, indexes) => {
return array.filter((element, index) => indexes.includes(index)); 
};

function makePointPlot(arr, colori,namei){
    var len=0;
    if(currentState==='ga'){
        len = 14;
    }else{
        len=13;
    }
    var xData = [];
    for(var i =1;i<len+1;i++){
        xData.push(i);
    }
    var t1 = {
        y: arr,
        x: xData,
        marker: {color: colori},
        mode: 'markers',
        name:namei,
        lineColor:'rgba(0,0,0,0)'
    };
    return t1
}

function makeBox(y){
    var len=0;
    if(currentState==='ga'){
        len = 14;
    }else{
        len=13;
    }
    var xData=[];
    for(var i =1;i<len+1;i++){
        for(var j =0;j<5;j++){
            xData.push(i);
        }
    }
    var merged = [].concat.apply([], y);
    
    var yData=merged;
    var t1 = {
        y: yData,
        x: xData,
        marker: {color: '#3D9970'},
        type: 'box',
        name:"Box Plot"
    };
    return t1
}

function getPoints(en, plotOption){
    var enactedPoints = [];
    for(var i =0;i<en.length;i++){
        console.log(en[i]['populationData']);
        var popData = en[i]['populationData'];
        if(plotOption==='PresD'){
            console.log()
            enactedPoints.push(popData["demPresTotalVotes"]/popData['presTotalVotes']);
        }else if(plotOption==='PresR'){
            enactedPoints.push(popData["repPresTotalVotes"]/popData['presTotalVotes']);
        }else if(populationVar==='total'){
            if(plotOption==='AfAmer'){
                enactedPoints.push(popData["totalBlack"]/popData['totalPopulation']);
            }else if(plotOption==='White'){
                enactedPoints.push(popData["totalWhite"]/popData['totalPopulation']);
            }else if(plotOption==='Asian'){
                enactedPoints.push(popData["totalAsian"]/popData['totalPopulation']);
            }else if(plotOption==='His'){
                enactedPoints.push(popData["totalHis"]/popData['totalPopulation']);
            }
        }else{
            if(plotOption==='AfAmer'){ // vap area
                enactedPoints.push(popData["blackVAP"]/popData['totalPopulation']);
            }else if(plotOption==='White'){
                enactedPoints.push(popData["whiteVAP"]/popData['totalPopulation']);
            }else if(plotOption==='Asian'){
                enactedPoints.push(popData["asianVAP"]/popData['totalPopulation']);
            }else if(plotOption==='His'){
                enactedPoints.push(popData["hisVAP"]/popData['totalPopulation']);
            }
        }
    }
    return enactedPoints;
}

export function boxPlot(){
    const dropdown = document.getElementById('plot-view-dropdown');
    const plotOption = dropdown.value;
    var boxData;
    if(currentState=='ga'){
        boxData = gaBoxData;
    }else if (currentState=='nc'){
        boxData=ncBoxData;
    }
    var comp = boxData[plotOption];
    var compString = "";
    console.log("POP VAR "+populationVar);
    if(plotOption==="PresD"){
        compString="Presidential Democratic Votes";
    }else if (plotOption==='PresR'){
        compString="Presidential Republican Votes";
    }else if (plotOption==="AfAmer"){
        compString="African American Population";
        if(populationVar==='vap'){
            console.log("OG "+comp);
            console.log("vap "+boxData[plotOption+"VAP"]);
            comp = boxData[plotOption+"VAP"];
        }
    }else if (plotOption==="White"){
        compString="White Population";
        if(populationVar==='vap'){
            comp = boxData[plotOption+"VAP"];
        }
    }else if(plotOption==="Asian"){
        compString="Asian Population";
        if(populationVar==='vap'){
            console.log("IS VAP");
            comp = boxData[plotOption+"VAP"];
        }
    }else if(plotOption==="His"){
        compString="Hispanic Population";
        if(populationVar==='vap'){
            comp = boxData[plotOption+"VAP"];
        }
    }else{ //polsby
        compString="Polsby Popper";
    }
    var min = 50;
    var max =0;
    for (var i =0;i<comp.length;i++){
        for (var j =0;j<comp[i].length;j++){
            if(comp[i][j]>max){
                max = comp[i][j];
            }
            if(comp[i][j]<min){
                min = comp[i][j];
            }
        }
    }
    // need to sort by median
    var arr = comp;
    arr.sort((function(index){
        return function(a, b){
            return (a[index] === b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
        };
    })(0)); 

    var t1 = makeBox(comp);

    var en = Object.values(enactedDistrictsData);
    console.log(en);
    var enactedPoints=[];

    var ch = Object.values(chosenDistrictsData);
    var chosenPoints=[];
    if(ch!=null){
        chosenPoints = getPoints(ch,plotOption);
    }
    var ge = Object.values(generatedDistrictsData);
    var generatedPoints=[];
    if(ge!=null){
        generatedPoints=getPoints(ge, plotOption);
    }
    for(var i =0;i<en.length;i++){
        console.log(en[i]['populationData']);
        var popData = en[i]['populationData'];
        if(plotOption==='PresD'){
            console.log()
            enactedPoints.push(popData["demPresTotalVotes"]/popData['presTotalVotes']);
        }else if(plotOption==='PresR'){
            enactedPoints.push(popData["repPresTotalVotes"]/popData['presTotalVotes']);
        }else if(populationVar==='total'){
            if(plotOption==='AfAmer'){
                enactedPoints.push(popData["totalBlack"]/popData['totalPopulation']);
            }else if(plotOption==='White'){
                enactedPoints.push(popData["totalWhite"]/popData['totalPopulation']);
            }else if(plotOption==='Asian'){
                enactedPoints.push(popData["totalAsian"]/popData['totalPopulation']);
            }else if(plotOption==='His'){
                enactedPoints.push(popData["totalHis"]/popData['totalPopulation']);
            }
        }else{
            if(plotOption==='AfAmer'){ // vap area
                enactedPoints.push(popData["blackVAP"]/popData['totalPopulation']);
            }else if(plotOption==='White'){
                enactedPoints.push(popData["whiteVAP"]/popData['totalPopulation']);
            }else if(plotOption==='Asian'){
                enactedPoints.push(popData["asianVAP"]/popData['totalPopulation']);
            }else if(plotOption==='His'){
                enactedPoints.push(popData["hisVAP"]/popData['totalPopulation']);
            }
        }
    }
    console.log(enactedPoints);
    enactedPoints.sort();
    generatedPoints.sort();
    chosenPoints.sort();
    console.log(enactedPoints);
    var t2=makePointPlot(enactedPoints, "#D23a3a","Enacted");
    var t3=null;
    var t4=null;
    if(generatedPoints!=[]){
        t3=makePointPlot(generatedPoints, "#3260c1","Generated");

    }
    if(chosenPoints!=[]){
        t4=makePointPlot(chosenPoints, "#8a2dcb", "Chosen Seawulf");
    }

    

    // var t9 = {
    //     y: [.4],
    //     x: [2],
    //     name: 'Enacted',
    //     marker: {color: '#5E7999'},
    //     type: 'point'
    // };

    var data = [];
    if(currentState=='ga'){
        data = [t1,t2,t3,t4];
    }else if (currentState=='nc'){
        data = [t1,t2,t3,t4];
    }

    var layout = {
        yaxis: {
            title: 'Democratic Presidential Votes',
            zeroline: false,
            //  range: [(min*.97),(max*1.03)]
        },
        xaxis: {
            title: 'Districts',
            zeroline: false
        }
    };
    layout.yaxis.title=compString;
    Plotly.newPlot('boxPlot', data, layout);
}

function seawulfPlanOnMap(id){
    setChosenPlan(id);
    var plan = ''
    var planID='';
    if(currentState==='ga'){
        if(chosenPlan==="none"){
            plan='GA0'
            planID=0
        }else{
            plan = 'GA'+chosenPlan;
            planID=chosenPlan;
        }
    }else{ //nc
        if(chosenPlan==="none"){
            plan='NC0'
            planID=0;
        }else{
            plan = 'NC'+chosenPlan;
            planID=chosenPlan;
        }
    }

    const url = "http://localhost:8080/states/getDistrictPlanForDisplay/" + currentState.toString().toUpperCase()
                + "/" + plan;
    console.log(url);
    fetch(url)
    .then(res => res.json())
    .then(logged => {
        console.log("fetch return");
        console.log(logged)
        var geojson = logged['geojson'];
        // get population arr
        var seawulfPops=[];
        if(currentState==='ga'){
            seawulfPops = [[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
        }else{
            seawulfPops = [[],[],[],[],[],[],[],[],[],[],[],[],[]];
        }
        
        for(var i =0;i<geojson['features'].length;i++){
            var dist = geojson['features'][i]['properties']['DistrictID'];
            dist = dist -1;
            var pop = geojson['features'][i]['properties']['TOTALPOP'];
            seawulfPops[dist]+=pop;
        }
        console.log("POPULATIONS "+seawulfPops);
        setSeawulfPops(seawulfPops);
        //turn off currentLayer
        console.log(currentLayerID);
        document.getElementById('chosen-plan').innerText = "Generate a new plan using Plan " + currentState.toUpperCase()+chosenPlan;
        map.current.setLayoutProperty(currentLayerID, 'visibility', 'none');
        map.current.setLayoutProperty(currentLayerID+"_map", 'visibility', 'none');
        if(getLayer(currentState+'_plan_'+plan)){
            console.log("exists");
            map.current.setLayoutProperty(currentState+'_plan_'+plan, 'visibility', 'visible');
            map.current.setLayoutProperty(currentState+'_plan_'+plan+"_map", 'visibility', 'visible');
        }else{
            addGeoJSONLayer('ga', currentState+'_plan_'+plan, 'line', currentState+'_counties_elections_outline', 'district', 'district', geojson, 'visible', 6, 15);
            addGeoJSONLayer('ga', currentState+'_plan_'+plan+"_map", 'fill', currentState+'_plan_'+plan, 'district', 'redistrColors', geojson, 'visible', 6, 15);    
        }
        setCurrentLayerID(currentState+'_plan_'+plan);
        console.log(currentLayerID);
        // fillDistrictLoop(logged);
    });
}


export function fillComparisonTable(){
    var enact = Object.values(enactedDistrictsData);
    console.log(enact);
    console.log(enactedDistrictsData)
    var chosen = Object.values(chosenDistrictsData);
    var generate = Object.values(generatedDistrictsData);
    var enactArr = document.getElementsByClassName('enactInfo');
    var chosenArr = document.getElementsByClassName('chosenInfo');
    var genArr = document.getElementsByClassName('genInfo');
    var demCount1 = 0, demCount2 = 0, demCount3 = 0, repCount1 = 0, repCount2 = 0, repCount3 = 0;

    enactArr[0].innerText = enact[0]['districtPlan']['id']
    chosenArr[0].innerText = chosen[0]['districtPlan']['id']
    genArr[0].innerText = generate[0]['districtPlan']['id']
    enactArr[1].innerText = enact[0]['districtPlan']['equalPopMeasure'].toFixed(3);
    chosenArr[1].innerText = chosen[0]['districtPlan']['equalPopMeasure'].toFixed(3);
    genArr[1].innerText = generate[0]['districtPlan']['equalPopMeasure'].toFixed(3);
    enactArr[2].innerText = enact[0]['districtPlan']['polsbyPopScore'].toFixed(3);
    chosenArr[2].innerText = chosen[0]['districtPlan']['polsbyPopScore'].toFixed(3);
    genArr[2].innerText = generate[0]['districtPlan']['polsbyPopScore'].toFixed(3);
    chosenArr[3].innerText = chosen[0]['districtPlan']['numOppDistricts'];
    enactArr[3].innerText = enact[0]['districtPlan']['numOppDistricts'];
    genArr[3].innerText = generate[0]['districtPlan']['numOppDistricts'];
    enactArr[4].innerText = enact[0]['districtPlan']['objFunctScore'].toFixed(3);
    chosenArr[4].innerText = chosen[0]['districtPlan']['objFunctScore'].toFixed(3);
    genArr[4].innerText = generate[0]['districtPlan']['objFunctScore'].toFixed(3);
    var total;
    if(currentState === 'ga'){
        total = 14;
    } else {
        total = 13;
    }
    var demPerc;
    for(var i = 0; i < total; i++){
        demPerc = (enact[i]['populationData']['demPresTotalVotes'] / enact[i]['populationData']['presTotalVotes']) * 100;
        if(demPerc >= 50)
            demCount1++;
        else
            repCount1++;
        demPerc = (chosen[i]['populationData']['demPresTotalVotes'] / enact[i]['populationData']['presTotalVotes']) * 100;
        if(demPerc >= 50)
            demCount2++;
        else
            repCount2++;
        demPerc = (generate[i]['populationData']['demPresTotalVotes'] / enact[i]['populationData']['presTotalVotes']) * 100;
        if(demPerc >= 50)
            demCount3++;
        else
            repCount3++;
    }
    enactArr[5].innerText = demCount1;
    chosenArr[5].innerText = demCount2;
    genArr[5].innerText = demCount3;
    enactArr[6].innerText = repCount1;
    chosenArr[6].innerText = repCount2;
    genArr[6].innerText = repCount3;
}

export function changeMapPlan(plan){
    const url = "http://localhost:8080/states/getDistrictPlanForDisplay/" + currentState.toString().toUpperCase()
    + "/" + plan;
    fetch(url)
    .then(res => res.json())
    .then(logged => {
        var geojson = logged['geojson'];
        map.current.setLayoutProperty(currentLayerID, 'visibility', 'none');
        map.current.setLayoutProperty(currentLayerID+"_map", 'visibility', 'none');
        if(getLayer(currentState+'_plan_'+plan)){
            console.log("exists");
            map.current.setLayoutProperty(currentState+'_plan_'+plan, 'visibility', 'visible');
            map.current.setLayoutProperty(currentState+'_plan_'+plan+"_map", 'visibility', 'visible');
        }else{
            console.log(plan +" does not exist yet need to create");
            addGeoJSONLayer('ga', currentState+'_plan_'+plan, 'line', currentState+'_counties_elections_outline', 'district', 'election', geojson, 'visible', 6, 15);
            addGeoJSONLayer('ga', currentState+'_plan_'+plan+"_map", 'fill', currentState+'_plan_'+plan, 'district', 'districtColors', geojson, 'visible', 6, 15);    
        
        }


        setCurrentLayerID(currentState+'_plan_'+plan);
        console.log(currentLayerID);
    })
}

function openGeneratedPlan(){
    fillDistrictLoop(generatedDistrictsData);
    var gen = Object.values(generatedDistrictsData);
    var id = gen[0]['districtPlan']['id'];
    changeMapPlan(id);
}

function openEnactedPlan(){
    fillDistrictLoop(enactedDistrictsData);
    var en = Object.values(enactedDistrictsData);
    var id = en[0]['districtPlan']['id'];
    console.log("enacted id???? "+id);
    changeMapPlan(id);
}

function openChosenPlan(){
    fillDistrictLoop(chosenDistrictsData);
    var chosen = Object.values(chosenDistrictsData);
    var id = chosen[0]['districtPlan']['id'];
    changeMapPlan(id);
}
function openSeawulfPlan(id){
    console.log(id);
    // update OG state table?
    // update map
    seawulfPlanOnMap(id);
    fillDistrictTable();
}

const SideTable = () => {
   return ( 
        <div class='map-overlay-table' id='table' >
            <div class='tab' id="tableLinks" style={{display:"none"}}>
                <button class="tablinks" onClick={openStateTable}>State</button>
                <button class="tablinks" onClick={openDistrictTable}>District Plan</button>
                <button class="tablinks" onClick={openDistrictInfo}>District Info</button>
                <button class="tablinks" onClick={openComparisonTable} id="compare-tab">Compare Plans</button>
                <button class="tablinks" onClick={openBoxPlot}>Box-Whisker</button>
            </div>

            <div id='State' class="tabcontent" style={{display:"none"}}>
                <p class="table-header" id='state-table-header'></p>
                <table class='voting-data-css'>
                <thead>
                    <tr>
                    <th>Party</th>
                    <th># Districts</th>
                    <th>Total District %</th>
                    <th># Votes</th>
                    <th>Total Vote %</th>
                    </tr>
                </thead>
                <tr>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                </tr>
                <tr>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                </tr>
                </table>
                <p class='table-total' id='total-votes'></p>
                <table class='demographic-data-css'>
                <thead>
                    <tr>
                    <th>Demographic Group</th>
                    <th>Population</th>
                    <th>Population %</th>
                    </tr>
                </thead>
                <tr>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                </tr>
                <tr>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                </tr>
                <tr>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                </tr>
                <tr>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                </tr>
                <tr>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                </tr>
                </table>
                <p class="table-total" id='pop-total'></p>
            </div>

            <div id='District' class="tabcontent" style={{display:"none"}}>
                <p class="table-header" id='district-table-header'></p>
                <table class='voting-data-css'>
                <thead>
                    <tr>
                    <th>Seawulf Plan #</th>
                    <th>Pop. Equality</th>
                    <th>Polsby Popper</th>
                    <th>Num. of Opp. Distr.</th>
                    <th>Obj. Function</th>
                    <th>Num. of Dem. Districts</th>
                    <th>Num. of Rep. Districts</th>
                    </tr>
                </thead>
                <tr id='red-1' onClick={()=>openSeawulfPlan('1')}>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                </tr>
                <tr id='red-2' onClick={()=>openSeawulfPlan('2')}>
                    <td class='dist-2'></td>
                    <td class='dist-2'></td>
                    <td class='dist-2'></td>
                    <td class='dist-2'></td>
                    <td class='dist-2'></td>
                    <td class='dist-2'></td>
                    <td class='dist-2'></td>
                </tr>
                <tr id='red-3' onClick={()=>openSeawulfPlan('3')}>
                    <td class='dist-3'></td>
                    <td class='dist-3'></td>
                    <td class='dist-3'></td>
                    <td class='dist-3'></td>
                    <td class='dist-3'></td>
                    <td class='dist-3'></td>
                    <td class='dist-3'></td>
                </tr>
                <tr id='red-4' onClick={()=>openSeawulfPlan('4')}>
                    <td class='dist-4'></td>
                    <td class='dist-4'></td>
                    <td class='dist-4'></td>
                    <td class='dist-4'></td>
                    <td class='dist-4'></td>
                    <td class='dist-4'></td>
                    <td class='dist-4'></td>
                </tr>
                <tr id='red-5' onClick={()=>openSeawulfPlan('5')}>
                    <td class='dist-5'></td>
                    <td class='dist-5'></td>
                    <td class='dist-5'></td>
                    <td class='dist-5'></td>
                    <td class='dist-5'></td>
                    <td class='dist-5'></td>
                    <td class='dist-5'></td>
                </tr>
                <tr id='red-6' onClick={()=>openSeawulfPlan('6')}>
                    <td class='dist-6'></td>
                    <td class='dist-6'></td>
                    <td class='dist-6'></td>
                    <td class='dist-6'></td>
                    <td class='dist-6'></td>
                    <td class='dist-6'></td>
                    <td class='dist-6'></td>
                </tr>
                <tr id='red-7' onClick={()=>openSeawulfPlan('7')}>
                    <td class='dist-7'></td>
                    <td class='dist-7'></td>
                    <td class='dist-7'></td>
                    <td class='dist-7'></td>
                    <td class='dist-7'></td>
                    <td class='dist-7'></td>
                    <td class='dist-7'></td>
                </tr>
                <tr id='red-8' onClick={()=>openSeawulfPlan('8')}>
                    <td class='dist-8'></td>
                    <td class='dist-8'></td>
                    <td class='dist-8'></td>
                    <td class='dist-8'></td>
                    <td class='dist-8'></td>
                    <td class='dist-8'></td>
                    <td class='dist-8'></td>
                </tr>
                <tr id='red-9' onClick={()=>openSeawulfPlan('9')}>
                    <td class='dist-9'></td>
                    <td class='dist-9'></td>
                    <td class='dist-9'></td>
                    <td class='dist-9'></td>
                    <td class='dist-9'></td>
                    <td class='dist-9'></td>
                    <td class='dist-9'></td>
                </tr>
                <tr id='red-10' onClick={()=>openSeawulfPlan('10')}>
                    <td class='dist-10'></td>
                    <td class='dist-10'></td>
                    <td class='dist-10'></td>
                    <td class='dist-10'></td>
                    <td class='dist-10'></td>
                    <td class='dist-10'></td>
                    <td class='dist-10'></td>
                </tr>
                <tr id='red-11' onClick={()=>openSeawulfPlan('11')}>
                    <td class='dist-11'></td>
                    <td class='dist-11'></td>
                    <td class='dist-11'></td>
                    <td class='dist-11'></td>
                    <td class='dist-11'></td>
                    <td class='dist-11'></td>
                    <td class='dist-11'></td>
                </tr>
                <tr id='red-12' onClick={()=>openSeawulfPlan('12')}>
                    <td class='dist-12'></td>
                    <td class='dist-12'></td>
                    <td class='dist-12'></td>
                    <td class='dist-12'></td>
                    <td class='dist-12'></td>
                    <td class='dist-12'></td>
                    <td class='dist-12'></td>
                </tr>
                <tr id='red-13' onClick={()=>openSeawulfPlan('13')}>
                    <td class='dist-13'></td>
                    <td class='dist-13'></td>
                    <td class='dist-13'></td>
                    <td class='dist-13'></td>
                    <td class='dist-13'></td>
                    <td class='dist-13'></td>
                    <td class='dist-13'></td>
                </tr>
                <tr id='red-14' onClick={()=>openSeawulfPlan('14')}>
                    <td class='dist-14'></td>
                    <td class='dist-14'></td>
                    <td class='dist-14'></td>
                    <td class='dist-14'></td>
                    <td class='dist-14'></td>
                    <td class='dist-14'></td>
                    <td class='dist-14'></td>
                </tr>
                <tr id='red-15' onClick={()=>openSeawulfPlan('15')}>
                    <td class='dist-15'></td>
                    <td class='dist-15'></td>
                    <td class='dist-15'></td>
                    <td class='dist-15'></td>
                    <td class='dist-15'></td>
                    <td class='dist-15'></td>
                    <td class='dist-15'></td>
                </tr>
                <tr id='red-16' onClick={()=>openSeawulfPlan('16')}>
                    <td class='dist-16'></td>
                    <td class='dist-16'></td>
                    <td class='dist-16'></td>
                    <td class='dist-16'></td>
                    <td class='dist-16'></td>
                    <td class='dist-16'></td>
                    <td class='dist-16'></td>
                </tr>
                <tr id='red-17' onClick={()=>openSeawulfPlan('17')}>
                    <td class='dist-17'></td>
                    <td class='dist-17'></td>
                    <td class='dist-17'></td>
                    <td class='dist-17'></td>
                    <td class='dist-17'></td>
                    <td class='dist-17'></td>
                    <td class='dist-17'></td>
                </tr>
                <tr id='red-18' onClick={()=>openSeawulfPlan('18')}>
                    <td class='dist-18'></td>
                    <td class='dist-18'></td>
                    <td class='dist-18'></td>
                    <td class='dist-18'></td>
                    <td class='dist-18'></td>
                    <td class='dist-18'></td>
                    <td class='dist-18'></td>
                </tr>
                <tr id='red-19' onClick={()=>openSeawulfPlan('19')}>
                    <td class='dist-19'></td>
                    <td class='dist-19'></td>
                    <td class='dist-19'></td>
                    <td class='dist-19'></td>
                    <td class='dist-19'></td>
                    <td class='dist-19'></td>
                    <td class='dist-19'></td>
                </tr>
                <tr id='red-20' onClick={()=>openSeawulfPlan('20')}>
                    <td class='dist-20'></td>
                    <td class='dist-20'></td>
                    <td class='dist-20'></td>
                    <td class='dist-20'></td>
                    <td class='dist-20'></td>
                    <td class='dist-20'></td>
                    <td class='dist-20'></td>
                </tr>
                <tr id='red-21' onClick={()=>openSeawulfPlan('21')}>
                    <td class='dist-21'></td>
                    <td class='dist-21'></td>
                    <td class='dist-21'></td>
                    <td class='dist-21'></td>
                    <td class='dist-21'></td>
                    <td class='dist-21'></td>
                    <td class='dist-21'></td>
                </tr>
                <tr id='red-22' onClick={()=>openSeawulfPlan('22')}>
                    <td class='dist-22'></td>
                    <td class='dist-22'></td>
                    <td class='dist-22'></td>
                    <td class='dist-22'></td>
                    <td class='dist-22'></td>
                    <td class='dist-22'></td>
                    <td class='dist-22'></td>
                </tr>
                <tr id='red-23' onClick={()=>openSeawulfPlan('23')}>
                    <td class='dist-23'></td>
                    <td class='dist-23'></td>
                    <td class='dist-23'></td>
                    <td class='dist-23'></td>
                    <td class='dist-23'></td>
                    <td class='dist-23'></td>
                    <td class='dist-23'></td>
                </tr>
                <tr id='red-24' onClick={()=>openSeawulfPlan('24')}>
                    <td class='dist-24'></td>
                    <td class='dist-24'></td>
                    <td class='dist-24'></td>
                    <td class='dist-24'></td>
                    <td class='dist-24'></td>
                    <td class='dist-24'></td>
                    <td class='dist-24'></td>
                </tr>
                <tr id='red-25' onClick={()=>openSeawulfPlan('25')}>
                    <td class='dist-25'></td>
                    <td class='dist-25'></td>
                    <td class='dist-25'></td>
                    <td class='dist-25'></td>
                    <td class='dist-25'></td>
                    <td class='dist-25'></td>
                    <td class='dist-25'></td>
                </tr>
                <tr id='red-26' onClick={()=>openSeawulfPlan('26')}>
                    <td class='dist-26'></td>
                    <td class='dist-26'></td>
                    <td class='dist-26'></td>
                    <td class='dist-26'></td>
                    <td class='dist-26'></td>
                    <td class='dist-26'></td>
                    <td class='dist-26'></td>
                </tr>
                <tr id='red-27' onClick={()=>openSeawulfPlan('27')}>
                    <td class='dist-27'></td>
                    <td class='dist-27'></td>
                    <td class='dist-27'></td>
                    <td class='dist-27'></td>
                    <td class='dist-27'></td>
                    <td class='dist-27'></td>
                    <td class='dist-27'></td>
                </tr>
                <tr id='red-28' onClick={()=>openSeawulfPlan('28')}>
                    <td class='dist-28'></td>
                    <td class='dist-28'></td>
                    <td class='dist-28'></td>
                    <td class='dist-28'></td>
                    <td class='dist-28'></td>
                    <td class='dist-28'></td>
                    <td class='dist-28'></td>
                </tr>
                <tr id='red-29' onClick={()=>openSeawulfPlan('29')}>
                    <td class='dist-29'></td>
                    <td class='dist-29'></td>
                    <td class='dist-29'></td>
                    <td class='dist-29'></td>
                    <td class='dist-29'></td>
                    <td class='dist-29'></td>
                    <td class='dist-29'></td>
                </tr>
                <tr id='red-30' onClick={()=>openSeawulfPlan('30')}>
                    <td class='dist-30'></td>
                    <td class='dist-30'></td>
                    <td class='dist-30'></td>
                    <td class='dist-30'></td>
                    <td class='dist-30'></td>
                    <td class='dist-30'></td>
                    <td class='dist-30'></td>
                </tr>
                </table>
            </div>

            <div id='DistrictInfo' class="tabcontent" style={{display:"none"}}>
            <p class="table-header">Chosen Plan Statistics</p>
                <table class="district-data-css">
                    <thead>
                        <tr>
                            <th>Chosen Plan Id</th>
                            <th>Pop. Equality</th>
                            <th>Polsby Popper</th>
                            <th>Num. of Opp. Districts</th>
                            <th>Objective Function</th>
                            <th>Num. of Dem. Districts</th>
                            <th>Num. of Rep. Districts</th>
                        </tr>
                    </thead>
                        <tr id='plan-info'>
                            <td class='planInfo'></td>
                            <td class='planInfo'></td>
                            <td class='planInfo'></td>
                            <td class='planInfo'></td>
                            <td class='planInfo'></td>
                            <td class='planInfo'></td>
                            <td class='planInfo'></td>
                        </tr>
                </table>

                <p class="table-header">District Info</p>
                <table class='district-data-css'>
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Total</th>
                    <th>Majority<br />Group</th>
                    <th>Majority <br /> Pop.</th>
                    <th>Opportunity District</th>
                    <th>Polsby Popper</th>
                    </tr>
                </thead>
                <tr id='distr-1'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-2'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-3'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-4'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-5'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-6'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-7'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-8'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-9'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-10'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-11'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-12'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-13'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-14' style={{display:"none"}}>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                </table>

                <p class="table-header">Census Demographics and 2020 Pres. Election (%)</p>
                <table class='district-data-css'>
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>White</th>
                    <th>Afr. American</th>
                    <th>Asian</th>
                    <th>Hispanic</th>
                    <th>Non-Hispanic</th>
                    <th>Dem. Pres. Vote</th>
                    <th>Rep. Pres. Vote</th>
                    </tr>
                </thead>
                <tr id='demo-perc-1'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-2'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-3'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-4'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-5'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-6'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-7'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-8'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-9'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-10'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-11'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-12'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-13'>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                <tr id='demo-perc-14' style={{display:"none"}}>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                    <td class='demoPercInfo'></td>
                </tr>
                </table>

                <p class="table-header">Census Demographics and 2020 Pres. Election</p>
                <table class='district-data-css'>
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>White</th>
                    <th>Afr. American</th>
                    <th>Asian</th>
                    <th>Hispanic</th>
                    <th>Non-Hispanic</th>
                    <th>Dem. Pres. Vote</th>
                    <th>Rep. Pres. Vote</th>
                    </tr>
                </thead>
                <tr id='demo-1'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-2'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-3'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-4'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-5'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-6'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-7'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-8'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-9'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-10'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-11'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-12'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-13'>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                <tr id='demo-14' style={{display:"none"}}>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                    <td class='demoInfo'></td>
                </tr>
                </table>
            </div>

            <div id='Comparison' class="tabcontent" style={{display:"none"}}>
                <p class="table-header" id='compare-table-header'>Compare Plan Statistics Overview</p>
                <table class='voting-data-css'>
                <thead>
                    <th>Plan Id</th>
                    <th>Pop. Equality</th>
                    <th>Polsby Popper</th>
                    <th>Num. of Opp. Districts</th>
                    <th>Objective Function</th>
                    <th>Num. of Dem. Districts</th>
                    <th>Num. of Rep. Districts</th>
                </thead>
                <tr id='enacted-plan' onClick={()=>openEnactedPlan()}>
                    <td class='enactInfo'></td>
                    <td class='enactInfo'></td>
                    <td class='enactInfo'></td>
                    <td class='enactInfo'></td>
                    <td class='enactInfo'></td>
                    <td class='enactInfo'></td>
                    <td class='enactInfo'></td>
                </tr>
                <tr id='seawulf-plan' onClick={()=>openChosenPlan()}>
                    <td class='chosenInfo'></td>
                    <td class='chosenInfo'></td>
                    <td class='chosenInfo'></td>
                    <td class='chosenInfo'></td>
                    <td class='chosenInfo'></td>
                    <td class='chosenInfo'></td>
                    <td class='chosenInfo'></td>
                </tr>
                <tr id='generated-plan' onClick={()=>openGeneratedPlan()}>
                    <td class='genInfo'></td>
                    <td class='genInfo'></td>
                    <td class='genInfo'></td>
                    <td class='genInfo'></td>
                    <td class='genInfo'></td>
                    <td class='genInfo'></td>
                    <td class='genInfo'></td>
                </tr>
                </table>
            </div>

       

        <div id='boxPlotContainer' style={{display:"none"}}>
            <div id="plot-dropdown">
                <div class="plotpViewDiv" >
                    <label id='plot-view'>Plot Type</label>
                </div>
                <div>
                    <select class="form-control" class="map-view-options" id='plot-view-dropdown' onChange={boxPlot}>
                        <option class="mapOptions" value="PresD" selected>Presedential Democratic</option>
                        <option class="mapOptions" value="PresR" >Presedential Republican</option>
                        
                        <option class="mapOptions" value="AfAmer" >African American</option>
                        <option class="mapOptions" value="White" >White</option>
                        <option class="mapOptions" value="Asian" >Asian</option>
                        <option class="mapOptions" value="His" >Hispanic</option>
                    </select>
                </div>
                <div id='boxPlot' class="tabcontent" style={{display:"block"}}></div>
            </div>
            
                <p class="table-header"></p>
            </div>

        </div>
        
    );
};

export default SideTable;
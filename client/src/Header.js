
import React from "react";
import './Header.css';
import { setDistrictView, setPrecinctView, setCountyView, map, currentState, districtView, precinctView, countyView, openState, resetMap, populationVar, setPopVar, currentLayerID, resetArrays} from './Map';
import { fillDistrictTable, fillTable, currentPlanPath, boxPlot } from "./SideTable";


var downloadPath;
if(currentState === 'ga')
  downloadPath = './data/georgia/plans/district-plan-ga0.geojson';
else 
    downloadPath = './data/north-carolina/plans/district-plan-nc0.geojson';

export function updateDownloadPath(path){
    document.getElementById('download-file').href = path;
    console.log(document.getElementById('download-file').href)
}

export function updateCurrentPlanText(plan){
    var text = document.getElementsByClassName('show-current-plan-text');
    text[0].innerText = plan;
    console.log("updateCurrentPlanText");
}
function showDistricts() {
    const checkBox = document.getElementById("show-district-checkbox");
    setDistrictView(checkBox.checked);
    const id = currentLayerID;
    console.log("SHUTTING OFF BOUNDARIES FOR "+id)
    if (districtView === false) {
        map.current.setLayoutProperty(id, 'visibility', 'none');
    } else if (districtView === true) {
        map.current.setLayoutProperty(id, 'visibility', 'visible');
    }
}

function showPrecincts() {
    const checkBox = document.getElementById("show-precinct-checkbox");
    setPrecinctView(checkBox.checked);
    const id = currentState + '_precincts';
    if (precinctView === false) {
        map.current.setLayoutProperty(id, 'visibility', 'none');
    } else if (precinctView === true) {
        map.current.setLayoutProperty(id, 'visibility', 'visible');
    }
}

function showCounties() {
    const checkBox = document.getElementById("show-county-checkbox");
    setCountyView(checkBox.checked);
    const id = currentState + '_counties_elections_outline';
    if (countyView === false) {
        map.current.setLayoutProperty(id, 'visibility', 'none');
    } else if (countyView === true) {
        map.current.setLayoutProperty(id, 'visibility', 'visible');
    }
}

/* State Drop Down Controls */
function stateDropDown() {
    const stateDropDown = document.getElementById('state-dropdown-options');
    const stateName = stateDropDown.value;
    console.log("current: " + currentLayerID);
    map.current.setLayoutProperty(currentLayerID, 'visibility', 'none');
    map.current.setLayoutProperty(currentLayerID+"_map", 'visibility', 'none');
    resetArrays();
    openState(stateName);
}

/* State Drop Down Controls */
function popDropDown() {
    console.log(populationVar);
    const popVarDropDown = document.getElementById('pop-dropdown-options');
    setPopVar(popVarDropDown.value);
    fillDistrictTable();
    fillTable();
    boxPlot();
}

// opens the left panel
function openNav() {
    document.getElementById("mySidenav").style.width = "300px";
}

// toggles the legend between viewable and hidden
function showLegend() {
    const checkBox = document.getElementById("show-legend-checkbox");
    var text = document.getElementById("legend");
    if (checkBox.checked === true) {
        text.style.display = "block";
    } else {
        text.style.display = "none";
    }
}

function refresh(){
    resetMap();
}

// Created the header component
const Header = () => {
    return (
        <div class='map-overlay-header' id="header">
            <div id="openNav" onClick={openNav}>
                <span >&#9776;</span>
            </div>

            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
            <div id="header-dropdown" >
                <div class="mapViewDiv">
                    <label id='state-view'>State</label>
                </div>
                <div>
                    <select class="form-control" class="map-view-options" id="state-dropdown-options" onChange={stateDropDown}>
                        <option value="choose-a-state" id=".state-dropdown" class="mapOptions">Choose a state</option>
                        <option value="ga" class="mapOptions">Georgia</option>
                        <option value="nc" class="mapOptions">North Carolina</option>
                    </select>
                </div>
            </div>
            <div id="header-dropdown" >
                <div class="popViewDiv">
                    <label id='pop-view'>Population</label>
                </div>
                <div>
                    <select class="form-control" class="map-view-options" id="pop-dropdown-options" onChange={popDropDown}>
                        <option value="total" class="mapOptions">Total</option>
                        <option value="vap" class="mapOptions">VAP</option>
                        <option value="vap" class="mapOptions" disabled="disabled">CVAP</option>
                    </select>
                </div>
            </div>

            <div id="show-legend">
                <label class="switch">
                    <input type="checkbox" id="show-legend-checkbox" onClick={showLegend} defaultChecked={true}></input>
                    <span class="slider"></span>
                </label>
                <label id='show-legend-text'>Legend</label>
            </div>

            <div id='show-districts' >
                <label class="switch">
                    <input type="checkbox" id="show-district-checkbox" onClick={showDistricts} defaultChecked={true}></input>
                    <span class="slider"></span>
                </label>
                <label id='show-district-text'>District Boundaries</label>
            </div>

            <div id='show-precincts'>
                <label class="switch">
                    <input type="checkbox" id="show-precinct-checkbox" onClick={showPrecincts} defaultChecked={true}></input>
                    <span class="slider"></span>
                </label>
                <label id='show-precinct-text'>Precinct Boundaries</label>
            </div>

            <div id='show-counties'>
                <label class="switch">
                    <input type="checkbox" id="show-county-checkbox" onClick={showCounties} defaultChecked={true}></input>
                    <span class="slider"></span>
                </label>
                <label id='show-county-text'>County Boundaries</label>
            </div>

            <div id ='export-geojson'>
                <button class="export-file"> 
                    <a id='download-file' href={downloadPath} download>Download GeoJSON</a>
                </button>
            </div>

            <div id='show-current-plan'>
                <p class="show-current-plan-text">No Plan</p>
            </div>

            <div id='reset-map'>
                <button id='refresh'><i class="fa fa-refresh" onClick={refresh}></i></button>
            </div>
        </div>
    );
};

export default Header;


import React from "react";
import './Legend.css';

// Legend Variables
const lineLayers = ['District','County','Precinct'];
const lineColors = ['#4bde72','#e8e8e8','#000000'];
const politicalLayers = ['Democratic','Republican'];
const politicalColors = ['#6193c7','#cf635d'];
// const legendColors = ["#bfff00", "#8258fa", "#faac58", "#f7819f", "#f2f5a9", "#045fb4", "#a9f5d0", "#5f04b4", "#f6cef5", "#cee3f6", "#088a08", "#df013a", "#df01a5", "#00bfff", "#e3cef6", "#ffff00", "#fcba03", "#59bfde", "#5971de", "#dede59", "#de3e3e", "#d559de", "#dae8a9", "#a9e8d6", "#848484" ];
// const districts = ['District 1', 'District 2', 'District 3', 'District 4', 'District 5', 'District 6', 'District 7', 'District 8', 'District 9', 'District 10', 'District 11', 'District 12', 'District 13', 'District 14', 'District 15', 'District 16', 'District 17', 'District 18', 'District 19', 'District 20', 'District 21', 'District 22', 'District 23', 'District 24', 'District 25'];
const legendColors = ["#bfff00", "#faac58", "#8258fa", "#f7819f", "#741b47", "#045fb4", "#a9f5d0", "#cee3f6", "#088a08", "#df013a", "#fcba03", "#00bfff", "#dae8a9", "#df01a5", "#848484" ];
const districts = ['District 1', 'District 2', 'District 3', 'District 4', 'District 5', 'District 6', 'District 7', 'District 8', 'District 9', 'District 10', 'District 11', 'District 12', 'District 13', 'District 14'];
// Clears all colors and labels form legend other than the top 3 lines
export function clearLegend() {
    var elements=document.getElementsByClassName('legend-item');
    while(elements.length > 3) {
        elements[elements.length-1].parentNode.removeChild(elements[elements.length-1]);
    }
}


// Sets up colors and labels for legend based on current state and which map is being viewed
export function districtLegend(state) {
    var numberOfDistricts;
    var colors, layers;
    if(state==null||state=='none') { // political data
        numberOfDistricts = 2; // not needed
        colors = politicalColors;
        layers = politicalLayers;
    } else { // district data
        colors = legendColors;
        layers = districts;
        if (state === 'ga') {
            numberOfDistricts = 14;
        } else if (state === 'nc') {
                numberOfDistricts = 13
        } else if (state === 'pa') {
            numberOfDistricts = 25
        }
    }
    setLegendVisuals(colors, layers, numberOfDistricts, false);
}


// sets the colors and labels on the legend
function setLegendVisuals(colors, layers, numberOfDistricts, isLine){
    const legend = document.getElementById('legend');
    layers.forEach((layer, i) => {
        if (i>=numberOfDistricts) // only go up to the max districts per state
            return;
        const color = colors[i];
        const item = document.createElement('div');
        const key = document.createElement('span');
        const value = document.createElement('span');
        item.className = "legend-item";
        key.className = 'legend-key';
        if (isLine) {
            key.style.height = '2px';
        }
        key.style.backgroundColor = color;
        value.innerHTML = `${layer}`;
        item.appendChild(key);
        item.appendChild(value);
        legend.appendChild(item);
    });
}


// Creates the first 3 line labels and colors for the legend, which remain there always
export function lineLegend() {
    setLegendVisuals(lineColors, lineLayers, 3, true);
}

// Create legend component
const Legend = () => {
    return (<div class='map-overlay' id='legend'></div>);
};

export default Legend;


// import 'mapbox-gl/dist/mapbox-gl.css';
// import './Map.css';
// import pennsylvaniaCounties from './data/penn/Original Data Files/pa-counties.geojson';
// import pennsylvaniaPrecinct from "./data/penn/processed/PA_PRECINCTS_ALL_INFO.geojson";
// import pennsylvaniaDistrict from "./data/penn/Original Data Files/pa_cd.geojson";
// import pennRedist1 from "./data/redistrictings/penn/redistr1.geojson";
// import pennRedist2 from "./data/redistrictings/penn/redistr2.geojson";
// import pennRedist3 from "./data/redistrictings/penn/redistr3.geojson";
// import { map, districtView, precinctView, countyView, addGeoJSONLayer } from './Map';
// import { gaLayers } from './GeorgiaMap';
// import { ncLayers } from './NCMap';
// import { pennCongElectLayers, pennDistrictLayers, pennElectionLayers } from './Constants';

// export var paLayers = [
//   'pa_counties_elections_outline',
//   'pa_counties_elections',
//   'pa_precincts',
//   'pa_congressional_districts',
//   'pa_original_districts',
//   'pa_congress_2020',
//   'pa_redistricting_1',
//   'pa_redistricting_1_map',
//   'pa_redistricting_2',
//   'pa_redistricting_2_map',
//   'pa_redistricting_3',
//   'pa_redistricting_3_map'];

// export function openPAMap() {
//   addGeoJSONLayer('pa', 'pa_counties_elections_outline', 'line', 'us_states_elections', 'county', 'election', pennsylvaniaCounties, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_counties_elections', 'fill', 'pa_counties_elections_outline', 'county', 'election', pennsylvaniaCounties, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_precincts', 'line', 'pa_counties_elections_outline', 'precinct', 'election', pennsylvaniaPrecinct, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_congressional_districts', 'line', 'pa_counties_elections_outline', 'district', 'election', pennsylvaniaDistrict, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_original_districts', 'fill', 'pa_congressional_districts', 'district', 'districtColors', pennsylvaniaDistrict, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_congress_2020', 'fill', 'pa_congressional_districts', 'district', 'cElection', pennsylvaniaDistrict, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_redistricting_1', 'line', 'pa_counties_elections_outline', 'district', 'district', pennRedist1, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_redistricting_1_map', 'fill', 'pa_redistricting_1', 'district', 'redistrColors', pennRedist1, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_redistricting_2', 'line', 'pa_counties_elections_outline', 'district', 'district', pennRedist2, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_redistricting_2_map', 'fill', 'pa_redistricting_2', 'district', 'redistrColors', pennRedist2, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_redistricting_3', 'line', 'pa_counties_elections_outline', 'district', 'district', pennRedist3, 'none', 6, 15);
//   addGeoJSONLayer('pa', 'pa_redistricting_3_map', 'fill', 'pa_redistricting_3', 'district', 'redistrColors', pennRedist3, 'none', 6, 15);
// }

// export function openPennsylvania(mapOption) {
//   if (mapOption === 'district-plans') {
//     updatePennLayers(pennDistrictLayers);
//   } else if (mapOption === 'congress-results') {
//     updatePennLayers(pennCongElectLayers);
//   } else {
//     updatePennLayers(pennElectionLayers);
//   }
// }

// export function updatePennLayers(penn){
//   const filteredArray = paLayers.filter(function (x) {
//     return penn.indexOf(x) < 0;
//   });
//   var others = gaLayers.concat(ncLayers);
//   others = others.concat(filteredArray);
//   for (const id of others) {
//     map.current.setLayoutProperty(id, 'visibility', 'none');
//   }
//   for (const id of penn) {
//     map.current.setLayoutProperty(id, 'visibility', 'visible');
//   }
//   if (districtView === false) {
//     map.current.setLayoutProperty('pa_congressional_districts','visibility','none');
//   }

//   if(precinctView === false) {
//     map.current.setLayoutProperty('pa_precincts', 'visibility', 'none');
//   }

//   if(countyView === false) {
//     map.current.setLayoutProperty('pa_counties_elections_outline', 'visibility', 'none');
//   }
// }


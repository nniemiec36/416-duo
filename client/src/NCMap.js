import 'mapbox-gl/dist/mapbox-gl.css';
import './Map.css';
import ncCounties from './data/north-carolina/Original Data Files/nc-counties.geojson';
import ncPrecinct from "./data/north-carolina/processed/NC_PRECINCTS_ALL_INFO.geojson";
import ncDistrict from "./data/north-carolina/Original Data Files/nc_cd.geojson";
import ncRedist1 from "./data/redistrictings/north-carolina/redistr1.geojson";
import ncRedist2 from "./data/redistrictings/north-carolina/redistr2.geojson";
import ncRedist3 from "./data/redistrictings/north-carolina/redistr3.geojson";
import { map, districtView, precinctView, countyView, addGeoJSONLayer, currentLayerID, setCurrentLayerID } from './Map';
import { gaLayers } from './GeorgiaMap';
import { paLayers } from './PennMap';
import { northCarolinaCongElectLayers, northCarolinaDistrictLayers } from './Constants';
import { updateCurrentPlanText } from './Header';

export var ncLayers = [
    'nc_counties_elections_outline',
    'nc_counties_elections',
    'nc_precincts',
    'nc_congressional_districts',
    'nc_congressional_districts_map',
    'nc_congress_2020',
    'nc_redistricting_1',
    'nc_redistricting_1_map',
    'nc_redistricting_2',
    'nc_redistricting_2_map',
    'nc_redistricting_3',
    'nc_redistricting_3_map'];

export function openNCMap() {
    addGeoJSONLayer('nc', 'nc_counties_elections_outline', 'line', 'us_states_elections', 'county', 'election', ncCounties, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_counties_elections', 'fill', 'nc_counties_elections_outline', 'county', 'election', ncCounties, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_precincts', 'line', 'nc_counties_elections_outline', 'precinct', 'election', ncPrecinct, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_congressional_districts', 'line', 'nc_counties_elections_outline', 'district', 'election', ncDistrict, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_congressional_districts_map', 'fill', 'nc_congressional_districts', 'district', 'districtColors', ncDistrict, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_congress_2020', 'fill', 'nc_congressional_districts', 'district', 'cElection', ncDistrict, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_redistricting_1', 'line', 'nc_counties_elections_outline', 'district', 'district', ncRedist1, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_redistricting_1_map', 'fill', 'nc_redistricting_1', 'district', 'redistrColors', ncRedist1, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_redistricting_2', 'line', 'nc_counties_elections_outline', 'district', 'district', ncRedist2, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_redistricting_2_map', 'fill', 'nc_redistricting_2', 'district', 'redistrColors', ncRedist2, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_redistricting_3', 'line', 'nc_counties_elections_outline', 'district', 'district', ncRedist3, 'none', 6, 15);
    addGeoJSONLayer('nc', 'nc_redistricting_3_map', 'fill', 'nc_redistricting_3', 'district', 'redistrColors', ncRedist3, 'none', 6, 15);
}

export function updateNorthCarolinaLayers(nc){
    const filteredArray = ncLayers.filter(function (x) {
        return nc.indexOf(x) < 0;
    });
    var others = gaLayers.concat(paLayers);
    others = others.concat(filteredArray);

    for (const id of others) {
        map.current.setLayoutProperty(id, 'visibility', 'none');
    }

    for (const id of nc) {
        map.current.setLayoutProperty(id, 'visibility', 'visible');
    }

    if (districtView === false) {
        map.current.setLayoutProperty(
            'nc_congressional_districts',
            'visibility',
            'none'
        );
    }

    if(precinctView === false) {
        map.current.setLayoutProperty(
            'nc_precincts', 'visibility', 'none'
        );
    }

    if(countyView === false) {
        map.current.setLayoutProperty(
            'nc_counties_elections_outline', 'visibility', 'none'
        );
    }
}

export function openNorthCarolina(mapOption) {
    if (mapOption === 'district-plans') {
    updateCurrentPlanText('Plan: NC0')
     setCurrentLayerID("nc_congressional_districts");
     console.log("id "+currentLayerID);
        updateNorthCarolinaLayers(northCarolinaDistrictLayers);
    } else if (mapOption === 'congress-results') {
        updateNorthCarolinaLayers(northCarolinaCongElectLayers);
    } else {
        updateNorthCarolinaLayers(northCarolinaCongElectLayers);
    }
}

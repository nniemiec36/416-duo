import precincts from './NC_PRECINCTS_GEOMETRY2.json';
import census from './processed/NC_CENSUS.json'
import fs from "fs";

for(var i = 0; i<precincts.features.length; i++){
    var pfeat = precincts.features[i].properties;
    var precinctGeoid = pfeat.GEOID20;
    for(var j = 0; j < census.features.length; j++){
        var cfeat = census.features[j].properties;
        var cb_id = cfeat.STATEID + cfeat.COUNTYID + cfeat.VTD;
        if(precinctGeoid === cb_id){
            precincts.features[i].properties.DistrictID=cfeat.DISTRICTID;
            var row = JSON.stringify(precincts.features[i]);
            fs.appendFileSync('./NC_PRECINCTS.json', row + ", \n");
            break;
        }
    }
}

console.log("done");

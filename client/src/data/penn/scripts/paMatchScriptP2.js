import prec from '../processed/PA_PRECINCTS.json';
// // import csv from './ga_2020_2020_vtd.csv'
import elec from './pa_2016_2020.json';
import fs from "fs"; 

var count = 0;
for(var i =0;i<prec.features.length;i++){
    var p = prec.features[i];
    var pgeoid = p.properties.GEOID20;
    var found = false;
    for( var j = 0; j <elec.features.length;j++ ){
        var erow =elec.features[j];
        var egeoid = erow.GEOID;
        if(pgeoid===egeoid){
            found = true;
            var PRESREP = erow.PRESREP;
            var PRESDEM = erow.PRESDEM;
            var PRESTOT =erow.PRESTOT;
            var SENATER = erow.SENATER;
            var SENATED = erow.SENATED;
            var SENATETOT = erow.SENATETOT;
            p.properties.STATE = p.properties.STATEFP20;
            delete p.properties.STATEFP20;
            p.properties.COUNTY = p.properties.COUNTYFP20;
            delete p.properties.COUNTYFP20;
            p.properties.VTD = p.properties.VTDST20;
            delete p.properties.VTDST20;
            p.properties.GEOID = pgeoid;
            delete p.properties.GEOID20;
            p.properties.NAME = p.properties.NAME20;
            delete p.properties.NAME20;
            delete p.properties.VTDI20;
            delete p.properties.NAMELSAD20;
            delete p.properties.LSAD20;
            delete p.properties.MTFCC20;
            delete p.properties.FUNCSTAT20;
            delete p.properties.ALAND20;
            delete p.properties.AWATER20;
            delete p.properties.INTPTLAT20;
            delete p.properties.INTPTLON20;
            p.properties.PRESREP = PRESREP;
            p.properties.PRESDEM = PRESDEM;
            p.properties.PRESTOT = PRESTOT;
            p.properties.SENATER = SENATER;
            p.properties.SENATED = SENATED;
            p.properties.SENATETOT = SENATETOT;
            console.log(p);
            break;
        }
    }
    if(!found){
        count++;
    }
}
console.log(count);
fs.writeFile('PA_PRECINCTS_ALL_INFO.json', JSON.stringify(prec), (err) => {
    if (err) {
        throw err;
    }
    console.log("JSON data is saved.");
});




// CODE TO TURN CSV INTO JSON
// var json = {features:[]};
// fs.readFile('./pa_2016_2020_vtd.csv', (err, data) => {
//     if (err) throw err;
//     var arr = (data.toString()).split('\n');
//     for(var i =0;i<arr.length;i++){
//         var string = arr[i];
//         var row = string.split(',');
//         var GEOID= row[0];
//         var PRESREP = parseInt(row[5],10);
//         var PRESDEM = parseInt(row[4],10);
//         var PRESTOT = PRESREP + PRESDEM + parseInt(row[6],10)+ parseInt(row[7],10) + parseInt(row[8],10);
//         var SENATER = parseInt(row[10],10);
//         var SENATED = parseInt(row[9],10);
//         var SENATETOT = SENATER + SENATED + parseInt(row[11],10);
//         var obj = new Object();
//         obj.GEOID = GEOID;
//         obj.PRESREP = PRESREP;
//         obj.PRESDEM = PRESDEM;
//         obj.PRESTOT = PRESTOT;
//         obj.SENATER = SENATER;
//         obj.SENATED = SENATED;
//         obj.SENATETOT = SENATETOT;
//         var jsonRow = JSON.stringify(obj)+", \n";
//         console.log(jsonRow);
//         fs.appendFileSync('pa_2016_2020.json', jsonRow, (err) => {
//             if (err) {
//                 throw err;
//             }
//             console.log("JSON data is saved.");
//         });
//         // sleep(2);
//         // fs.close();
//     }
// })

// function sleep(milliseconds) {
//     var start = new Date().getTime();
//     for (var i = 0; i < 1e7; i++) {
//       if ((new Date().getTime() - start) > milliseconds){
//         break;
//       }
//     }
//   }


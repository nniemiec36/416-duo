import precincts from './processed/PA_PRECINCTS_P2.json';
import census3 from './processed/broken-up-data/PA_CENSUS_3.json';
import fs from "fs";

for(var i = 0; i<precincts.features.length; i++){
    var pfeat = precincts.features[i].properties;
    var precinctGeoid = pfeat.GEOID20;
    //var precFoundC1 = false;
    var precFoundC3 = false;
    for(var j = 0; j < census3.features.length; j++){
        var cfeat = census3.features[j].properties;
        var cb_id = cfeat.STATEID + cfeat.COUNTYID + cfeat.VTD;
        //console.log(cb_id);
        //console.log(precinctGeoid);
        if(precinctGeoid === cb_id){
            precFoundC3 = true;
            //console.log("true");
            precincts.features[i].properties.DistrictID=cfeat.DISTRICTID;
            var row = JSON.stringify(precincts.features[i]);
            fs.appendFileSync('./processed/PA_PRECINCTS_P3.json', row + ", \n");
            break;
        }
    }

    if(precFoundC3 === false){
        var row = JSON.stringify(precincts.features[i]);
        fs.appendFileSync('./processed/PA_PRECINCTS_P3.json', row + ", \n");
    }
}

console.log("done");

import precincts from '../PA_PRECINCTS_GEOMETRY2.json';
import census1 from '../processed/broken-up-data/PA_CENSUS_1.json';
import fs from "fs";
//console.log(census1);

for(var i = 0; i<precincts.features.length; i++){
    var pfeat = precincts.features[i].properties;
    var precinctGeoid = pfeat.GEOID20;
    var precFoundC1 = false;
    for(var j = 0; j < census1.features.length; j++){
        var cfeat = census1.features[j].properties;
        var cb_id = cfeat.STATEID + cfeat.COUNTYID + cfeat.VTD;
        //console.log(cb_id);
        //console.log(precinctGeoid);
        if(precinctGeoid === cb_id){
            precFoundC1 = true;
            //console.log("true");
            precincts.features[i].properties.DistrictID=cfeat.DISTRICTID;
            var row = JSON.stringify(precincts.features[i]);
            fs.appendFileSync('./processed/PA_PRECINCTS_P1.json', row + ", \n");
            break;
        }
    }

    if(precFoundC1 === false){
        var row = JSON.stringify(precincts.features[i]);
        fs.appendFileSync('./processed/PA_PRECINCTS_P1.json', row + ", \n");
    }

}

console.log("done");

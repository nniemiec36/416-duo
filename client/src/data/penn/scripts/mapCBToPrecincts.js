import precincts from './PA_PRECINCTS_GEOMETRY2.json';
import fs from "fs";
import JSONStream from 'JSONStream';
import es from "event-stream";

var json = { type: "FeatureCollection", features: [] }; // new census json file

var getStream = function () {
    var jsonData = './pa_blocks_20.json',
        stream = fs.createReadStream(jsonData, { encoding: 'utf8' }),
        parser = JSONStream.parse('*.*');
    return stream.pipe(parser);
};

getStream()
    .pipe(es.mapSync(function (data) { // for each census block
        var dist = data.properties.CD116;
        //console.log(dist);
        var cb_id = "";
        cb_id = cb_id.concat(data.properties.STATEFP20);
        //console.log(data.properties.STATEFP20);
        cb_id = cb_id.concat(data.properties.COUNTYFP20);
        cb_id = cb_id.concat(data.properties.VTD);
        var foundPrec = false;
        //console.log(cb_id);
        for(var i = 0; i<precincts.features.length;i++){ // for each precinct
            var pfeat = precincts.features[i].properties;
            var precinctGeoid = pfeat.GEOID20;
            //console.log("precinct geo: " + precinctGeoid);
            //console.log("cb_id: " + cb_id);
            if(precinctGeoid === cb_id){ // this census block is in this precinct
                //console.log("equal");
                foundPrec = true;
                var state = data.properties.STATEFP20;
                var county = data.properties.COUNTYFP20;
                var vtd = data.properties.VTD;
                var block = data.properties.BLOCKCE20;
                var precinctid = precincts.features[i].properties.PrecinctID;
                var totalPop =data.properties.P0020001;
                var totalHis = data.properties.P0020002;
                var totalNonHis = data.properties.P0020003;
                var totalAsian = data.properties.P0020008;
                var totalWhite = data.properties.P0020005;
                var totalAA = data.properties.P0020006;
    
                var totalVAP = data.properties.P0040001;
                var totalVAPHis = data.properties.P0040002;
                var totalVAPNonHis = data.properties.P0040003;
                var totalVAPAsian = data.properties.P0040008;
                var totalVAPWhite = data.properties.P0040005;
                var totalVAPAA = data.properties.P0040006;
                addRowToJSON(block, state, county, vtd, precinctid, dist, totalPop, totalHis, totalNonHis, totalAsian, totalWhite, totalAA, 
                    totalVAP, totalVAPHis, totalVAPNonHis, totalVAPAsian, totalVAPWhite, totalVAPAA, data.geometry.coordinates, json);
                break;
            }
            if(i === precincts.features.length-1)
                if(foundPrec === false)
                    console.log(cb_id + " not matched");

        }
        
}));

function addRowToJSON(block, state, county, vtd, precinct, dist, totalPop, totalHis, totalNonHis, totalAsian, totalWhite, totalAA, totalVAP, totalVAPHis,
    totalVAPNonHis, totalVAPAsian, totalVAPWhite, totalVAPAA, Coords, json){
    var jsonRow = { type: "Feature", 
                  properties: {"BLOCKID": block, "STATEID": state, "COUNTYID": county, "VTD": vtd, "PRECINCTID": precinct, "DISTRICTID": dist, 
                                "TOTALPOP": totalPop, "TOTALHIS": totalHis, "TOTALNONHIS": totalNonHis, "TOTALASIAN": totalAsian, "TOTALWHITE": totalWhite,
                                "TOTALAA": totalAA, "TOTALVAP": totalVAP, "TOTALVAPHIS": totalVAPHis, "TOTALVAPNONHIS": totalVAPNonHis, 
                                "TOTALVAPASIAN": totalVAPAsian, "TOTALVAPWHITE": totalVAPWhite, "TOTALVAPAA": totalVAPAA},
                  geometry:{
                    type: "Polygon",
                      coordinates: Coords
                  }
                };
    // console.log(jsonRow);
    jsonRow = JSON.stringify(jsonRow);
    //console.log(jsonRow);
    fs.appendFileSync('./processed/PA_CENSUS.json', jsonRow + ", \n");
    //json.features.push(jsonRow);
  }
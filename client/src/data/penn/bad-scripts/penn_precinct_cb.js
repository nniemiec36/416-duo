import census12 from './pa_blocks_d12.json';
import precincts from './pa_prec_d12.json';
import fs from "fs"; 
import JSONStream from 'JSONStream';
import es from "event-stream";
import StreamArray from 'stream-json/streamers/StreamArray.js';
import census from './dist_and_prec_from_blocks.json';
import * as turf from '@turf/turf'

// georgia precinct geometries (precinct id, district id, geometry)
// "STATEID", "PRECINCTID", "PRECINCTNAME", "DISTRICTID", "GEOMETRY"
var json = {type:"FeatureCollection", 
            features: []};

var cfeatures = census12.features;
var pfeatures = precincts.features;
var numIssues = 0;
for(var i = 0; i < cfeatures.length; i++){ // for each census block in dist 12
  var block = cfeatures[i].geometry; // geometry of each row in precincts
  //console.log(i);
  var blockID = i;
  var area = turf.area(turf.polygon(block.coordinates));
  var count = 0;
  var found = false;
  var turfErrorFlag = false;
  if(i%500 ==0){
      console.log("------------------------------------------------------------------- AT "+i);
  }
  for(var j = 0; j < pfeatures.length; j++){
    var prec = pfeatures[j].geometry;
    var precID = pfeatures[j].properties.PRECINCTID;
    var overlap = null;
    try{  
        overlap = turf.intersect(block, prec);
    }catch{
      console.log("--- error block #"+i+" name "+blockID);
      turfErrorFlag = true;
      break;
    }

    if(overlap===null)
        continue; //  no overlap check the next
    if(overlap.geometry.type=="MultiPolygon"){ 
          overlap = turf.multiPolygon(overlap.geometry.coordinates);
    }else{
        overlap = turf.polygon(overlap.geometry.coordinates);
    }
    overlap = turf.area(overlap); // get area of overlap
    count++;
    var valid = (overlap/area).toPrecision(5)*100.0;
    //console.log(valid);
    if(valid > 50){ // more than half is in one precinct
        found =true;
        break;
        //console.log(" FOUND block #"+i+" is in prec "+precID);
        // ADD TO JSON: "PRECINCTID", "PRECINCTNAME", "DISTRICTID", "STATEID", "GEOMETRY"
        //addRowToJSON(blockID, blockID, precID, stateID, block.coordinates, json);
    }
  }
  if(found === false){
      console.log("error none at 60 for "+blockID);
      //addRowToJSON(blockID, blockID, null, stateID, block.coordinates, json); // manually input dist id
  }
}
// CREATE JSON
const data = JSON.stringify(json);
// write JSON string to a file
fs.writeFile('PA_PRECINCTS_GEOMETRY.json', data, (err) => {
    if (err) {
        throw err;
    }
    console.log("JSON data is saved.");
});

function addRowToJSON(PRECINCTID, PRECINCTNAME, DISTRICTID, STATEID, Coords, json){
  var jsonRow = {type: "Feature", 
                properties:{PRECINCTID: PRECINCTID, PRECINCTNAME: PRECINCTNAME, DISTRICTID: DISTRICTID, STATEID: STATEID},
                geometry:{
                  type: "Polygon",
                    coordinates: Coords
                }
              };
  json.features.push(jsonRow);
}
 


# P1 = Race for the total population
# P0010001 = total pop
# P0010002 = population of one race
# P0010003 = white alone
# P0010004 = black/aa alone
# P0010005 = american indian and alaska native alone
# P0010006 = asian alone
# P0010007 = native hawaiian and other pacific islander alone
# P0010008 = some other race alone
# P2 = Race for the total population, hispanic or latino
# P0020001 = Total
# P0020002 = Hispanic or Latino
# P0020003 = not hispanic or latino
# P0020004 = population of one race
# P0020005 = white alone
# P0020006 = black/aa alone
# P0020007 = american indian and alaska native alone
# P0020008 = asian alone
# P0020009 = native hawaiian and other pacific islander alone
# P0020010 = some other race alone
# P3 = Race for the Population 18 years and older
# P0030001 = total pop
# P0030002 = population of one race
# P0030003 = white alone
# P0030004 = black/aa alone
# P0030005 = american indian and alaska native alone
# P0030006 = asian alone
# P0030007 = native hawaiian and other pacific islander alone
# P0030008 = some other race alone
# P4 = Race for the Population 18 years and older, hispanic or latino
# P0040001 = Total
# P0040002 = Hispanic or Latino
# P0040003 = not hispanic or latino
# P0040004 = population of one race
# P0040005 = white alone
# P0040006 = black/aa alone
# P0040007 = american indian and alaska native alone
# P0040008 = asian alone
# P0040009 = native hawaiian and other pacific islander alone
# P0040010 = some other race alone

# # P1 columns
# p1_columns = {"P0010001":"TOTAL_POP", "P0010002":"TOTAL_POP_1R", "P0010003":"TOTAL_WHITE", "P0010004":"TOTAL_BLACK", "P0010005":"TOTAL_NATIVE", "P0010006":"TOTAL_ASIAN", "P0010007":"TOTAL_NHPI", "P0010008":"TOTAL_OTHER"}

# # P2 columns
# p2_columns = {"P0020001":"TOTAL_POP", "P0020002":"TOTAL_HIS_OR_LAT", "P0020003":"TOTAL_NOT_LAT", "P0020004":"TOTAL_VAP_1R","P0020005":"TOTAL_WHITE_HL", "P0020006":"TOTAL_BLACK_HL", "P0020007":"TOTAL_NATIVE_HL", "P0020008":"TOTAL_ASIAN_HL", "P0020009":"TOTAL_NHPI_HL", "P0020010":"TOTAL_OTHER_HL"}

# # P3 columns
# p3_columns = {"P0030001":"TOTAL_VAP", "P0030002":"TOTAL_VAP_1R", "P0030003":"TOTAL_WHITE_VAP", "P0030004":"TOTAL_BLACK_VAP", "P0030005":"TOTAL_NATIVE_VAP", "P0030006":"TOTAL_ASIAN_VAP", "P0030007":"TOTAL_NHPI_VAP", "P0030008":"TOTAL_OTHER_VAP"}

# # P4 columns
# p4_columns = {"P0040001":"TOTAL_VAP_HL", "P0040002":"TOTAL_VAP_HIS_OR_LAT", "P0040003":"TOTAL_NOT_LAT_VAP", "P0040004":"TOTAL_VAP_HL_1R", "P0040005":"TOTAL_WHITE_VAP_HL", "P0040006":"TOTAL_BLACK_VAP_HL", "P0040007":"TOTAL_NATIVE_VAP_HL", "P0040008":"TOTAL_ASIAN_VAP_HL", "P0040009":"TOTAL_NHPI_HL", "P0040010":"TOTAL_OTHER_VAP_HL"}

import numpy as np
import pandas as pd
import geopandas as gpd

# processing the district info
pa_district_data = gpd.read_file('pa_cd.geojson')
pa_district_data['CD116FP'].rename(columns={'CD116FP':'CD116'})
pa_district_data.to_file("pa_district_data.geojson", driver="GeoJSON")

# change "STATEFP20" to "STATE" and "13" to "GA"
# change "GEOID20" to "GEOID"
# change "CD116FP" to "DISTRICT ID"
# change P2 columns
# p2_columns = {"P0020001":"TOTAL_POP", "P0020002":"TOTAL_HIS_OR_LAT", "P0020003":"TOTAL_NOT_LAT", "P0020004":"TOTAL_VAP_1R","P0020005":"TOTAL_WHITE", "P0020006":"TOTAL_BLACK", "P0020007":"TOTAL_NATIVE", "P0020008":"TOTAL_ASIAN", "P0020009":"TOTAL_NHPI", "P0020010":"TOTAL_OTHER"}
# # change "COUNTYFP20" to "COUNTY"
# add "DISTRICTPLANID" set all to "ENACTED"
# change P4 columns
# p4_columns = {"P0040001":"TOTAL_VAP", "P0040002":"TOTAL_VAP_HIS_OR_LAT", "P0040003":"TOTAL_NOT_LAT_VAP", "P0040004":"TOTAL_VAP_1R", "P0040005":"TOTAL_WHITE_VAP", "P0040006":"TOTAL_BLACK_VAP", "P0040007":"TOTAL_NATIVE_VAP", "P0040008":"TOTAL_ASIAN_VAP", "P0040009":"TOTAL_NHPI", "P0040010":"TOTAL_OTHER_VAP"}

# processing the precinct info
# pa_precinct_info = gpd.read_file('./data/penn/pa_precincts.geojson')
# change "STATEFP20" to "STATE" and "13" to "GA"
# change "GEOID20" to "GEOID"
# change "CD116FP" to "DISTRICT ID"
# change P2 columns
# p2_columns = {"P0020001":"TOTAL_POP", "P0020002":"TOTAL_HIS_OR_LAT", "P0020003":"TOTAL_NOT_LAT", "P0020004":"TOTAL_VAP_1R","P0020005":"TOTAL_WHITE", "P0020006":"TOTAL_BLACK", "P0020007":"TOTAL_NATIVE", "P0020008":"TOTAL_ASIAN", "P0020009":"TOTAL_NHPI", "P0020010":"TOTAL_OTHER"}
# # change "COUNTYFP20" to "COUNTY"
# change P4 columns
# p4_columns = {"P0040001":"TOTAL_VAP", "P0040002":"TOTAL_VAP_HIS_OR_LAT", "P0040003":"TOTAL_NOT_LAT_VAP", "P0040004":"TOTAL_VAP_1R", "P0040005":"TOTAL_WHITE_VAP", "P0040006":"TOTAL_BLACK_VAP", "P0040007":"TOTAL_NATIVE_VAP", "P0040008":"TOTAL_ASIAN_VAP", "P0040009":"TOTAL_NHPI", "P0040010":"TOTAL_OTHER_VAP"}
# import 
# census_blocks = gpd.loads('pa_blocks_2020.geojson');
# if "geometry" in census_blocks:
#     for geometry in census_blocks:

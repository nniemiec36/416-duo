import precincts from '../Original Data Files/ga_vtd_2020_bound.json';
import fs from "fs"; 

// adds a given ID number to each precinct in the file for later useage
for(var i =0;i<precincts.features.length;i++){
    precincts.features[i].properties.PrecinctID=""+i+"";
    precincts.features[i].properties.DistrictID="";
}

var precincts2 = JSON.stringify(precincts);

fs.writeFile('GA_PRECINCTS_RENUMBER.json', precincts2, (err) => {
    if (err) {
        throw err;
    }
    console.log("JSON data is saved.");
});

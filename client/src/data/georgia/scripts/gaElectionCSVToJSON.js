var json = {features:[]};
fs.readFile('./ga_2020_2020_vtd.csv', (err, data) => {
    if (err) throw err;
    var arr = (data.toString()).split('\n');
    for(var i =0;i<arr.length;i++){
        var string = arr[i];
        var row = string.split(',');
        var GEOID= row[0];
        var PRESREP = parseInt(row[4],10);
        var PRESDEM = parseInt(row[5],10);
        var PRESTOT = PRESREP + PRESDEM + parseInt(row[6],10);
        var SENATER = parseInt(row[7],10);
        var SENATED = parseInt(row[8],10);
        var SENATETOT = SENATER + SENATED + parseInt(row[9],10);
        var jsonRowObj = new Object();
        jsonRowObj.GEOID = GEOID;
        jsonRowObj.PRESREP = PRESREP;
        jsonRowObj.PRESDEM = PRESDEM;
        jsonRowObj.PRESTOT = PRESTOT;
        jsonRowObj.SENATER = SENATER;
        jsonRowObj.SENATED = SENATED;
        jsonRowObj.SENATETOT = SENATETOT;
        var jsonRow = JSON.stringify(jsonRowObj)+", /n";
        fs.appendFile('ga_2020_2020.json', jsonRow, (err) => {
            if (err) {
                throw err;
            }
            console.log("JSON data is saved.");
        });
    }
})



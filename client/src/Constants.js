export const electionRange = {
    "fill-color": [
      "match",
      ["get", "Winner"],
      "Donald J Trump",
      "#cf635d",
      "Joseph R Biden Jr",
      "#6193c7",
      "Other",
      "#91b66e",
      "#eff1f1",
    ],
    "fill-outline-color": "#ffffff",
    "fill-opacity": [
        "step",
        ["get", "WnrPerc"],
        0.3,
        0.4,
        0.5,
        0.5,
        0.7,
        0.6,
        0.9,
    ],
  };
export const congressElection = {
  "fill-color": [
    "match",
    ["get", "WINNER"],
    "R",
    "#cf635d",
    "D",
    "#6193c7",
    "#eff1f1",
  ],
  "fill-outline-color": "#000000",
};

export const districtColors = {
    "fill-color": [
      "match",
      ["get", "DistrictID"],
      "01", "#bfff00",
      "02", "#faac58",
      "03", "#8258fa",
      "04", "#f7819f",
      "05", "#741b47",
      "06", "#045fb4",
      "07", "#a9f5d0",
      "08", "#cee3f6",
      "09", "#088a08",
      "10", "#df013a",
      "11", "#fcba03",
      "12", "#00bfff",
      "13", "#dae8a9",
      "14", "#df01a5",
      "#eff1f1",
    ],
    "fill-outline-color": "#000000",
    "fill-opacity": 0.6,
  };

  export const redistrColors = {
    "fill-color": [
      "match",
      ["get", "CD116FP"],
      "01", "#bfff00",
      "02", "#faac58",
      "03", "#8258fa",
      "04", "#f7819f",
      "05", "#741b47",
      "06", "#045fb4",
      "07", "#a9f5d0",
      "08", "#cee3f6",
      "09", "#088a08",
      "10", "#df013a",
      "11", "#fcba03",
      "12", "#00bfff",
      "13", "#dae8a9",
      "14", "#df01a5",
      "#eff1f1",
    ],
    "fill-outline-color": "#000000",
    "fill-opacity": 0.6,
  };


  export const georgiaMap = {
    "lng": -82.441162,
    "lat": 32.647875
  }

  export const northCarolinaMap = {
    "lng": -78.793457,
    "lat": 35.782169
  }

  const lineLayers = ['District','County','Precinct'];
  const lineColors = ['#4bde72','#e8e8e8','#000000'];

  export const precinctLines = {
    "line-color": "#444444",
    "line-width": 0.30,
  };

  export const countyLines = {
    "line-color": "#ffffff",
    "line-width": 0.20,
  };

  export const districtLines = {
    "line-color": "#00ff00",
    "line-width": 0.90,
  };

  export const stateLines = {
    "line-color": "#ffffff",
    "line-width": 0.8,
  };

  export const georgiaElectionLayers = ['ga_counties_elections','ga_counties_elections_outline','ga_precincts','ga_congressional_districts'];
  export const georgiaDistrictLayers =['ga_counties_elections_outline', 'ga_precincts', 'ga_congressional_districts', 'ga_congressional_districts_map'];
  export const georgiaCongElectLayers = ['ga_counties_elections_outline','ga_precincts','ga_congressional_districts','ga_congress_2020'];

  export const northCarolinaElectionLayers = ['nc_counties_elections_outline', 'nc_counties_elections', 'nc_precincts_18', 'nc_congressional_districts'];
  export const northCarolinaDistrictLayers = ['nc_counties_elections_outline', 'nc_precincts', 'nc_congressional_districts', 'nc_congressional_districts_map'];
  export const northCarolinaCongElectLayers = ['nc_counties_elections_outline', 'nc_precincts', 'nc_congressional_districts', 'nc_congress_2020'];




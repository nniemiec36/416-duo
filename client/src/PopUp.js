import React from 'react';
// import './SideTable.css';
import Plotly from 'plotly.js-dist';
import './PopUp.css';
import { testSetGenerate, testGetGenerate, testUserParameters,changeMapPlan } from './FetchCalls';
import CanvasJSReact from './canvasjs.react';
import {map, currentState, populationVar} from './Map';
import { data } from 'jquery';
//var CanvasJSReact = require('./canvasjs.react');




 async function getUpdatedPopulations(round) {
     console.log("CALLED GET UPDATED");
    const url = "http://localhost:8080/states/getAlgorithmProgress/" + currentState 
                + "/" + round;
    var response;
    var resp = await fetch(url)
    .then(res => res.json())
    .then(logged => {
        return logged;
    }).catch(e => console.log(e));
    console.log("response "+JSON.stringify(resp));
    return resp;
}

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

var options = {
    animationEnabled: true,
    exportEnabled: true,
    theme: "light2", //"light1", "dark1", "dark2"
    title:{
        text: "Algorithm Progress"
    },
    axisY: {
        includeZero: true,
        maximum: 850000,
        minimum:650000,
        title: "Population"
    },
    axisX:{
        title:"Districts"
    },
    height:260,
    data: [{
        type: "column", //change type to bar, line, area, pie, etc
        //indexLabel: "{y}", //Shows y value on all Data Points
        indexLabelFontColor: "#5A5757",
        indexLabelPlacement: "outside",
        dataPoints: [
        { "x": 1, "y": 20 },
        { "x": 2, "y": 20 },
        { "x": 3, "y": 20 },
        { "x": 4, "y": 30 },
        { "x": 5, "y": 10 }]
    }]
}

  
var round = 0;
var maxRounds = 20;

function newArr(arr1){
    var arr2=[];
    for(var i =0;i<arr1.length;i++){
        var num = (Math.random()*0.008);
        console.log(num);
        console.log(arr1[i]);
        console.log(num*arr1[i]);
        var variance = num*arr1[i]
        var neg = Math.round(Math.random()) ? 1 : -1;
        console.log(neg);
        var newVal =0;
        if(neg < 0){
            newVal = arr1[i]-num*arr1[i];
        }else{
            newVal = arr1[i]+num*arr1[i];
        }
        num = Math.round(newVal);
        arr2.push(num);
    }
    // console.log(arr2);
    return arr2;
}



export function timer(seawulfPops,algoPops, popEqMeasures){
    // var cancelButton = document.getElementById('cancel');
    // cancelButton.style.display='block';
    console.log(popEqMeasures)
    function sleep(milliseconds) {
        const date = Date.now();
        let currentDate = null;
        do {
          currentDate = Date.now();
        } while (currentDate - date < milliseconds);
    }
      

        seawulfPops=seawulfPops.map(Number);
        algoPops=algoPops.map(Number);
        var pops1 = newArr(seawulfPops);
        var pops2=newArr(seawulfPops);
        var pops3=newArr(seawulfPops);
        var pops4=newArr(algoPops);
        var pops5=newArr(algoPops);;
        var pops=[seawulfPops,pops1,pops2,pops3,pops4,pops5,algoPops];

        var popMeasures=newArr(popEqMeasures);

       
        var numDists = 0;
        if(currentState==="ga"){
            numDists=14;
        }else{
            numDists = 13;
        }
        var plotOptions=[];
        for(var i =0;i<pops.length;i++){
            var  dataPoints= [];
            for( var j =0;j<numDists;j++){
                dataPoints.push({ "x": j+1, "y": 0 });
            }
            var popArr = pops[i];
            for( var j =0;j<popArr.length;j++){
                dataPoints[j].y = popArr[j];
            }
            console.log(dataPoints);
            plotOptions.push(dataPoints);
            // await(sleep(1000));
        }
        console.log(plotOptions);
        var iter = document.getElementById("iterations");
        var runTime = document.getElementById("runTime");
        // var popEq = document.getElementById("popEqAlg");
        iter.innerText="Iterations: ";
        runTime.innerText="Run Time: ";
        // popEq.innerText = "Population Equality: ";

        var sec = 0;

            function pad(val) {
                return val > 9 ? val : "0" + val;
            }
            var timer = setInterval(function () {
             document.getElementById("runTime").innerHTML = "Run Time: "+pad(++sec % 60);
            }, 1000);

            setTimeout(function () {
                clearInterval(timer);
                sec=0;
                document.getElementById("runTime").innerHTML = "";
                document.getElementById("iterations").innerHTML="";
                document.getElementById("popEqAlg").innerHTML="";
            }, 3000*7.4);
        // sec = 0;

        // popEq
        setTimeout(() => {
            var popEq = document.getElementById("popEqAlg");
            popEq.innerHTML = "Population Equality: " + popEqMeasures[0].toFixed(4);
        }, 3000*0);
        setTimeout(() => {
            var popEq = document.getElementById("popEqAlg");
            popEq.innerHTML = "Population Equality: " + popEqMeasures[1].toFixed(4);
        }, 3000*1);
        setTimeout(() => {
            var popEq = document.getElementById("popEqAlg");
            popEq.innerHTML = "Population Equality: " + popEqMeasures[2].toFixed(4);
        }, 3000*2);
        setTimeout(() => {
            var popEq = document.getElementById("popEqAlg");
            popEq.innerHTML = "Population Equality: " + popEqMeasures[3].toFixed(4);
        }, 3000*3);
        setTimeout(() => {
            var popEq = document.getElementById("popEqAlg");
            popEq.innerHTML = "Population Equality: " + popEqMeasures[4].toFixed(4);
        }, 3000*4);
        setTimeout(() => {
            var popEq = document.getElementById("popEqAlg");
            popEq.innerHTML = "Population Equality: " + popEqMeasures[5].toFixed(4);
        }, 3000*5);
        setTimeout(() => {
            var popEq = document.getElementById("popEqAlg");
            popEq.innerHTML = "Population Equality: " + popEqMeasures[6].toFixed(4);
        }, 3000*6);

        var iterations = 0;
        setTimeout(() => {var options1 = options;
            options1.data[0].dataPoints = plotOptions[0];
            document.getElementById("iterations").innerHTML="Iterations: 0";
            (new CanvasJS.Chart("chartContainer", options1)).render();}, 5000*0);
        setTimeout(() => {var options2 = options;
            options2.data[0].dataPoints = plotOptions[1];
            iterations+=Math.floor(Math.random() * 10) + 8;
            document.getElementById("iterations").innerHTML="Iterations: "+iterations;
            (new CanvasJS.Chart("chartContainer", options2)).render();},  3000*1);
        
        setTimeout(() => {var options3 = options;
            options3.data[0].dataPoints = plotOptions[2];
            iterations+=Math.floor(Math.random() * 10) + 8;
            document.getElementById("iterations").innerHTML="Iterations: "+iterations;
            (new CanvasJS.Chart("chartContainer", options3)).render();},  3000*2);

        setTimeout(() => {var options4 = options;
            options4.data[0].dataPoints = plotOptions[3];
            iterations+=Math.floor(Math.random() * 10) + 8;
            document.getElementById("iterations").innerHTML="Iterations: "+iterations;
            (new CanvasJS.Chart("chartContainer", options4)).render();},  3000*3);

        setTimeout(() => {var options5 = options;
            options5.data[0].dataPoints = plotOptions[4];
            iterations+=Math.floor(Math.random() * 10) + 8;
            document.getElementById("iterations").innerHTML="Iterations: "+iterations;
            (new CanvasJS.Chart("chartContainer", options5)).render();},  3000*4);

        setTimeout(() => {var options6= options;
            options6.data[0].dataPoints = plotOptions[5];
            iterations+=Math.floor(Math.random() * 10) + 8;
            document.getElementById("iterations").innerHTML="Iterations: "+iterations;
            (new CanvasJS.Chart("chartContainer", options6)).render();},  3000*5);
        
        setTimeout(() => {var options7= options;
            options7.data[0].dataPoints = plotOptions[6];
            iterations+=Math.floor(Math.random() * 10) + 8;
            document.getElementById("iterations").innerHTML="Iterations: "+iterations;
            (new CanvasJS.Chart("chartContainer", options7)).render();},  3000*6);

        // var resp = await getUpdatedPopulations(round);
        // console.log("RESPONSE "+resp);
        // console.log(resp.currentPopulations);
        // for( var i =0;i<resp.currentPopulations.length;i++){
        //     dataPoints[i].y = resp.currentPopulations[i];
        // }
        // options.data[0].dataPoints = dataPoints;
        // // chart.render();
        // (new CanvasJS.Chart("chartContainer", options)).render();
    
}



const PopUp = () => {
   return ( 
        <div  >
            <div class="modal" id="pop-up">
            <div class="modal_content">
                <span class="close" id="popupclose" >&times;</span>
                <div id="chartContainer" >
                </div>
            </div>
            </div>
        </div>
    );
};

export default PopUp;
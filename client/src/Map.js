import React, { useRef, useEffect, useState } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import './Map.css';
import { districtLegend, clearLegend, lineLegend } from './Legend';
import { redistrColors, electionRange, districtColors, georgiaMap, northCarolinaMap, pennMap, precinctLines, districtLines, stateLines, countyLines, congressElection, northCarolinaCongElectLayers, georgiaElectionLayers, northCarolinaElectionLayers, georgiaCongElectLayers, georgiaDistrictLayers, northCarolinaDistrictLayers } from './Constants';
import stateData from './data/statesElections.geojson';
import Header, { updateCurrentPlanText } from './Header';
import Legend from './Legend';
import LeftPanel from './LeftPanel';
import SideTable, { fillDistrictTable } from './SideTable';
import PopUp from './PopUp';
import gaStateTable from "./data/georgia/ga-state-table-content.json";
import paStateTable from "./data/penn/pa-state-table-content.json";
import ncStateTable from "./data/north-carolina/nc-state-table-content.json";
import { openGAMap, openGeorgia, updateGeorgiaLayers } from './GeorgiaMap';
import { openNCMap, openNorthCarolina, updateNorthCarolinaLayers } from './NCMap';
import { openTable, closeTable } from './SideTable';

mapboxgl.accessToken = 'pk.eyJ1Ijoibm5pZW1pZWMiLCJhIjoiY2t0Z2NlYTBqMGdzYjJ3cGhvNTl5dmk3NSJ9.3OkgWWwVaFcxi77btxn3Zg';

export var map;
export var currentState;
export var currentMap;
export var chosenPlan;
export var districtView;
export var precinctView;
export var countyView;
export var populationVar;
var states = ['Pennsylvania', 'Georgia', 'North Carolina'];
export var currentLayerID;
export var currentDistrictsData;
export var currentPlanData;
export var currentPlanID;
export var generatedDistrictsData;
export var generatedPlanData;
export var enactedPlanData;
export var enactedDistrictsData;
export var chosenPlanData;
export var chosenDistrictsData;
export var seawulfPops=[];

export function setSeawulfPops(arr){
  seawulfPops=arr;
}

export function setCurrentLayerID(layerID){
  currentLayerID=layerID;
}

export function setChosenPlan(chosen){
  chosenPlan=chosen;
  console.log(chosenPlan);
}

export function resetArrays(){
  currentDistrictsData={};
  currentPlanData={};
  enactedDistrictsData={};
  enactedPlanData={};
  generatedDistrictsData={};
  generatedPlanData={};
}
const Map = () => {
  map = useRef(null);
  const mapContainerRef = useRef(null);
  const [active, setActive] = useState(null);

  useEffect(() => {
    if (map.current) return;
    else {
      map.current = new mapboxgl.Map({
        container: mapContainerRef.current,
        center: [-95.7, 37.7],
        zoom: 3,
        style: 'mapbox://styles/nniemiec/cktg9vbjk177f17uipv8q58j0',
      });

      var nav = new mapboxgl.NavigationControl();
      map.current.addControl(nav, 'top-right');
      populationVar = 'total';
      console.log(populationVar);

      map.current.on('load', () => {
        addGeoJSONLayer('none', 'us_states_elections_outline', 'line', 'waterway-label', 'country', 'election', stateData, 'visible', 0, 15);
        addGeoJSONLayer('none', 'us_states_elections', 'fill', 'us_states_elections_outline', 'country', 'election', stateData, 'visible', 0, 6);
        const legend = document.getElementById('legend');
        const label = document.createElement('label');
        label.innerHTML = `Legend`;
        legend.appendChild(label);
        lineLegend();
        
        currentLayerID='none';
        openGAMap();
        openNCMap();
      });
    }

    currentState = 'none';
    districtView = true;
    precinctView = true;
    countyView = true;
    currentMap = 'district-plans';
    chosenPlan = 'none';
    currentPlanID='none'
    populationVar = 'total';
    currentDistrictsData = {};
    currentPlanData = {};
    generatedPlanData = {};
    generatedDistrictsData = {};
    enactedPlanData = {};
    enactedDistrictsData = {};
    chosenPlanData = {};
    chosenDistrictsData = {};

    // change cursor style to point when hovering a clickable state
    map.current.on('mouseenter', 'us_states_elections', function () {
      map.current.getCanvas().style.cursor = 'pointer';
    });

    // Change it back to a pointer when it leaves.
    map.current.on('mouseleave', 'us_states_elections', function () {
      map.current.getCanvas().style.cursor = '';
    });

    // Clicked on a state that is not Georgia, North Carolina, or Pennsylvania
    map.current.on('click', 'us_counties_elections', function (e) {
      var stateName = e.features[0].properties.State;
      if (!states.includes(stateName)) {
        resetMap();
      }
    });

    /* Clicking states */
    map.current.on('click', 'us_states_elections', function (e) {
      var stateName = e.features[0].properties.State;
      // const mapView = document.getElementById('map-view-dropdown');
      // const mapOption = mapView.value;
      if (stateName === 'Georgia') {
        currentState = 'ga';
        currentDistrictsData={};
        currentPlanData={};
        enactedDistrictsData={};
        enactedPlanData={};
        generatedDistrictsData={};
        generatedPlanData={};
      }else if(stateName==="North Carolina"){
        currentState='nc';
        currentDistrictsData={};
        currentPlanData={};
        enactedDistrictsData={};
        enactedPlanData={};
        generatedDistrictsData={};
        generatedPlanData={};
      }else{
        currentState='none';
      }
      console.log("CLICKED A STATE current: " + currentLayerID);
      map.current.setLayoutProperty(currentLayerID, 'visibility', 'none');
      map.current.setLayoutProperty(currentLayerID+"_map", 'visibility', 'none');
      openState(currentState);
      console.log("current LAYER: " + currentLayerID);
    });
  }, []);

  // useEffect(() => {
  //   map.current.on('load', () => {
  //     const generate = document.getElementById('generate');
  //     generate.onclick = function () {
  //       // const off = [(currentState + '_redistricting_1'), (currentState + '_redistricting_1_map'), (currentState + '_redistricting_2'), (currentState + '_redistricting_2_map'), (currentState + '_redistricting_3'), (currentState + '_redistricting_3_map')];
  //       // for (const id of off) {
  //       //   map.current.setLayoutProperty(id, 'visibility', 'none');
  //       // }
  //       // const on = [(currentState + '_congressional_districts'), (currentState + '_original_districts')];
  //       // for (const id of on) {
  //       //   map.current.setLayoutProperty(id, 'visibility', 'visible');
  //       // }
  //       // fillComparisonTable();
  //       // const modal = document.getElementById("pop-up")
  //       // const closeBtn = document.getElementById("popupclose")
  //       // modal.style.display = "block";
  //       // closeBtn.addEventListener("click", () => {
  //       //   modal.style.display = "none";
  //       // })
  //       // const modal = document.querySelector(".modal")
  //       // const closeBtn = document.querySelector(".close")
  //       // modal.style.display = "block";
  //       // closeBtn.addEventListener("click", () => {
  //       //   modal.style.display = "none";
  //       // })
  //     }
  //     // document.addEventListener("DOMContentLoaded",() => {
  //     //   const the_button = document.querySelector(".js-btn")
  //     //   the_button.addEventListener("click", handleClick)
  //     // })
  //   });
  // });

  return (
    <div>
      
      <div ref={mapContainerRef} className='map-container' />
      <Header active={active} />
      <Legend active={active} />
      <SideTable active={active} />
      <LeftPanel active={active} />
      <PopUp active={active}/>
      
    </div>
  );
};

// sets the min and max of generate side panel
export function setInputs() {
  var values = document.getElementsByClassName('planInfo');
  var arr = [];
  for (var i = 0; i < values.length; i++) { // get number out
    arr.push(values[i].innerText);
  }
  console.log("place hold "+arr[1]);
  document.getElementById('pop-eq-input').value = arr[1];
  document.getElementById('pop-eq-input').max = arr[1] - (-0.01);
  document.getElementById('pop-eq-input').min = arr[1] - 0.01;
}

function flyTo() {
  var lat;
  var lng;
  if (currentState === 'ga') {
    lat = georgiaMap.lat;
    lng = georgiaMap.lng;
  } else if (currentState === 'nc') {
    lat = northCarolinaMap.lat;
    lng = northCarolinaMap.lng;
  } else {
    resetMap();
    return;
  }
  map.current.flyTo({
    center: [lng, lat],
    zoom: 6
  });
}

export function openState(state) {
  currentState = state;
  Object.assign(currentDistrictsData, {});
  Object.assign(currentPlanData, {});
  flyTo(state);
  if (state === 'ga') {
    openGeorgia('district-plans');
    console.log("LAYER ID "+currentLayerID);
  } else if (state === 'nc') {
    openNorthCarolina('district-plans');
  } else {
    resetMap();
    return;
  }
  openTable();
  districtLegend(currentState);
}

export function electionMap() {
  if (currentState === 'ga') {
    updateGeorgiaLayers(georgiaElectionLayers);
  } else if (currentState === 'nc') {
    updateNorthCarolinaLayers(northCarolinaElectionLayers);
  } else {
    map.current.setLayoutProperty('us_states_elections_outline', 'visibility', 'visible');
    map.current.setLayoutProperty('us_states_elections', 'visibility', 'visible');
  }
  clearLegend();
  districtLegend();
}

export function conDisMap() {
  if (currentState === 'ga') {
    updateGeorgiaLayers(georgiaCongElectLayers);
  } else if (currentState === 'nc') {
    updateNorthCarolinaLayers(northCarolinaCongElectLayers);
  } else {
    map.current.setLayoutProperty('us_states_elections_outline', 'visibility', 'visible');
    map.current.setLayoutProperty('us_states_elections', 'visibility', 'visible');
  }
  clearLegend();
  districtLegend();
}

export function districtMap() {
  if (currentState === 'ga') {
    updateGeorgiaLayers(georgiaDistrictLayers);
  } else if (currentState === 'nc') {
    updateNorthCarolinaLayers(northCarolinaDistrictLayers);
  } else {
    map.current.setLayoutProperty('us_states_elections_outline', 'visibility', 'visible');
    map.current.setLayoutProperty('us_states_elections', 'visibility', 'visible');
  }
  clearLegend();
  districtLegend(currentState);
}

export function getLayer(layer){
  if(map.current.getLayer(layer)){
    return true;
  }
  return false;
}

export function addGeoJSONLayer(state, layerID, fillType, aboveLayer, level, mapView, data, visibility, minzoom, maxzoom) {
  let paintVar = '';
  if (fillType === 'fill') {
    if (mapView === 'election') {
      paintVar = electionRange;
    } else if (mapView === 'districtColors') {
      console.log("district collorsc chosen");
      paintVar = districtColors;
    } else if (mapView === 'cElection') {
      paintVar = congressElection;
    } else if (mapView === 'redistrColors') {
      console.log("REDISTRICTING COLORS PLEASE");
      paintVar = districtColors;
    }
  } else { 
    if (level === 'country') {
      paintVar = stateLines;
    } else if (level === 'precinct') {
      paintVar = precinctLines;
    } else if (level === 'district') {
      paintVar = districtLines;
    } else if (level === 'county') {
      paintVar = countyLines;
    }
  }
  map.current.addLayer({
    "id": layerID,
    "type": fillType,
    source: {
      type: "geojson",
      data: data,
    },
    paint: paintVar,
    minzoom: minzoom,
    maxzoom: maxzoom,
    layout: {
      'visibility': visibility,
    },
  },
    aboveLayer
  );
}

export function resetMap() {
  currentState = 'none';
  map.current.flyTo({
    center: [-85.5, 37.7],
    zoom: 3
  });
  closeTable();
  clearLegend();
  map.current.setLayoutProperty(currentLayerID, 'visibility', 'none');
  map.current.setLayoutProperty(currentLayerID+"_map", 'visibility', 'none');
  setChosenPlan(0);
  updateCurrentPlanText('No Plan');
  currentLayerID='none';
  currentDistrictsData={};
  currentPlanData={};
  chosenDistrictsData={};
  chosenPlan='none';
  chosenPlanData={};
  generatedDistrictsData={};
  generatedPlanData={};
  document.getElementById('chosen-plan').innerText = "Please Choose a Plan";
  document.getElementById('pop-eq-input').value = 0;
}

export function setDistrictView(flag) {
  districtView = flag;
}

export function setPrecinctView(flag) {
  precinctView = flag;
}

export function setCountyView(flag) {
  countyView = flag;
}

export function setPopVar(popVar) {
  populationVar = popVar;
  console.log(populationVar);
}

export default Map;
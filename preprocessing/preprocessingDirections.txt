
For the first set of preprocessing (combining data), we had to break out scripts based on states (because PA was so large) (client/src/data/{state}/scripts)
1. addNumbersToPrecincts.js
2. mapCBToPrecincts.js
3. mapCBDistToPrecincts.js
4. gaElectionCSVToJSON.js
5. gaMatchElectionJSONToPrecincts.js
now we move onto python preprocessing code (preprocessing/)
1. addPrecinctPopulations.py ../{STATE}_PRECINCTS_ALL_INFO.json
2. aggregatePrecinctPopulations.py ../{STATE}_CENSUS.json result_from_1.json
3. aggregateDistrictPopulations.py ../{STATE}_CENSUS.json
4. addElectionDataToCD.py [ran for each state]
5. separateVAPAndTotal.py ../result_from_3.json
now we move onto graph files
1. make sure geojson is formatted correctly
2. python3 geojsonGraph.py data/processed/precincts.geojson
3. python3 adjustPrecinctNieghbors.py result_from_2.geojson
4. python3 seawulfFormat.py result_from_3.json
5. inside gerrymandering-mcmc, run python3 cli.py with result_from_4.json
import json as json
import geopandas as gpd
import pandas as pd
import sys

# need to

if len(sys.argv) != 2:
    print("provide the path of the json file you are trying to convert.")
    exit(0)
if "graph" in sys.argv[1]:
    exit(0)

json_path = sys.argv[1]
print(json_path)

file = open(json_path, "r")
data = json.load(file)
print(data['features'][0]['properties'])
print(range(len(data['features'])))

for feature in data['features']:
    feature["properties"].update({"TOTALPOP":0, "TOTALHIS":0, "TOTALNONHIS":0, "TOTALASIAN":0, "TOTALWHITE": 0, "TOTALAA":0, "TOTALVAP":0, "TOTALVAPHIS":0, "TOTALVAPNONHIS":0, "TOTALVAPASIAN":0, "TOTALVAPWHITE":0, "TOTALVAPAA":0})

file1 = open("pa_prectest.json", "w")
json.dump(data, file1)
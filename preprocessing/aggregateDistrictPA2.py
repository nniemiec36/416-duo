import json as json
import pandas as pd
import sys

pa_cd_pop = "./pa-cd-pop.json"
pa_census = "../client/src/data/penn/processed/PA_CENSUS.json"


data2=pd.read_json(pa_census)

file1 = open(pa_cd_pop, "r")
#file2 = open(pa_census, "r")

# with open(pa_census) as file2:
#     file2.seek(0)
#     data2 = file2.read(168492 - 0)
# total_pop = 0

data1 = json.load(file1)
# data2 = json.load(file2)

print(data1['features'][0]['TOTALPOP'])
print(int(len(data2['features'])/2))


for i in (range(0, int(len(data2['features'])/2))):
    total_pop += data2['features'][i]['properties']['TOTALPOP']
    dist = int(data2['features'][i]['properties']['DISTRICTID'])
    data1['features'][dist]['TOTALPOP'] += data2['features'][i]['properties']['TOTALPOP']
    data1['features'][dist]['TOTALHIS'] += data2['features'][i]['properties']['TOTALHIS']
    data1['features'][dist]['TOTALNONHIS'] += data2['features'][i]['properties']['TOTALNONHIS']
    data1['features'][dist]['TOTALASIAN'] += data2['features'][i]['properties']['TOTALASIAN']
    data1['features'][dist]['TOTALWHITE'] += data2['features'][i]['properties']['TOTALWHITE']
    data1['features'][dist]['TOTALAA'] += data2['features'][i]['properties']['TOTALAA']
    data1['features'][dist]['TOTALVAP'] += data2['features'][i]['properties']['TOTALVAP']
    data1['features'][dist]['TOTALVAPHIS'] += data2['features'][i]['properties']['TOTALVAPHIS']
    data1['features'][dist]['TOTALVAPNONHIS'] += data2['features'][i]['properties']['TOTALVAPNONHIS']
    data1['features'][dist]['TOTALVAPASIAN'] += data2['features'][i]['properties']['TOTALVAPASIAN']
    data1['features'][dist]['TOTALVAPWHITE'] += data2['features'][i]['properties']['TOTALVAPWHITE']
    data1['features'][dist]['TOTALVAPAA'] += data2['features'][i]['properties']['TOTALVAPAA']

print(total_pop)
# file1 = open(pa_cd_pop, "w")
# json.dump(data1, file1)

import json as json
import math as math
# import shapely as shapely
from shapely.geometry import Polygon, mapping, shape
from shapely.ops import unary_union


cd_path = '../client/src/data/georgia/processed/GA_PRECINCTS_ALL_INFO.geojson'
cd_file = open(cd_path, "r")
cd_data = json.load(cd_file)

# shapely.ops.unary_union(geoms)

districtPolygons = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # each list is list of precincts shape objs in district given by id
districtsOnly = [] # district shape objects returned from union
popperScores = []
for i in range(len(cd_data["features"])):
    p = shape(json.loads(json.dumps(cd_data['features'][i]['geometry'])))
    dist = int(cd_data['features'][i]["properties"]['DistrictID'])
    dist -=1
    districtPolygons[dist].append(p)

for polyList in districtPolygons:
    if(polyList==[]):
        continue
    district = unary_union(polyList)
    districtsOnly.append(district)

count = 0
for dist in districtsOnly:
    area = dist.area
    perimeter = dist.length
    polsby = (4 * math.pi * area) / (perimeter**2)
    print("Dist #"+str(count+1)+" is "+str(polsby))
    popperScores.append(polsby)
    count +=1

planPopperScore = 0
for i in range(len(popperScores)):
    planPopperScore+=popperScores[i]
planPopperScore = planPopperScore / len(popperScores)
print(planPopperScore)


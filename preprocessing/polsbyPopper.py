# The Polsby-Popper (PP) measure (polsby & Popper, 1991) is the ratio of the area of the district (AD) 
# to the area of a circle whose circumference is equal to the perimeter of the district (PD). A district’s
#  Polsby-Popper score falls with the range of [0,1] and a score closer to 1 indicates a more compact district.

import json as json
import math as math
# import shapely as shapely
from shapely.geometry import Polygon, mapping, shape

cd_path = './GA_CD_ALL_INFO.geojson'
cd_file = open(cd_path, "r")
cd_data = json.load(cd_file)

popperScores = []
for i in range(len(cd_data["features"])):
    p = shape(json.loads(json.dumps(cd_data['features'][i]['geometry'])))
    # print(p.area)
    # print(p.length)
    area = p.area
    perimeter = p.length
    polsby = (4 * math.pi * area) / (perimeter**2)
    print(cd_data['features'][i]['properties']["DistrictID"] +" polsby "+str(polsby))
    popperScores.append(polsby)


planPopperScore = 0
for i in range(len(popperScores)):
    planPopperScore+=popperScores[i]
planPopperScore = planPopperScore / len(popperScores)
print(planPopperScore)


print(Polygon([[0,0],[1,0],[1,1],[0,1]]).length)
print(Polygon([[0,0],[1,0],[1,1],[0,1]]).area)
print(4 * math.pi * Polygon([[0,0],[.5,-.3],[1,0],[1.3,.5],[1,1],[.5,1.3],[0,1],[-.3,.5]]).area/(Polygon([[0,0],[.5,-.3],[1,0],[1.3,.5],[1,1],[.5,1.3],[0,1],[-.3,.5]]).length)**2)
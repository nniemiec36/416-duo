# create a free account at redistrictingdatahub.org to access the files

# download shp file for penn here, unzip, place in folder
# https://redistrictingdatahub.org/dataset/pennsylvania-congressional-district-pl-94171-2020/

# download shp file for ga here, unzip, place in folder
# https://redistrictingdatahub.org/dataset/georgia-congressional-district-pl-94171-2020/

# download shp file for nc here, unzip, place in folder
# https://redistrictingdatahub.org/dataset/north-carolina-congressional-district-pl-94171-2020/

# then run this on your terminal by using python3 preprocessingblocks.py
# and push the geojson files to git

import geopandas as gpd

ga_blocks = gpd.read_file('ga_pl2020_b/ga_pl2020_b.shp')
ga_blocks.to_file('data/ga_blocks.geojson', driver='GeoJSON')

nc_blocks = gpd.read_file('nc_pl2020_b/nc_pl2020_b.shp')
nc_blocks.to_file('data/nc_blocks.geojson', driver='GeoJSON')

pa_blocks = gpd.read_file('pa_pl2020_b/pa_pl2020_b.shp')
pa_blocks.to_file('data/pa_blocks.geojson', driver='GeoJSON')
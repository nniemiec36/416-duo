import pandas as pd
import geopandas as gp
import json as json
import sys

file = open(sys.argv[1], "r")

data = json.load(file)

print(data['adjacency'][0])
print(data['adjacency'][0][0])
print(data['adjacency'][0][0]['id'])

for i in range(len(data['adjacency'])):
    id = i
    is_border_block = False
    for j in range(len(data['adjacency'][i])):
        j_id = data['adjacency'][i][j]['id']
        if data['nodes'][id]['DISTRICTID'] != data['nodes'][j_id]['DISTRICTID']:
            is_border_block = True
            break
    data['nodes'][id]['ISBORDERBLOCK'] = is_border_block

file_name = sys.argv[1].split(".json")[0] + "-2.json"
file1 = open(file_name, "w")
json.dump(data, file1)


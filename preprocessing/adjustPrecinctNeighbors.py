import pandas as pd
import geopandas as gp
import json as json
import sys

file = open(sys.argv[1], "r")

data = json.load(file)

adj_list = []

# print(data['adjacency'][0])
# print(data['adjacency'][0][0])
# print(len(data['adjacency']))
for i in range(len(data['adjacency'])):
    list = [];
    for j in range(len(data['adjacency'][i])):
        # print(data['adjacency'][i][j])
        # print(data['adjacency'][i][j]['shared_perim'])
        cell = data['adjacency'][i][j]
        perim = data['adjacency'][i][j]['shared_perim']
        if(perim >= 60.96):
            list.append(cell)
    adj_list.append(list)    
# print(adj_list)
data['adjacency'] = adj_list
file_name = sys.argv[2]
file1 = open(file_name, "w")
json.dump(data, file1)
# for i in len(data['adjacency']):
#     list = [];
#     print(data['adjacency'][i]);
#     for j in data['adjacency'][i]:
#         print(j);
    # for j in i[]:

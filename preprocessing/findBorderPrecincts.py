import json as json
import sys

if len(sys.argv) != 2:
    print("provide the file to insert")
    exit(0)

json_path = sys.argv[1]
print(json_path)

precincts = []

file = open(json_path, "r")
data = json.load(file)
json_array = {"directed": False, "multigraph": False, "graph": [], "nodes": [], "adjacency": []}
print(data['nodes'][0]['boundary_node'])
print(type(data['nodes'][0]['boundary_node']))
border_count = 0
for i in range(len(data['nodes'])):
    if data['nodes'][i]['boundary_node'] == True:
        json_array['nodes'].append(data['nodes'][i])
        adjacency_array = []
        for j in range(len(data['adjacency'][i])):
            id = data['adjacency'][i][j]['id']
            if data['nodes'][id]['boundary_node'] == True:
                print("node " + str(i) + " : " + str(id))
                adjacency_array.append(data['adjacency'][i][j])
        print(adjacency_array)
        json_array['adjacency'].append(adjacency_array)
        precincts.append(data['nodes'][i]['PrecinctID']) 
        border_count += 1

print(border_count)
print(precincts)
new_file_name = file_name = sys.argv[1].split("-precincts-graph-2.json")[0] + "-border-precincts.json"
file1 = open(new_file_name, "w")
json.dump(json_array, file1)

# only gonna worry about the border precincts when running the algorithm
# then only gonna worry about the census blocks inside those border precincts


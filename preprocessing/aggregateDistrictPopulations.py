import json as json
import sys

if len(sys.argv) != 2:
    print("provide the path of the json file you are trying to convert.")
    exit(0)
if "graph" in sys.argv[1]:
    exit(0)

geojson_path = sys.argv[1]
print(geojson_path)

if "GA_CENSUS" in geojson_path:
    print("georgia true")
    json_obj = {
        "01": { "STATEFP20": "13", "GEOID20": "1301", "CD116FP": "01", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "02": { "STATEFP20": "13", "GEOID20": "1302", "CD116FP": "02", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "03": { "STATEFP20": "13", "GEOID20": "1303", "CD116FP": "03", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "04": { "STATEFP20": "13", "GEOID20": "1304", "CD116FP": "04", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "05": { "STATEFP20": "13", "GEOID20": "1305", "CD116FP": "05", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "06": { "STATEFP20": "13", "GEOID20": "1306", "CD116FP": "06", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "07": { "STATEFP20": "13", "GEOID20": "1307", "CD116FP": "07", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "08": { "STATEFP20": "13", "GEOID20": "1308", "CD116FP": "08", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "09": { "STATEFP20": "13", "GEOID20": "1309", "CD116FP": "09", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "10": { "STATEFP20": "13", "GEOID20": "1310", "CD116FP": "10", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "11": { "STATEFP20": "13", "GEOID20": "1311", "CD116FP": "11", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "12": { "STATEFP20": "13", "GEOID20": "1312", "CD116FP": "12", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "13": { "STATEFP20": "13", "GEOID20": "1313", "CD116FP": "13", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "14": { "STATEFP20": "13", "GEOID20": "1314", "CD116FP": "14", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 }
    }
    file_name = "./ga-cd-pop.json"
elif "NC_CENSUS" in geojson_path: 
    print("north carolina true")
    file_name = "./nc-cd-pop.json"
    json_obj = {
        "01": { "STATEFP20": "37", "GEOID20": "3701", "CD116FP": "01", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "02": { "STATEFP20": "37", "GEOID20": "3702", "CD116FP": "02", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "03": { "STATEFP20": "37", "GEOID20": "3703", "CD116FP": "03", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "04": { "STATEFP20": "37", "GEOID20": "3704", "CD116FP": "04", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "05": { "STATEFP20": "37", "GEOID20": "3705", "CD116FP": "05", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "06": { "STATEFP20": "37", "GEOID20": "3706", "CD116FP": "06", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "07": { "STATEFP20": "37", "GEOID20": "3707", "CD116FP": "07", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "08": { "STATEFP20": "37", "GEOID20": "3708", "CD116FP": "08", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "09": { "STATEFP20": "37", "GEOID20": "3709", "CD116FP": "09", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "10": { "STATEFP20": "37", "GEOID20": "3710", "CD116FP": "10", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "11": { "STATEFP20": "37", "GEOID20": "3711", "CD116FP": "11", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "12": { "STATEFP20": "37", "GEOID20": "3712", "CD116FP": "12", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "13": { "STATEFP20": "37", "GEOID20": "3713", "CD116FP": "13", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 }
    }
    # file_name = "./nc-cd-pop.json"
else:
    json_obj = {
        "01": { "STATEFP20": "42", "GEOID20": "4201", "CD116FP": "01", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "02": { "STATEFP20": "42", "GEOID20": "4202", "CD116FP": "02", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "03": { "STATEFP20": "42", "GEOID20": "4203", "CD116FP": "03", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "04": { "STATEFP20": "42", "GEOID20": "4204", "CD116FP": "04", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "05": { "STATEFP20": "42", "GEOID20": "4205", "CD116FP": "05", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "06": { "STATEFP20": "42", "GEOID20": "4206", "CD116FP": "06", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "07": { "STATEFP20": "42", "GEOID20": "4207", "CD116FP": "07", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "08": { "STATEFP20": "42", "GEOID20": "4208", "CD116FP": "08", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "09": { "STATEFP20": "42", "GEOID20": "4209", "CD116FP": "09", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "10": { "STATEFP20": "42", "GEOID20": "4210", "CD116FP": "10", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "11": { "STATEFP20": "42", "GEOID20": "4211", "CD116FP": "11", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "12": { "STATEFP20": "42", "GEOID20": "4212", "CD116FP": "12", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "13": { "STATEFP20": "42", "GEOID20": "4213", "CD116FP": "13", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "14": { "STATEFP20": "42", "GEOID20": "4214", "CD116FP": "14", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "15": { "STATEFP20": "42", "GEOID20": "4215", "CD116FP": "15", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "16": { "STATEFP20": "42", "GEOID20": "4216", "CD116FP": "16", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "17": { "STATEFP20": "42", "GEOID20": "4217", "CD116FP": "17", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 },
        "18": { "STATEFP20": "42", "GEOID20": "4218", "CD116FP": "18", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0 }
    }
    file_name = "./pa-cd-pop.json"

file = open(geojson_path, "r")
total_pop = 0

data = json.load(file)
# print(data['features'][0]['properties']['DISTRICTID'])
# print(range(len(data['features'])))
# print(data['features'][17]['properties']['TOTALPOP'])
# print(json_obj['01']['TOTALPOP'])
print(int(len(data['features'])/2))
for i in (range(0, int(len(data['features'])/2))):
    total_pop += data['features'][i]['properties']['TOTALPOP']
    dist = data['features'][i]['properties']['DISTRICTID']
    json_obj[dist]['TOTALPOP'] += data['features'][i]['properties']['TOTALPOP']
    json_obj[dist]['TOTALHIS'] += data['features'][i]['properties']['TOTALHIS']
    json_obj[dist]['TOTALNONHIS'] += data['features'][i]['properties']['TOTALNONHIS']
    json_obj[dist]['TOTALASIAN'] += data['features'][i]['properties']['TOTALASIAN']
    json_obj[dist]['TOTALWHITE'] += data['features'][i]['properties']['TOTALWHITE']
    json_obj[dist]['TOTALAA'] += data['features'][i]['properties']['TOTALAA']
    json_obj[dist]['TOTALVAP'] += data['features'][i]['properties']['TOTALVAP']
    json_obj[dist]['TOTALVAPHIS'] += data['features'][i]['properties']['TOTALVAPHIS']
    json_obj[dist]['TOTALVAPNONHIS'] += data['features'][i]['properties']['TOTALVAPNONHIS']
    json_obj[dist]['TOTALVAPASIAN'] += data['features'][i]['properties']['TOTALVAPASIAN']
    json_obj[dist]['TOTALVAPWHITE'] += data['features'][i]['properties']['TOTALVAPWHITE']
    json_obj[dist]['TOTALVAPAA'] += data['features'][i]['properties']['TOTALVAPAA']

print(total_pop)
for i in (range(int(len(data['features'])/2), len(data['features']))):
    total_pop += data['features'][i]['properties']['TOTALPOP']
    dist = data['features'][i]['properties']['DISTRICTID']
    json_obj[dist]['TOTALPOP'] += data['features'][i]['properties']['TOTALPOP']
    json_obj[dist]['TOTALHIS'] += data['features'][i]['properties']['TOTALHIS']
    json_obj[dist]['TOTALNONHIS'] += data['features'][i]['properties']['TOTALNONHIS']
    json_obj[dist]['TOTALASIAN'] += data['features'][i]['properties']['TOTALASIAN']
    json_obj[dist]['TOTALWHITE'] += data['features'][i]['properties']['TOTALWHITE']
    json_obj[dist]['TOTALAA'] += data['features'][i]['properties']['TOTALAA']
    json_obj[dist]['TOTALVAP'] += data['features'][i]['properties']['TOTALVAP']
    json_obj[dist]['TOTALVAPHIS'] += data['features'][i]['properties']['TOTALVAPHIS']
    json_obj[dist]['TOTALVAPNONHIS'] += data['features'][i]['properties']['TOTALVAPNONHIS']
    json_obj[dist]['TOTALVAPASIAN'] += data['features'][i]['properties']['TOTALVAPASIAN']
    json_obj[dist]['TOTALVAPWHITE'] += data['features'][i]['properties']['TOTALVAPWHITE']
    json_obj[dist]['TOTALVAPAA'] += data['features'][i]['properties']['TOTALVAPAA']

print(file_name)
print(total_pop)
file1 = open(file_name, "w")
json.dump(json_obj, file1)

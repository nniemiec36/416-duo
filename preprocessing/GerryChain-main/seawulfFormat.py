import geopandas as gpd
import json as json
import sys

from networkx.readwrite.json_graph import adjacency

list_dist = {"01":"A", "02":"B", "03":"C", "04":"D", "05":"E", "06":"F", "07":"G", "08":"H", "09":"I", "10":"J", "11":"K", "12":"L", "13":"M", "14":"N", "15":"O", "16":"P", "17":"Q", "18":"R"}

# "1": {
#         "adjacent_nodes": [
#             "2",
#             "12"
#         ],
#         "population": 50,
#         "voting_history": "D",
#         "district": "A"
#     },
#     "2": {
#         "adjacent_nodes": [
#             "3",
#             "13"
#         ],
#         "population": 50,
#         "voting_history": "D",
#         "district": "A"
#     },

file = open(sys.argv[1], "r")

data = json.load(file)

json_array = {}

print(data['adjacency'][0][0]['id'])
print(data['nodes'][0])
print(data['nodes'][0]['SENATETOT'])
for i in range(len(data['adjacency'])):
    node_index = str(i)
    adjacency_arr = []
    print(i)
    for j in range(len(data['adjacency'][i])):
        adjacency_arr.append(str(data['adjacency'][i][j]['id']))
    
    adj_arr = {'adjacent_nodes': []}
    adj_arr['adjacent_nodes'] = adjacency_arr
    json_array[node_index] = adj_arr

    if data['nodes'][i]['SENATETOT'] != 0:
        dem_percent = data['nodes'][i]['SENATED']/data['nodes'][i]['SENATETOT']
        rep_percent = data['nodes'][i]['SENATER']/data['nodes'][i]['SENATETOT']
    else:
        dem_percent = 0
        rep_percent = 0

    if dem_percent > rep_percent:
        json_array[node_index]["voting_history"] = 'D'
    else:
        json_array[node_index]['voting_history'] = 'R'
    json_array[node_index]["population"] = data['nodes'][i]['TOTALPOP']
    json_array[node_index]["district"] = list_dist[data['nodes'][i]['DistrictID']]

print(json_array['0'])

file_name = sys.argv[1].split(".json")[0] + "-2.json"
file1 = open(file_name, "w")
json.dump(json_array, file1)
import json as json
# import mapshaper as map
#import geopandas as gpd
import sys
import os
from decimal import *
import math
from shapely.geometry import Polygon, mapping, shape
from shapely.ops import unary_union

# redirect stdout
orig_stdout = sys.stdout
f = open('plansFound.txt', 'w')
sys.stdout = f

# get path
output_plans_path = sys.argv[1] # directory that contains graph files form seawulf
directory = "processedWithData"
processed_path = os.path.join(output_plans_path, directory)    

# check measures
for item in os.listdir(processed_path): # for each seawulf district plan file
     plan_file = open(os.path.join(processed_path, item), "r")
     plan_data = json.load(plan_file)

     measures = plan_data["PlanMeasures"]
     if(measures['ObjectiveFunct1']>0.7 or measures['ObjectiveFunct2']>0.7):
        print(str(item)+" good obj func "+str(measures['ObjectiveFunct1']))
     if(measures['Polsby']>0.2):
         print(str(item)+" good polsby "+str(measures['Polsby']))
     if(measures['PopulationEquality']>0.87):
         print(str(item)+" good pop eq "+str(measures['PopulationEquality']))
     if(measures['NumberOpportunity']>=4):
         print(str(item)+" good opportunity "+str(measures['NumberOpportunity']))
     percents = plan_data['percents']
     rep = percents["PresR"]
     dem = percents["PresD"]
     numRep = 0
     numDem = 0
     for i in range(len(rep)):
          if(rep[i]>0.5):
               numRep+=1
          if(dem[i]>0.5):
               numDem+=1
     if(numRep >= 6 and numRep <=8 and numDem >=6 and numDem <=8):
          print(str(item)+" good dem/rep split rep "+str(numRep)+" dem "+str(numDem))



    



# close stdout
sys.stdout = orig_stdout
f.close()
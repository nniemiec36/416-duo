import json as json
# import mapshaper as map
#import geopandas as gpd
import sys
import os
from decimal import *
import math
from shapely.geometry import Polygon, mapping, shape
from shapely.ops import unary_union


list_dist = {"A":"01", "B":"02", "C":"03", "D":"04", "E":"05", "F":"06", "G":"07", "H":"08", "I":"09", "J":"10", "K":"11", "L":"12", "M":"13", "N":"14", "O":"15", "P":"16", "Q":"17", "R":"18"}



geo_path = sys.argv[1] # all info precincts file
geo_file = open(geo_path, "r")
geo_data = json.load(geo_file)

numDistricts = 14 # for georgia

output_plans_path = sys.argv[2] # directory that contains graph files form seawulf
# make output directory 
directory = "processed"
processed_path = os.path.join(output_plans_path, directory)
if not os.path.isdir(processed_path):
    os.mkdir(processed_path)

    count = 0
    for item in os.listdir(output_plans_path):
        if os.path.isfile(os.path.join(output_plans_path, item)) and item.endswith('.json'):
            plan_file = open(os.path.join(output_plans_path, item), "r")
            plan_data = json.load(plan_file)
            count+=1
            for i in range(len(geo_data['features'])):
                geoPrecinct = geo_data['features'][i]['properties']['PrecinctID']
                for j in range(len(plan_data['nodes'])):
                    planPrecinct = plan_data['nodes'][j]['id']
                    if(geoPrecinct==planPrecinct):
                        planDistrict = plan_data['nodes'][j]['district']
                        planDistrict = list_dist.get(planDistrict)
                        geo_data['features'][i]['properties']['DistrictID'] = planDistrict

            file_name = processed_path+"/plan_reformatted_"+str(count)+".geojson"
            file1 = open(file_name, "w")
            json.dump(geo_data, file1)

#dir already made
itemCount = 0
for item in os.listdir(processed_path): # for each seawulf district plan file
    itemCount+=1
    print(item)
    plan_file = open(os.path.join(processed_path, item), "r")
    plan_data = json.load(plan_file)

    measures = {'01':{"PresTot":0,"PresD":0, "PresR":0, "SenTot":0, "SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}, 
                '02':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0},
                '03':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0},
                '04':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0},
                '05':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0},
                '06':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0},
                '07':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0},
                '08':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0},
                '09':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}, 
                '10':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}, 
                '11':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}, 
                '12':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}, 
                '13':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}, 
                '14':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}, 
                '15':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}, 
                '16':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}, 
                '17':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}, 
                '18':{"PresTot":0,"PresD":0, "PresR":0,  "SenTot":0,"SenD":0, "SenR":0, "Pop":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "VAP":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0}}
   
    percents = {'01':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '02':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '03':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '04':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '05':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '06':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '07':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '08':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '09':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '10':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '11':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '12':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '13':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '14':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '15':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '16':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '17':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0},
                    '18':{"PresD":0, "PresR":0, "SenD":0, "SenR":0, "AfAmer":0, "White":0, "Asian":0, "His":0, "NonHis":0, "AfAmerVAP":0, "WhiteVAP":0, "AsianVAP":0, "HisVAP":0, "NonHisVAP":0, "Opportuniy": "", "polsby":0}}
        
    districtPolygons = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # each list is list of precincts shape objs in district given by id
    districtsOnly = [] # district shape objects returned from union
    popperScores = []
    for i in range(len(plan_data["features"])):
        p = shape(json.loads(json.dumps(plan_data['features'][i]['geometry'])))
        dist = int(plan_data['features'][i]["properties"]['DistrictID'])
        dist -=1
        districtPolygons[dist].append(p)

    for polyList in districtPolygons:
        if(polyList==[]):
            continue
        district = unary_union(polyList)
        districtsOnly.append(district)

    count = 0
    for dist in districtsOnly:
        curDistNum = list(measures)[count]
        area = dist.area
        perimeter = dist.length
        polsby = (4 * math.pi * area) / (perimeter**2)
        print("Dist #"+str(count+1)+" is "+str(polsby))
        percents[curDistNum]["polsby"] = polsby
        popperScores.append(polsby)
        count +=1

    planPopperScore = 0
    eqPopScore = 0
    for i in range(len(popperScores)):
        planPopperScore+=popperScores[i]
    planPopperScore = planPopperScore / len(popperScores)
    print(planPopperScore)


    # go through each precinct in the plan to get info
    for i in range(len(plan_data['features'])):
        plan = plan_data['features'][i]['properties']
        dist = plan['DistrictID']
        PresD = plan['PRESDEM']
        PresR = plan['PRESREP']
        PresTot = plan['PRESTOT']
        SenR = plan['SENATER']
        SenD = plan['SENATED']
        SenTot = plan['SENATETOT']
        AfAmer = plan['TOTALAA']
        White = plan['TOTALWHITE']
        Asian = plan['TOTALASIAN']
        His = plan['TOTALHIS']
        NonHis = plan['TOTALNONHIS']
        Pop = plan['TOTALPOP']
        AfAmerVAP = plan['TOTALVAPAA']
        WhiteVAP = plan['TOTALVAPWHITE']
        AsianVAP = plan['TOTALVAPASIAN']
        HisVAP = plan['TOTALVAPHIS']
        NonHisVAP = plan['TOTALVAPNONHIS']
        VAP = plan['TOTALVAP']

        #update measuers dictionary with totals per district
        distDict = measures.get(dist)
        distDict['PresD'] = distDict['PresD']+PresD
        distDict['PresR'] = distDict['PresR']+PresR
        distDict['PresTot'] = distDict['PresTot']+PresTot
        distDict['SenD'] = distDict['SenD']+SenD
        distDict['SenR'] = distDict['SenR']+SenR
        distDict['SenTot'] = distDict['SenTot']+SenTot

        distDict['AfAmer'] = distDict['AfAmer']+AfAmer
        distDict['White'] = distDict['White']+White
        distDict['Asian'] = distDict['Asian']+Asian
        distDict['His'] = distDict['His']+His
        distDict['NonHis'] = distDict['NonHis']+NonHis
        distDict['Pop'] = distDict['Pop']+Pop
        
        distDict['AfAmerVAP'] = distDict['AfAmerVAP']+AfAmerVAP
        distDict['WhiteVAP'] = distDict['WhiteVAP']+WhiteVAP
        distDict['AsianVAP'] = distDict['AsianVAP']+AsianVAP
        distDict['HisVAP'] = distDict['HisVAP']+HisVAP
        distDict['NonHisVAP'] = distDict['NonHisVAP']+NonHisVAP
        distDict['VAP'] = distDict['VAP']+VAP

        # last entry, do calculations and add percentage dictionary
        if i == (len(plan_data['features']) - 1):
            equalPopSum = 0
            statePop =0
            for j in range(len(measures)):
                curDistNum = list(measures)[j]
                curDist = measures.get(curDistNum)
                statePop+=curDist['Pop']
            print("statepop "+str(statePop))
            idealPop = statePop / numDistricts 
            for j in range(len(measures)): # for each district
                curDistNum = list(measures)[j]
                curDist = measures.get(curDistNum)
                if curDist['PresTot'] == 0:
                    continue # this dist not in this state
                percPresD = curDist['PresD'] / curDist['PresTot']
                percPresD = Decimal(percPresD).quantize(Decimal('1e-5'))
                percPresR = curDist['PresR'] / curDist['PresTot']
                percPresR = Decimal(percPresR).quantize(Decimal('1e-5'))
                
                percSenD = curDist['SenD'] / curDist['SenTot']
                percSenD = Decimal(percSenD).quantize(Decimal('1e-5'))
                percSenR = curDist['SenR'] / curDist['SenTot']
                percSenR = Decimal(percSenR).quantize(Decimal('1e-5'))

                percAfAmer = curDist['AfAmer'] / curDist['Pop']
                percAfAmer = Decimal(percAfAmer).quantize(Decimal('1e-5'))
                percWhite = curDist['White'] / curDist['Pop']
                percWhite = Decimal(percWhite).quantize(Decimal('1e-5'))
                percAsian = curDist['Asian'] / curDist['Pop']
                percAsian = Decimal(percAsian).quantize(Decimal('1e-5'))
                percHis = curDist['His'] / curDist['Pop']
                percHis = Decimal(percHis).quantize(Decimal('1e-5'))
                percNonHis = curDist['NonHis'] / curDist['Pop']
                percNonHis = Decimal(percNonHis).quantize(Decimal('1e-5'))

                percAfAmerVAP = curDist['AfAmerVAP'] / curDist['VAP']
                percAfAmerVAP = Decimal(percAfAmerVAP).quantize(Decimal('1e-5'))
                percWhiteVAP = curDist['WhiteVAP'] / curDist['VAP']
                percWhiteVAP = Decimal(percWhiteVAP).quantize(Decimal('1e-5'))
                percAsianVAP = curDist['AsianVAP'] / curDist['VAP']
                percAsianVAP = Decimal(percAsianVAP).quantize(Decimal('1e-5'))
                percHisVAP = curDist['HisVAP'] / curDist['VAP']
                percHisVAP = Decimal(percHisVAP).quantize(Decimal('1e-5'))
                percNonHisVAP = curDist['NonHisVAP'] / curDist['VAP']
                percNonHisVAP = Decimal(percNonHisVAP).quantize(Decimal('1e-5'))
                
                # print(curDistNum+" white "+str(percWhiteVAP)+" black "+str(percAfAmerVAP)+" asain "+str(percAsianVAP)+" his "+str(percHisVAP)+" non his "+str(percNonHisVAP))
                percents[curDistNum]['PresD'] = str(percPresD)
                percents[curDistNum]['PresR'] = str(percPresR)
                percents[curDistNum]['SenD'] = str(percSenD)
                percents[curDistNum]['SenR'] = str(percSenR)

                percents[curDistNum]['AfAmer'] = str(percAfAmer)
                percents[curDistNum]['White'] = str(percWhite)
                percents[curDistNum]['Asian'] = str(percAsian)
                percents[curDistNum]['His'] = str(percHis)
                percents[curDistNum]['NonHis'] = str(percNonHis)

                percents[curDistNum]['AfAmerVAP'] = str(percAfAmerVAP)
                percents[curDistNum]['WhiteVAP'] = str(percWhiteVAP)
                percents[curDistNum]['AsianVAP'] = str(percAsianVAP)
                percents[curDistNum]['HisVAP'] = str(percHisVAP)
                percents[curDistNum]['NonHisVAP'] = str(percNonHisVAP)

                if(float(percents[curDistNum]['AfAmer']) > .5):
                    percents[curDistNum]['Opportuniy'] = 'AfAmer'
                elif float(percents[curDistNum]['Asian']) > .5:
                    percents[curDistNum]['Opportuniy'] = 'Asian'
                elif float(percents[curDistNum]['His']) > .5:
                    percents[curDistNum]['Opportuniy'] = 'His'
                else:
                    percents[curDistNum]['Opportuniy'] = 'White'

                # CALCULATE POPULATION EQUALITY <---------------------------------------------
                equalPopSum += ((measures[curDistNum]['Pop']/idealPop)-1)**2
            eqPopScore = math.sqrt(equalPopSum)
            print("POP EQ SCORE : "+str(math.sqrt(equalPopSum)))
                
    # all relevant election and demographic info is in the percents dictionary for this plan, add it to the top of the geojson file
    geo_data['popEq'] = eqPopScore
    geo_data["polsby"] = planPopperScore
    geo_data['percents'] = percents
    feat = geo_data['features']
    del geo_data['features']
    geo_data['features'] = feat
    file_name = processed_path+"/processed_plan_"+str(itemCount)+".geojson"
    file1 = open(file_name, "w")
    json.dump(geo_data, file1)

    


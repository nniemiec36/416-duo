import json as json

# cd_path = '../../client/src/data/georgia/processed/GA_CENSUS_RENUMBER.json'
cd_path = '../../client/src/data/north-carolina/processed/NC_PRECINCTS_ALL_INFO.geojson'
cd_file = open(cd_path, "r")
cd_data = json.load(cd_file)

totalPop = 0
for i in range(len(cd_data["features"])):
    pop = cd_data['features'][i]['properties']['TOTALPOP']
    # print(pop)
    totalPop = totalPop +pop
print(totalPop)
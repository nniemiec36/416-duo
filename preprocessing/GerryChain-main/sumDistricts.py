import json as json
import math
from os import popen
import numpy as np

# geojson_path = "../graph-processed-751-2.json"
geojson_path = "./seawulf-plan-test.json"
precinct_file = open(geojson_path, "r")
precinct_data = json.load(precinct_file)

def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper

def find_percentages(ideal_pop, original_arr):
    pop_percentages = []
    for i in range(len(original_arr)):
        pop_percentages.append(truncate(((ideal_pop - original_arr[i])/ideal_pop), 4))
    return pop_percentages

def reached_goal(target_pos, target_neg, arr) -> bool:
    counter = 0
    num_districts = len(arr)
    for i in range(len(arr)):
        if arr[i] > 0 and arr[i] < target_pos:
            counter += 1
        elif arr[i] < 0 and arr[i] > neg_target:
            counter += 1
    if num_districts == counter:
        return True
    else:
        return False

def find_worst_dist(pops_array):
    worst_distr = []
    for i in range(len(pops_array)):
        if abs(pops_array[i]) > 0.3:
            if i < 8:
                worst_distr.append("0" + str(i+1))
            else:
                worst_distr.append(str(i+1))
    return worst_distr



district_pops = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
dpops_percent_from_ideal = []

pos_target = .03
neg_target = -.03
ideal_pop = 765136

for i in range(len(precinct_data['nodes'])):
    if precinct_data['nodes'][i]['DistrictID'] == '01':
        district_pops[0] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '02':
        district_pops[1] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '03':
        district_pops[2] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '04':
        district_pops[3] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '05':
        district_pops[4] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '06':
        district_pops[5] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '07':
        district_pops[6] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '08':
        district_pops[7] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '09':
        district_pops[8] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '10':
        district_pops[9] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '11':
        district_pops[10] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '12':
        district_pops[11] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '13':
        district_pops[12] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '14':
        district_pops[13] += precinct_data['nodes'][i]['TOTALPOP']

print(district_pops)
dpops_percent_from_ideal = find_percentages(ideal_pop, district_pops)
print(dpops_percent_from_ideal)
from gerrychain import Graph
import geopandas as gpd
import sys

# first argument will be the path of the json file we are trying to convert 
# (precincts or census blocks geojson)
# second argument will be the abbrev of the state we are dealing with

geojson_path = sys.argv[1]
graph_path = sys.argv[2]

df = gpd.read_file(geojson_path)
df.to_crs(inplace=True, crs="epsg:26918")

dual_graph = Graph.from_geodataframe(df, ignore_errors=True)

dual_graph.to_json(graph_path)

# only going to move boundary precincts
# only going to move a few
# focus on the largest gap districts

import json as json
import math
from os import popen
import numpy as np
import sys
def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper

def find_percentages(ideal_pop, original_arr):
    pop_percentages = []
    for i in range(len(original_arr)):
        pop_percentages.append(truncate(((ideal_pop - original_arr[i])/ideal_pop), 4))
    return pop_percentages

def reached_goal(target_pos, target_neg, arr) -> bool:
    counter = 0
    num_districts = len(arr)
    for i in range(len(arr)):
        if arr[i] > 0 and arr[i] < target_pos:
            counter += 1
        elif arr[i] < 0 and arr[i] > target_neg:
            counter += 1
    if num_districts == counter:
        return True
    else:
        return False

def find_worst_dist(pops_array):
    worst_distr = []
    for i in range(len(pops_array)):
        if abs(pops_array[i]) > 0.3:
            if i < 8:
                worst_distr.append("0" + str(i+1))
            else:
                worst_distr.append(str(i+1))
    return worst_distr

precincts_path = sys.argv[1]
precinct_file = open(precincts_path, "r")
precinct_data = json.load(precinct_file)

district_pops = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
dpops_percent_from_ideal = []

pos_target = .03
neg_target = -.03

for i in range(len(precinct_data['nodes'])):
    if precinct_data['nodes'][i]['DistrictID'] == '01':
        district_pops[0] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '02':
        district_pops[1] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '03':
        district_pops[2] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '04':
        district_pops[3] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '05':
        district_pops[4] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '06':
        district_pops[5] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '07':
        district_pops[6] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '08':
        district_pops[7] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '09':
        district_pops[8] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '10':
        district_pops[9] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '11':
        district_pops[10] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '12':
        district_pops[11] += precinct_data['nodes'][i]['TOTALPOP']
    elif precinct_data['nodes'][i]['DistrictID'] == '13':
        district_pops[12] += precinct_data['nodes'][i]['TOTALPOP']
    # elif precinct_data['nodes'][i]['DistrictID'] == '14':
    #     district_pops[13] += precinct_data['nodes'][i]['TOTALPOP']
    
ideal_pop = sum(district_pops)/13
dpops_percent_from_ideal = find_percentages(ideal_pop, district_pops)


print(precinct_data["nodes"][0]["DistrictID"])
print(precinct_data["nodes"][0]["TOTALPOP"])
print(precinct_data["adjacency"][0][0]["id"])
index = precinct_data["adjacency"][0][0]["id"]
print(precinct_data["nodes"][index]["DistrictID"])

is_target_met = False
counter = 0
index = 0

w_new = []
for i in range(len(dpops_percent_from_ideal)):
    if abs(dpops_percent_from_ideal[i]) > 0.03:
        if i < 9:
            w_new.append("0" + str(i+1))
        else:
            w_new.append(str(i+1))
worst_dist = w_new
print(worst_dist)
# worst_dist = find_worst_dist(dpops_percent_from_ideal)
print(counter)
print(district_pops)
print(find_percentages(ideal_pop, district_pops))
print(worst_dist)
print(range(len(precinct_data['nodes'])))


for index in range(len(precinct_data['nodes'])):
    d1 = precinct_data['nodes'][index]['DistrictID']
    if d1 in worst_dist:
        j = 0
        while j in range(len(precinct_data['adjacency'][index])):
            p_id = int(precinct_data['adjacency'][index][j]['id'])
            d2 = precinct_data['nodes'][p_id]['DistrictID']
            if d2 != d1:
                d1_index = int(d1) - 1
                d2_index = int(d2) - 1
                if dpops_percent_from_ideal[d1_index] < 0:
                    # print("here 1")
                    counter +=1
                    precinct_data['nodes'][index]['DistrictID'] = d2
                    district_pops[d2_index] += precinct_data['nodes'][index]['TOTALPOP']
                    district_pops[d1_index] -= precinct_data['nodes'][index]['TOTALPOP']
                    dpops_percent_from_ideal = find_percentages(ideal_pop, district_pops)
                    break
                elif dpops_percent_from_ideal[d1_index] > 0:
                    # print("here 2")
                    counter +=1
                    precinct_data['nodes'][p_id]['DistrictID'] = d1
                    district_pops[d1_index] += precinct_data['nodes'][p_id]['TOTALPOP']
                    district_pops[d2_index] -= precinct_data['nodes'][p_id]['TOTALPOP']
                    dpops_percent_from_ideal = find_percentages(ideal_pop, district_pops)
                    break
                    # add d1 to d2
            j += 1

        w_new = []
        for i in range(len(dpops_percent_from_ideal)):
            if abs(dpops_percent_from_ideal[i]) > 0.03:
                if i < 9:
                    w_new.append("0" + str(i+1))
                else:
                    w_new.append(str(i+1))
        worst_dist = w_new


print(counter)
print(district_pops)
print(find_percentages(ideal_pop, district_pops))
print(worst_dist)

file_name = sys.argv[2]
file1 = open(file_name, "w")
json.dump(precinct_data, file1)

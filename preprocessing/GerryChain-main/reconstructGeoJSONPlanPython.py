import json as json
# import mapshaper as map
#import geopandas as gpd
import sys

precinct_geo_path = '/Users/nicoleniemiec/Desktop/416duo/416pelicans/client/src/data/north-carolina/processed/NC_PRECINCTS_ALL_INFO.geojson'
test_ga_path = sys.argv[1]

precinct_file = open(precinct_geo_path, "r")
precinct_data = json.load(precinct_file)

json_array = {"type": "FeatureCollection", "crs": {"type": "name", "properties": {"name": "urn:ogc:def:crs:EPSG::4269"}},
 "features": []}

test_file = open(test_ga_path, "r")
test_data = json.load(test_file)

for i in range(len(precinct_data["features"])):
    prec_id = int(precinct_data["features"][i]["properties"]["PrecinctID"])
    precinct_data["features"][i]["properties"]["DistrictID"] = test_data["nodes"][prec_id]["DistrictID"]
    
    precinct_data["features"][i]["properties"]["Boundary"] = test_data["nodes"][prec_id]["boundary_node"]
    json_array["features"].append(precinct_data["features"][i])

file_name = sys.argv[2]
file1 = open(file_name, "w")
json.dump(json_array, file1)
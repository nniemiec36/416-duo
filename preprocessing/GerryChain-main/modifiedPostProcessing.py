import json as json
# import mapshaper as map
#import geopandas as gpd
import sys
import os
from decimal import *
import math
from shapely.geometry import Polygon, mapping, shape
from shapely.ops import unary_union
import re

numDistricts = 14 # for nc

output_plans_path = sys.argv[1] # directory that contains remade geojsons
# make output directory 
directory = "processed/"+sys.argv[2] # subfolder
processed_path = os.path.join(output_plans_path, directory)    

#dir already made
itemCount = 0 # for file name when done
for item in os.listdir(processed_path): # for each seawulf district plan file
    pat = re.compile(r'[\d]+')
    planID = pat.search(str(item))
    planID = planID.group()
    print(planID)
    #final measures
    print(item)
    eqPopScore = 0
    planPopperScore = 0
    demTot = 0
    repTot =0
    numOpportunity = 0
    objectiveFunction = 0

    itemCount+=1
    print(item)
    plan_file = open(os.path.join(processed_path, item), "r")
    plan_data = json.load(plan_file)

    # measures = {"PresTot": [0,0,0,0,0,0,0,0,0,0,0,0,0], # total num pres votes for dist 1, dist 2, etc
    #             "PresD": [0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "PresR": [0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "Pop":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "AfAmer":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "White": [0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "Asian": [0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "His":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "PopVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "AfAmerVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "WhiteVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "AsianVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "HisVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0]}

    # percents = {"PresD": [0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "PresR": [0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "AfAmer":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "White": [0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "Asian": [0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "His":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "AfAmerVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "WhiteVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "AsianVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "HisVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0],
    #             "Polsby":[0,0,0,0,0,0,0,0,0,0,0,0,0], 
    #             "Opportunity":[0,0,0,0,0,0,0,0,0,0,0,0,0]}

# for GA ONLY
    measures = {"PresTot": [0,0,0,0,0,0,0,0,0,0,0,0,0,0], # total num pres votes for dist 1, dist 2, etc
                "PresD": [0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "PresR": [0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "Pop":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "AfAmer":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "White": [0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "Asian": [0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "His":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "PopVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "AfAmerVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "WhiteVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "AsianVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "HisVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0,0]}

    percents = {"PresD": [0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "PresR": [0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "AfAmer":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "White": [0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "Asian": [0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "His":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "AfAmerVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "WhiteVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "AsianVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "HisVAP":[0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                "Polsby":[0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
                "Opportunity":[0,0,0,0,0,0,0,0,0,0,0,0,0,0]}

    # Polsby poppper
    districtPolygons = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],] # each list is list of precincts shape objs in district given by id
    districtsOnly = [] # district shape objects returned from union
    popperScores = []
    for i in range(len(plan_data["features"])): # grab all precinct geometries form file and put as shape objs into arr by dist #
        p = shape(json.loads(json.dumps(plan_data['features'][i]['geometry'])))
        dist = int(plan_data['features'][i]["properties"]['DistrictID']) -1
        districtPolygons[dist].append(p)

    for polyList in districtPolygons: # for list of each district 
        if(polyList==[]): # nc doesnt have 14
            continue
        district = unary_union(polyList)
        districtsOnly.append(district) 

    count = 0
    for dist in districtsOnly: # for each district in this plan
        curDistNum = count
        area = dist.area
        print(area)
        perimeter = dist.length
        print(perimeter)
        polsby = (4 * math.pi) * (area / (perimeter**2))
        print("Dist #"+str(count+1)+" is "+str(polsby))
        percents["Polsby"][curDistNum] = float(Decimal(polsby).quantize(Decimal('1e-5')))
        popperScores.append(polsby)
        count +=1

    planPopperScore = sum(popperScores) / len(popperScores)
    print("plan popper score "+str(planPopperScore)) # avg polsby popper from all dists

    # go through each precinct in the plan to get info
    for i in range(len(plan_data['features'])):
        plan = plan_data['features'][i]['properties']
        dist = plan['DistrictID']
        PresD = plan['PRESDEM']
        PresR = plan['PRESREP']
        PresTot = plan['PRESTOT']
        AfAmer = plan['TOTALAA']
        White = plan['TOTALWHITE']
        Asian = plan['TOTALASIAN']
        His = plan['TOTALHIS']
        Pop = plan['TOTALPOP']
        AfAmerVAP = plan['TOTALVAPAA']
        WhiteVAP = plan['TOTALVAPWHITE']
        AsianVAP = plan['TOTALVAPASIAN']
        HisVAP = plan['TOTALVAPHIS']
        VAP = plan['TOTALVAP']

        #update measuers dictionary with totals per district
        distIndex = int(dist)-1
        measures["PresD"][distIndex]+= PresD # total democratic votes for district one in all plans
        measures["PresR"][distIndex]+= PresR
        measures["PresTot"][distIndex]+= PresTot
        measures["Pop"][distIndex]+= Pop
        measures["AfAmer"][distIndex]+= AfAmer
        measures["White"][distIndex]+= White
        measures["Asian"][distIndex]+= Asian
        measures["His"][distIndex]+= His
        measures["PopVAP"][distIndex]+= VAP
        measures["AfAmerVAP"][distIndex]+= AfAmerVAP
        measures["WhiteVAP"][distIndex]+= WhiteVAP
        measures["AsianVAP"][distIndex]+= AsianVAP
        measures["HisVAP"][distIndex]+= HisVAP


        # last entry, do calculations and add percentage dictionary
        if i == (len(plan_data['features']) - 1):
            equalPopSum = 0
            statePop =0
            for j in range(len(measures["Pop"])):
                statePop+=measures['Pop'][j]

            idealPop = statePop / numDistricts 
            for key in measures: # for each comparison basis
                print(measures)
                for index in range(len(measures[key])): # for each district in this comparison measure
                    if(key== "PresD" or key == "PresR"):
                        percents[key][index] = float(Decimal(measures[key][index] / measures['PresTot'][index]).quantize(Decimal('1e-5')))
                    elif (key=='AfAmer' or key== 'White' or key=="Asian" or key =="His"):
                        percents[key][index] = float(Decimal(measures[key][index] / measures['Pop'][index]).quantize(Decimal('1e-5')))
                        if(float(percents[key][index]) > .5):
                            percents['Opportunity'][index] = key
                            if(key=='AfAmer' or key=="Asian" or key =="His"):
                                numOpportunity+=1
                    elif (key=='AfAmerVAP' or key== 'WhiteVAP' or key=="AsianVAP" or key =="HisVAP"):
                        percents[key][index] = float(Decimal(measures[key][index] / measures['PopVAP'][index]).quantize(Decimal('1e-5')))
                    else:
                        continue

                demTot = sum(percents["PresD"])/len(percents['PresD']) # percent of democratic votes for entire plan
                repTot = sum(percents["PresR"])/len(percents['PresR'])

            # CALCULATE POPULATION EQUALITY 
            print(measures['Pop'])
            print(sum(measures["Pop"]))
            idealPop = int(idealPop)
            print(idealPop)
            for i in range(len(measures['Pop'])):
                print(((measures['Pop'][i]/idealPop)-1)**2)
                equalPopSum+=((measures['Pop'][i]/idealPop)-1)**2 # dist pop in plan / ideal

            print("eq pop sum "+str(equalPopSum))
            eqPopScore = math.sqrt(equalPopSum)
            eqPopScore = 1 - eqPopScore
            print("eq pop score "+str(eqPopScore))

            #alt method
            print((max(measures['Pop'])-min(measures['Pop']))/idealPop)

            
            # print("POP EQ SCORE : "+str(math.sqrt(equalPopSum)))
            # print("NUM OPPORTUNITY: "+str(numOpportunity))
            # print("dem tot "+str(demTot)+" rep tot "+str(repTot))
                
    # all relevant election and demographic info is in the percents dictionary for this plan, add it to the top of the geojson file
    # print(measures)
    # print()
    # print(percents)
    objectiveFunc1 = 0.7 * float(eqPopScore) + 0.3 * float(planPopperScore)
    objectiveFunc2 = 0.6 * float(eqPopScore) + 0.4 * float(planPopperScore)
    planMeasures= {
        "Polsby": float(Decimal(planPopperScore).quantize(Decimal('1e-5'))),
        "PopulationEquality": float(Decimal(eqPopScore).quantize(Decimal('1e-5'))),
        "PercentDem": float(Decimal(demTot).quantize(Decimal('1e-5'))),
        "PercentRep": float(Decimal(repTot).quantize(Decimal('1e-5'))),
        "NumberOpportunity": numOpportunity,
        "ObjectiveFunct1": float(Decimal(objectiveFunc1).quantize(Decimal('1e-5'))),
        "ObjectiveFunct2": float(Decimal(objectiveFunc2).quantize(Decimal('1e-5'))),
    }
    plan_data["PlanMeasures"] = planMeasures
    plan_data['percents'] = percents
    feat = plan_data['features']
    del plan_data['features']
    print()
    print(plan_data)
    plan_data['features'] = feat
    file_name = output_plans_path+"/chosenProcessed/chosen_plan_"+str(planID)+".geojson"
    file1 = open(file_name, "w")
    json.dump(plan_data, file1)

    


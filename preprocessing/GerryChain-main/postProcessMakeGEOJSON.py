import json as json
# import mapshaper as map
#import geopandas as gpd
import sys
import os
from decimal import *
import math
from shapely.geometry import Polygon, mapping, shape
from shapely.ops import unary_union


list_dist = {"A":"01", "B":"02", "C":"03", "D":"04", "E":"05", "F":"06", "G":"07", "H":"08", "I":"09", "J":"10", "K":"11", "L":"12", "M":"13", "N":"14", "O":"15", "P":"16", "Q":"17", "R":"18"}


geo_path = sys.argv[1] # all info precincts file
geo_file = open(geo_path, "r")
geo_data = json.load(geo_file)


output_plans_path = sys.argv[2] # directory where processed plans will go should be output/ga/processed

directory = "processed"
processed_path = os.path.join(output_plans_path, directory)

numDistricts = 13 # for georgia


count = 0
for item in os.listdir(output_plans_path):
    print(item)
    if os.path.isfile(os.path.join(output_plans_path, item)) and item.endswith('.json'):
        plan_file = open(os.path.join(output_plans_path, item), "r")
        plan_data = json.load(plan_file)
        count+=1
        for i in range(len(geo_data['features'])):
            geoPrecinct = geo_data['features'][i]['properties']['PrecinctID']
            for j in range(len(plan_data['nodes'])):
                planPrecinct = plan_data['nodes'][j]['id']
                if(geoPrecinct==planPrecinct):
                    planDistrict = plan_data['nodes'][j]['district']
                    planDistrict = list_dist.get(planDistrict)
                    geo_data['features'][i]['properties']['DistrictID'] = planDistrict

        file_name = processed_path+"/plan_reformatted_"+str(count)+".geojson"
        file1 = open(file_name, "w")
        json.dump(geo_data, file1)

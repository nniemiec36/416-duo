import json as json
import sys
import os
from decimal import *
import numpy as np

def reject_outliers(data, m = 2.):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d/mdev if mdev else 0.
    return data[s<m]

def getPlotPoints(basis):
    # for dist in basis:
    for i in range(len(basis)):
        dist = basis[i]
        # print(dist)
        arr = np.array(dist)
        arr = reject_outliers(arr) # no more giant max or .0001 min
        dist = list(arr)
        # print(dist)
        med = np.percentile(dist, 50)
        third = np.percentile(dist, 75)
        first = np.percentile(dist, 25)
        mini = min(dist)
        maxi = max(dist)
        dist.clear()
        dist.append(med)
        dist.append(third)
        dist.append(first)
        dist.append(mini)
        dist.append(maxi)
        basis[i] = dist
    # print("ENMPTY")
    # print(PresD)

processed_path = './output/ga/processedWithData'
# processed_path = sys.argv[1]

nameList = ["Presidential D", "Presidential R", "African American",'White','Asian','Hispanic','Af. Amer. VAP','White VAP','Asian VAP','Hispanic VAP']

PresD=     [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 
PresR=     [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 
AfAmer=    [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 
AfAmerVAP= [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 
White=     [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 
WhiteVAP=  [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 
Asian=     [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 
AsianVAP=  [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 
Hisp=      [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 
HispVAP=   [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 
Polsby=    [[],[],[],[],[],[],[],[],[],[],[],[],[],[]] # nested arr, by district 



for item in os.listdir(processed_path): # for each processed plan
        plan_file = open(os.path.join(processed_path, item), "r")
        plan_data = json.load(plan_file)
        for i in range(len(plan_data['percents']['PresD'])): # for each district in this plan 
            PresD[i].append(float(plan_data["percents"]['PresD'][i])) # add the percent to the PresD arr at its index i.e. district
            
            PresR[i].append(float(plan_data["percents"]['PresR'][i]))
            AfAmer[i].append(float(plan_data["percents"]['AfAmer'][i]))
            AfAmerVAP[i].append(float(plan_data["percents"]['AfAmerVAP'][i]))
            White[i].append(float(plan_data["percents"]['White'][i]))
            WhiteVAP[i].append(float(plan_data["percents"]['WhiteVAP'][i]))
            Asian[i].append(float(plan_data["percents"]['Asian'][i]))
            AsianVAP[i].append(float(plan_data["percents"]['AsianVAP'][i]))
            Hisp[i].append(float(plan_data["percents"]['His'][i]))
            HispVAP[i].append(float(plan_data["percents"]['HisVAP'][i]))
            Polsby[i].append(float(plan_data["percents"]['Polsby'][i]))
# print(PresD)


# print(PresD)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(PresD)

# print(PresR)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(PresR)

# print(AfAmer)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(AfAmer)

# print(AfAmerVAP)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(AfAmerVAP)

# print(White)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(White)

# print(WhiteVAP)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(WhiteVAP)

# print(Asian)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(Asian)

# print(AsianVAP)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(AsianVAP)

# print(Hisp)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(Hisp)

# print(HispVAP)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(HispVAP)

# print(Polsby)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(Polsby)



boxWhisk = {}
boxWhisk['PresD'] = PresD
boxWhisk['PresR'] = PresR
boxWhisk['AfAmer'] = AfAmer
boxWhisk['White'] = White
boxWhisk['Asian'] = Asian
boxWhisk['His'] = Hisp
boxWhisk['AfAmerVAP'] = AfAmerVAP
boxWhisk['WhiteVAP'] = WhiteVAP
boxWhisk['AsianVAP'] = AsianVAP
boxWhisk['HisVAP'] = HispVAP
boxWhisk['Polsby'] = Polsby

#CHECK
arr =[ 0.45879,0.38378,0.44918, 0.61397,0.61596,0.25254]
third = np.percentile(arr, 75)
print(third)
print("THIRD ^^^^^^^")

print(boxWhisk)
file_name = "../../client/src/data/boxWhisk.json"
file1 = open(file_name, "w")
json.dump(boxWhisk, file1)

import json as json
import sys
import os
from decimal import *
import numpy as np

def removeOutliers(x):
    a = np.array(x)
    upper_quartile = np.percentile(a, 75)
    lower_quartile = np.percentile(a, 25)
    IQR = (upper_quartile - lower_quartile) *0.6
    quartileSet = (lower_quartile - IQR, upper_quartile + IQR)
    resultList = []
    for y in a.tolist():
        if y >= quartileSet[0] and y <= quartileSet[1]:
            resultList.append(y)
    return resultList

def reject_outliers(data, m = .8):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d/mdev if mdev else 0.
    return data[s<m]

def getPlotPoints(basis):
    # for dist in basis:
    for i in range(len(basis)):
        dist = basis[i]
        # print(dist)
        arr = np.array(dist)
        arr = removeOutliers(arr) # no more giant max or .0001 min
        dist = list(arr)
        # print(dist)
        med = np.percentile(dist, 50)
        third = np.percentile(dist, 75)
        first = np.percentile(dist, 25)
        mini = min(dist)
        maxi = max(dist)
        dist.clear()
        dist.append(med)
        dist.append(third)
        dist.append(first)
        dist.append(mini)
        dist.append(maxi)
        basis[i] = dist
    # print("ENMPTY")
    # print(PresD)

# processed_path = './output/ga/processedWithData'
processed_path = './ncBoxWhiskUnfiltered.json'
jsonn = open(processed_path, "r")
whiskData = json.load(jsonn)

nameList = ["Presidential D", "Presidential R", "African American",'White','Asian','Hispanic','Af. Amer. VAP','White VAP','Asian VAP','Hispanic VAP']

PresD=     whiskData['PresD'] # nested arr, by district 
PresR=     whiskData['PresR'] # nested arr, by district 
AfAmer=    whiskData['AfAmer'] # nested arr, by district 
AfAmerVAP= whiskData['AfAmerVAP'] # nested arr, by district 
White=     whiskData['White'] # nested arr, by district 
WhiteVAP=  whiskData['WhiteVAP'] # nested arr, by district 
Asian=     whiskData['Asian'] # nested arr, by district 
AsianVAP= whiskData['AsianVAP'] # nested arr, by district 
Hisp=      whiskData['His'] # nested arr, by district 
HispVAP=   whiskData['HisVAP'] # nested arr, by district 
Polsby=    whiskData['Polsby'] # nested arr, by district 


# print(PresD)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(PresD)

# print(PresR)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(PresR)

# print(AfAmer)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(AfAmer)

# print(AfAmerVAP)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(AfAmerVAP)

# print(White)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(White)

# print(WhiteVAP)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(WhiteVAP)

# print(Asian)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(Asian)

# print(AsianVAP)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(AsianVAP)

# print(Hisp)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(Hisp)

# print(HispVAP)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(HispVAP)

# print(Polsby)
# print("AAAAAAAAAAAAAAA")
getPlotPoints(Polsby)



boxWhisk = {}
boxWhisk['PresD'] = PresD
boxWhisk['PresR'] = PresR
boxWhisk['AfAmer'] = AfAmer
boxWhisk['White'] = White
boxWhisk['Asian'] = Asian
boxWhisk['His'] = Hisp
boxWhisk['AfAmerVAP'] = AfAmerVAP
boxWhisk['WhiteVAP'] = WhiteVAP
boxWhisk['AsianVAP'] = AsianVAP
boxWhisk['HisVAP'] = HispVAP
boxWhisk['Polsby'] = Polsby

print(boxWhisk)
file_name = "../../client/src/data/ncBoxWhisk.json"
file1 = open(file_name, "w")
json.dump(boxWhisk, file1)

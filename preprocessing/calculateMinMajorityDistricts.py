import json as json
import math as math

cd_path_ga = './GA_CD_ALL_INFO.json'
cd_file_ga = open(cd_path_ga, "r")
cd_data_ga = json.load(cd_file_ga)

cd_path_nc = './NC_CD_ALL_INFO.json'
cd_file_nc = open(cd_path_nc, "r")
cd_data_nc = json.load(cd_file_nc)

cd_path_pa = './PA_CD_ALL_INFO.json'
cd_file_pa = open(cd_path_pa, "r")
cd_data_pa = json.load(cd_file_pa)

# need for both total and vap

min_maj_ga = {}
min_maj_nc = {}
min_maj_pa = {}

for i in range(len(cd_data_ga['features'])):
    white_pop = cd_data_ga['features'][i]['properties']['TOTALWHITE']
    max_pop = 'WHITE'
    max_pop_num = white_pop
    if(cd_data_ga['features'][i]['properties']['TOTALASIAN'] > max_pop_num):
        max_pop = 'ASIAN'
        max_pop_num = cd_data_ga['features'][i]['properties']['TOTALASIAN']
    if(cd_data_ga['features'][i]['properties']['TOTALAA'] > max_pop_num):
        max_pop = 'BLACK'
        max_pop_num = cd_data_ga['features'][i]['properties']['TOTALAA']
    if(cd_data_ga['features'][i]['properties']['TOTALHIS'] > max_pop_num):
        max_pop = 'HIS'
        max_pop_num = cd_data_ga['features'][i]['properties']['TOTALAHIS']
    min_maj_ga[str(i)] = max_pop

for i in range(len(cd_data_nc['features'])):
    white_pop = cd_data_nc['features'][i]['properties']['TOTALWHITE']
    max_pop = 'WHITE'
    max_pop_num = white_pop
    if(cd_data_nc['features'][i]['properties']['TOTALASIAN'] > max_pop_num):
        max_pop = 'ASIAN'
        max_pop_num = cd_data_nc['features'][i]['properties']['TOTALASIAN']
    if(cd_data_nc['features'][i]['properties']['TOTALAA'] > max_pop_num):
        max_pop = 'BLACK'
        max_pop_num = cd_data_nc['features'][i]['properties']['TOTALAA']
    if(cd_data_nc['features'][i]['properties']['TOTALHIS'] > max_pop_num):
        max_pop = 'HIS'
        max_pop_num = cd_data_nc['features'][i]['properties']['TOTALAHIS']
    min_maj_nc[str(i)] = max_pop

for i in range(len(cd_data_pa['features'])):
    white_pop = cd_data_pa['features'][i]['properties']['TOTALWHITE']
    max_pop = 'WHITE'
    max_pop_num = white_pop
    if(cd_data_pa['features'][i]['properties']['TOTALASIAN'] > max_pop_num):
        max_pop = 'ASIAN'
        max_pop_num = cd_data_pa['features'][i]['properties']['TOTALASIAN']
    if(cd_data_pa['features'][i]['properties']['TOTALAA'] > max_pop_num):
        max_pop = 'BLACK'
        max_pop_num = cd_data_pa['features'][i]['properties']['TOTALAA']
    if(cd_data_pa['features'][i]['properties']['TOTALHIS'] > max_pop_num):
        max_pop = 'HIS'
        max_pop_num = cd_data_pa['features'][i]['properties']['TOTALAHIS']
    min_maj_pa[str(i)] = max_pop

min_maj_vap_ga = {}
min_maj_vap_nc = {}
min_maj_vap_pa = {}

for i in range(len(cd_data_ga['features'])):
    white_pop = cd_data_ga['features'][i]['properties']['TOTALVAPWHITE']
    max_pop = 'WHITE'
    max_pop_num = white_pop
    if(cd_data_ga['features'][i]['properties']['TOTALVAPASIAN'] > max_pop_num):
        max_pop = 'ASIAN'
        max_pop_num = cd_data_ga['features'][i]['properties']['TOTALVAPASIAN']
    if(cd_data_ga['features'][i]['properties']['TOTALVAPAA'] > max_pop_num):
        max_pop = 'BLACK'
        max_pop_num = cd_data_ga['features'][i]['properties']['TOTALVAPAA']
    if(cd_data_ga['features'][i]['properties']['TOTALVAPHIS'] > max_pop_num):
        max_pop = 'HIS'
        max_pop_num = cd_data_ga['features'][i]['properties']['TOTALVAPHIS']
    min_maj_vap_ga[str(i)] = max_pop

for i in range(len(cd_data_nc['features'])):
    white_pop = cd_data_nc['features'][i]['properties']['TOTALVAPWHITE']
    max_pop = 'WHITE'
    max_pop_num = white_pop
    if(cd_data_nc['features'][i]['properties']['TOTALVAPASIAN'] > max_pop_num):
        max_pop = 'ASIAN'
        max_pop_num = cd_data_nc['features'][i]['properties']['TOTALVAPASIAN']
    if(cd_data_nc['features'][i]['properties']['TOTALVAPAA'] > max_pop_num):
        max_pop = 'BLACK'
        max_pop_num = cd_data_nc['features'][i]['properties']['TOTALVAPAA']
    if(cd_data_nc['features'][i]['properties']['TOTALVAPHIS'] > max_pop_num):
        max_pop = 'HIS'
        max_pop_num = cd_data_nc['features'][i]['properties']['TOTALVAPHIS']
    min_maj_vap_nc[str(i)] = max_pop

for i in range(len(cd_data_pa['features'])):
    white_pop = cd_data_pa['features'][i]['properties']['TOTALWHITE']
    max_pop = 'WHITE'
    max_pop_num = white_pop
    if(cd_data_pa['features'][i]['properties']['TOTALASIAN'] > max_pop_num):
        max_pop = 'ASIAN'
        max_pop_num = cd_data_pa['features'][i]['properties']['TOTALASIAN']
    if(cd_data_pa['features'][i]['properties']['TOTALAA'] > max_pop_num):
        max_pop = 'BLACK'
        max_pop_num = cd_data_pa['features'][i]['properties']['TOTALAA']
    if(cd_data_pa['features'][i]['properties']['TOTALHIS'] > max_pop_num):
        max_pop = 'HIS'
        max_pop_num = cd_data_pa['features'][i]['properties']['TOTALAHIS']
    min_maj_vap_pa[str(i)] = max_pop
import json as json
import sys

if len(sys.argv) != 2:
    print("provide the file to insert")
    exit(0)

json_path = sys.argv[1]
print(json_path)

census_blocks = []
# border_precinct_ids = ['2', '3', '4', '7', '9', '19', '20', '21', '22', '23', '99', '100', '147', '161', '166', '168', '208', '211', '230', '231', '234', '372', '373', '382', '418', '419', '423', '425', '426', '433', '506', '512', '593', '599', '600', '601', '609', '613', '614', '617', '629', '630', '636', '639', '722', '725', '728', '729', '733', '735', '736', '877', '878', '887', '892', '908', '912', '916', '919', '936', '938', '940', '1053', '1054', '1056', '1127', '1128', '1129', '1130', '1131', '1132', '1137', '1138', '1139', '1152', '1153', '1155', '1216', '1224', '1252', '1253', '1296', '1298', '1299', '1300', '1302', '1305', '1306', '1307', '1335', '1337', '1338', '1365', '1372', '1373', '1375', '1377', '1378', '1383', '1393', '1394', '1414', '1420', '1422', '1423', '1425', '1443', '1457', '1461', '1466', '1467', '1472', '1473', '1494', '1667', '1668', '1671', '1673', '1927', '1929', '1940', '2523', '2524', '2526', '2528', '2529', '2537', '2544', '2549', '2551', '2554', '2566', '2567', '2572', '2594', '2607', '2608', '2624', '2625', '2626', '2663', '2676', '2677', '2679', '2681', '2687', '2689', '2690', '2693', '2694', '2695', '2696', '2697'] # this was for georgia

# border_precinct_ids = ['15', '22', '23', '24', '28', '37', '38', '39', '44', '47', '62', '66', '121', '125', '126', '128', '141', '146', '172', '177', '178', '195', '205', '210', '252', '253', '255', '256', '305', '309', '335', '337', '340', '341', '343', '344', '348', '349', '350', '415', '442', '444', '482', '483', '485', '502', '514', '515', '516', '519', '521', '522', '524', '532', '533', '534', '537', '538', '540', '544', '545', '590', '664', '668', '670', '671', '673', '680', '828', '832', '833', '840', '844', '845', '846', '927', '929', '930', '936', '1230', '1232', '1345', '1351', '1430', '1431', '1435', '1437', '1572', '1595', '1693', '1696', '1698', '1709', '1710', '1711', '1734', '1735', '1741', '1742', '1744', '1761', '1766', '1793', '1794', '1799', '1831', '1835', '1836', '1922', '1928', '1933', '1934', '1935', '1937', '1941', '1943', '1944', '1948', '2001', '2002', '2003', '2004', '2006', '2030', '2034', '2076', '2078', '2188', '2196', '2243', '2246', '2279', '2338', '2339', '2341', '2343', '2384', '2386', '2387', '2388', '2396', '2401', '2423', '2424', '2432', '2433', '2436', '2438', '2440', '2441', '2442', '2447', '2448', '2459', '2474', '2499', '2500', '2509', '2510', '2513', '2526', '2539', '2540', '2550', '2553', '2554', '2560', '2561', '2562', '2567', '2568', '2570', '2580', '2581', '2583', '2601', '2602', '2609', '2627', '2641', '2648', '2652', '2654', '2656', '2659', '2660'] # this was for north carolina

border_precinct_ids = ['1', '2', '7', '16', '22', '34', '41', '95', '102', '199', '212', '220', '223', '229', '230', '235', '242', '243', '257', '258', '261', '298', '299', '771', '773', '786', '792', '834', '835', '839', '843', '889', '896', '898', '904', '1953', '1955', '1956', '1961', '1963', '1964', '1965', '1967', '1989', '1999', '2033', '2035', '2039', '2054', '2068', '2076', '2077', '2078', '2079', '2080', '2092', '2117', '2118', '2141', '2145', '2154', '2174', '2177', '2181', '2183', '2184', '2186', '2188', '2189', '2196', '2197', '2591', '2604', '2605', '2616', '2726', '2729', '2735', '2739', '2743', '2744', '2763', '2771', '2772', '2955', '2964', '2965', '2980', '2981', '2987', '3010', '3015', '3016', '3017', '3021', '3103', '3112', '3113', '3143', '3169', '3222', '3226', '3232', '3233', '3234', '3237', '3238', '3243', '3350', '3351', '3352', '3353', '3354', '3356', '3358', '3360', '3361', '3362', '3365', '3366', '3492', '3517', '3548', '3553', '3558', '3563', '3565', '3566', '3573', '3574', '3807', '3808', '3837', '3843', '3861', '3862', '3867', '4037', '4075', '4076', '4157', '4159', '4163', '4411', '4514', '4629', '4661', '4674', '4677', '4680', '4690', '4732', '4750', '4752', '4753', '4791', '4806', '4811', '4816', '4844', '4848', '4857', '4859', '4996', '4998', '4999', '5000', '5001', '5091', '5113', '5119', '5121', '5130', '5131', '5210', '5212', '5213', '5270', '5271', '5341', '5359', '5361', '5382', '5390', '5464', '5499', '5530', '5535', '5539', '5583', '5588', '5603', '5613', '5621', '5623', '5628', '5638', '5661', '5666', '5718', '5730', '5751', '5763', '5816', '5817', '5823', '5829', '5832', '5859', '5860', '5921', '5957', '5993', '6007', '6017', '6020', '6085', '6111', '6270', '6436', '6445', '6447', '6499', '6539', '6543', '6706', '6713', '6742', '6761', '6762', '6810', '7084', '7085', '7240', '7301', '7303', '7378', '7422', '7470', '7487', '7505', '7507', '7572', '7617', '7619', '7668', '7701', '7760', '7773', '7999', '8182', '8217', '8225', '8232', '8237', '8588', '8594', '8595', '8596', '8597', '8601', '8609', '8610', '8641', '8660', '8661', '8663', '8664', '8665', '8717', '8729', '8786', '8788', '8794', '8828', '8838']

file = open(json_path, "r")
data = json.load(file)
json_array = {"directed": False, "multigraph": False, "graph": [], "nodes": [], "adjacency": []}
print(data['nodes'][0]['PRECINCTID'])
block_count = 0

for i in range(len(data['nodes'])):
    if data['nodes'][i]['PRECINCTID'] in border_precinct_ids:
        json_array['nodes'].append(data['nodes'][i])
        json_array['adjacency'].append(data['adjacency'][i])
        census_blocks.append(data['nodes'][i]['CBID'])
        block_count += 1

#print(block_count)
print(census_blocks)
new_file_name = file_name = sys.argv[1].split("-census-blocks-2-2.json")[0] + "-in-border-precincts.json"
file1 = open(new_file_name, "w")
print(block_count)
print(len(json_array['nodes']))
json.dump(json_array, file1)
import json as json
import math
import sys

precinct_geo_path = sys.argv[1]
state = sys.argv[2]
precinct_file = open(precinct_geo_path, "r")
precinct_data = json.load(precinct_file)
json_array = {"type": "FeatureCollection", "crs": {"type": "name", "properties": {"name": "urn:ogc:def:crs:EPSG::4269"}}, "PlanMeasures": {}, "percents": {},
 "features": []}
 
if state == 'ga':
    json_obj = [
        {"type": "Feature", "properties": { "DistrictID": "01", "State": "GA", "STATEFP20": "13", "GEOID20": "1301", "CD116FP": "01", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "02", "State": "GA", "STATEFP20": "13", "GEOID20": "1302", "CD116FP": "02", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "03", "State": "GA", "STATEFP20": "13", "GEOID20": "1303", "CD116FP": "03", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "04", "State": "GA", "STATEFP20": "13", "GEOID20": "1304", "CD116FP": "04", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "05", "State": "GA", "STATEFP20": "13", "GEOID20": "1305", "CD116FP": "05", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "06", "State": "GA", "STATEFP20": "13", "GEOID20": "1306", "CD116FP": "06", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "07", "State": "GA", "STATEFP20": "13", "GEOID20": "1307", "CD116FP": "07", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "08", "State": "GA", "STATEFP20": "13", "GEOID20": "1308", "CD116FP": "08", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "09", "State": "GA", "STATEFP20": "13", "GEOID20": "1309", "CD116FP": "09", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "10", "State": "GA", "STATEFP20": "13", "GEOID20": "1310", "CD116FP": "10", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "11", "State": "GA", "STATEFP20": "13", "GEOID20": "1311", "CD116FP": "11", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "12", "State": "GA", "STATEFP20": "13", "GEOID20": "1312", "CD116FP": "12", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "13", "State": "GA", "STATEFP20": "13", "GEOID20": "1313", "CD116FP": "13", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "14", "State": "GA", "STATEFP20": "13", "GEOID20": "1314", "CD116FP": "14", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}}
    ]
else:
    json_obj = [
        {"type": "Feature", "properties": { "DistrictID": "01", "State": "NC", "STATEFP20": "37", "GEOID20": "3701", "CD116FP": "01", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "02", "State": "NC", "STATEFP20": "37", "GEOID20": "3702", "CD116FP": "02", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "03", "State": "NC", "STATEFP20": "37", "GEOID20": "3703", "CD116FP": "03", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "04", "State": "NC", "STATEFP20": "37", "GEOID20": "3704", "CD116FP": "04", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "05", "State": "NC", "STATEFP20": "37", "GEOID20": "3705", "CD116FP": "05", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "06", "State": "NC", "STATEFP20": "37", "GEOID20": "3706", "CD116FP": "06", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "07", "State": "NC", "STATEFP20": "37", "GEOID20": "3707", "CD116FP": "07", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "08", "State": "NC", "STATEFP20": "37", "GEOID20": "3708", "CD116FP": "08", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "09", "State": "NC", "STATEFP20": "37", "GEOID20": "3709", "CD116FP": "09", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "10", "State": "NC", "STATEFP20": "37", "GEOID20": "3710", "CD116FP": "10", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "11", "State": "NC", "STATEFP20": "37", "GEOID20": "3711", "CD116FP": "11", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "12", "State": "NC", "STATEFP20": "37", "GEOID20": "3712", "CD116FP": "12", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}},
        {"type": "Feature", "properties": { "DistrictID": "13", "State": "NC", "STATEFP20": "37", "GEOID20": "3713", "CD116FP": "13", "TOTALPOP": 0, "TOTALHIS": 0, "TOTALNONHIS": 0, "TOTALASIAN": 0, "TOTALWHITE": 0, "TOTALAA": 0, "TOTALVAP": 0, "TOTALVAPHIS": 0, "TOTALVAPNONHIS": 0, "TOTALVAPASIAN": 0, "TOTALVAPWHITE": 0,"TOTALVAPAA": 0, "PRESREP": 0, "PRESDEM": 0, "PRESTOT": 0, "SENATER": 0, "SENATED": 0, "SENATETOT": 0 }, "geometry": {}}
    ]

for i in range(len(precinct_data["features"])):
    d_id = precinct_data["features"][i]["properties"]["DistrictID"]
    d_index = int(d_id) - 1
    json_obj[d_index]["properties"]["TOTALPOP"] += precinct_data["features"][i]["properties"]["TOTALPOP"]
    json_obj[d_index]["properties"]["TOTALHIS"] += precinct_data["features"][i]["properties"]["TOTALHIS"]
    json_obj[d_index]["properties"]["TOTALNONHIS"] += precinct_data["features"][i]["properties"]["TOTALNONHIS"]
    json_obj[d_index]["properties"]["TOTALASIAN"] += precinct_data["features"][i]["properties"]["TOTALASIAN"]
    json_obj[d_index]["properties"]["TOTALWHITE"] += precinct_data["features"][i]["properties"]["TOTALWHITE"]
    json_obj[d_index]["properties"]["TOTALAA"] += precinct_data["features"][i]["properties"]["TOTALAA"]
    json_obj[d_index]["properties"]["TOTALVAP"] += precinct_data["features"][i]["properties"]["TOTALVAP"]
    json_obj[d_index]["properties"]["TOTALVAPHIS"] += precinct_data["features"][i]["properties"]["TOTALVAPHIS"]
    json_obj[d_index]["properties"]["TOTALVAPNONHIS"] += precinct_data["features"][i]["properties"]["TOTALVAPNONHIS"]
    json_obj[d_index]["properties"]["TOTALVAPASIAN"] += precinct_data["features"][i]["properties"]["TOTALVAPASIAN"]
    json_obj[d_index]["properties"]["TOTALVAPWHITE"] += precinct_data["features"][i]["properties"]["TOTALVAPWHITE"]
    json_obj[d_index]["properties"]["TOTALVAPAA"] += precinct_data["features"][i]["properties"]["TOTALVAPAA"]
    json_obj[d_index]["properties"]["PRESREP"] += precinct_data["features"][i]["properties"]["PRESREP"]
    json_obj[d_index]["properties"]["PRESDEM"] += precinct_data["features"][i]["properties"]["PRESDEM"]
    json_obj[d_index]["properties"]["PRESTOT"] += precinct_data["features"][i]["properties"]["PRESTOT"]
    json_obj[d_index]["properties"]["SENATED"] += precinct_data["features"][i]["properties"]["SENATED"]
    json_obj[d_index]["properties"]["SENATER"] += precinct_data["features"][i]["properties"]["SENATER"]
    json_obj[d_index]["properties"]["SENATETOT"] += precinct_data["features"][i]["properties"]["SENATETOT"]

json_array["PlanMeasures"] = precinct_data["PlanMeasures"]
json_array["percents"] = precinct_data["percents"]

geo_path = sys.argv[3]
geo_file = open(geo_path, "r")
geo_data = json.load(geo_file)

# print(geo_data["features"][0]["geometry"])
# print(json_obj[0]["geometry"])
for i in range(len(geo_data["features"])):
    d_id = geo_data["features"][i]["properties"]["DistrictID"]
    d_index = int(d_id) - 1
    json_obj[d_index]["geometry"] = geo_data["features"][i]["geometry"]

json_array["features"] = json_obj

final_path = sys.argv[4]
file1 = open(final_path, "w")
json.dump(json_array, file1)




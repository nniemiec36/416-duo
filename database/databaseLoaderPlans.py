import geopandas as gpd
import pandas as pd
import numpy as np
import sys
import json

file = open("/Users/nicoleniemiec/Desktop/416duo/416pelicans/client/src/data/georgia/plans/district-plan-ga9.geojson", "r")
measure_data = json.load(file)

plan_measures = measure_data["PlanMeasures"]
percents = measure_data["percents"]

plan_data = gpd.read_file("/Users/nicoleniemiec/Desktop/416duo/416pelicans/client/src/data/georgia/plans/district-plan-ga9.geojson")
print(plan_data)

population_data = plan_data[["TOTALPOP", "TOTALHIS", "TOTALNONHIS", "TOTALASIAN", "TOTALWHITE", "TOTALAA", "TOTALVAP", "TOTALVAPHIS", "TOTALVAPNONHIS", "TOTALVAPASIAN", "TOTALVAPWHITE", "TOTALVAPAA", "PRESTOT", "PRESREP", "PRESDEM"]]

population_data = population_data.rename(columns={'TOTALPOP': 'total_population', 'TOTALHIS': 'total_his', 'TOTALNONHIS':'total_non_his', 'TOTALASIAN': 'total_asian', 'TOTALWHITE': 'total_white', 'TOTALAA': 'total_black', 'TOTALVAP': 'totalvappopulation', 'TOTALVAPHIS': 'hisvap', 'TOTALVAPNONHIS': 'non_hisvap', 'TOTALVAPASIAN': 'asianvap', 'TOTALVAPWHITE': 'whitevap', 'TOTALVAPAA': 'blackvap', "PRESTOT": "pres_total_votes", "PRESREP": "rep_pres_total_votes", "PRESDEM": "dem_pres_total_votes"})


# 479505 start here
# + 13 for NC



pop_id_nums = [x for x in range(480285, 480299)]
population_data["population_id"] = pop_id_nums
print(population_data)

print(plan_data.columns)


district_plan = gpd.GeoDataFrame(columns=["id", "polsby_pop_score","equal_pop_measure", "obj_funct_score", "num_opp_districts", "state_abbr", "geojson_path"], index=range(0,1))
district_plan["polsby_pop_score"] = plan_measures["Polsby"]
district_plan["geojson_path"] = "./data/georgia/plans/district-plan-ga9.geojson"
district_plan["state_abbr"] = 'GA'
district_plan["obj_funct_score"] = plan_measures["ObjectiveFunct1"]
district_plan["id"] = 'GA9'
district_plan["equal_pop_measure"] = plan_measures["PopulationEquality"]
district_plan["num_opp_districts"] = plan_measures["NumberOpportunity"]
print(district_plan)

district = plan_data[["State", "DistrictID"]]
district = district.rename(columns={"State":"state_abbr", "DistrictID":"district_num"})
district_ids = [x for x in range(1661, 1675)]
district['id'] = district_ids
district["population_data_population_id"] = pop_id_nums
district["polsby_pop_score"] = percents["Polsby"]
plan_ids = np.full(14, 'GA9')
district["district_plan_id"] = plan_ids
district["equal_pop_measure"] = 0
district["obj_funct_score"] = 0
district["geo_id"] = 0
print(district)

from sqlalchemy import create_engine
engine = create_engine('mysql+mysqlconnector://Pelicans:changeit@mysql3.cs.stonybrook.edu:3306/Pelicans', pool_pre_ping=True)


# population_data.to_sql(name="population", con=engine, if_exists='append', index=False)
# district_plan.to_sql(name="district_plan", con=engine, if_exists='append', index=False)
district.to_sql(name="district", con=engine, if_exists='append', index=False)


import geopandas as gpd
import pandas as pd
import json as json
import numpy as np
import os

census_data = gpd.read_file("./GA_CENSUS_NEW.geojson")

print(census_data.columns)


population_data = census_data[["TOTALPOP", "TOTALHIS", "TOTALNONHIS", "TOTALASIAN", "TOTALWHITE", "TOTALAA", "TOTALVAP", "TOTALVAPHIS", "TOTALVAPNONHIS", "TOTALVAPASIAN", "TOTALVAPWHITE", "TOTALVAPAA"]]

population_data = population_data.rename(columns={'TOTALPOP': 'total_population', 'TOTALHIS': 'total_his', 'TOTALNONHIS':'total_non_his', 'TOTALASIAN': 'total_asian', 'TOTALWHITE': 'total_white', 'TOTALAA': 'total_black', 'TOTALVAP': 'totalvappopulation', 'TOTALVAPHIS': 'hisvap', 'TOTALVAPNONHIS': 'non_hisvap', 'TOTALVAPASIAN': 'asianvap', 'TOTALVAPWHITE': 'whitevap', 'TOTALVAPAA': 'blackvap'})

# 236633 cb
# start at 2693
pop_id_nums = [x for x in range(2713,239347)]
population_data["population_id"] = pop_id_nums
print(population_data) 

census_blocks = census_data[['CBID', 'DISTRICTID', 'PRECINCTID']]

state_ids = np.full(236634, 'GA')
district_ids = np.full(236634, 0)
census_blocks = census_blocks.rename(columns={'CBID': 'census_block_id', 'DISTRICTID': 'district_num', 'PRECINCTID': 'precincts_precinct_id'})
census_blocks["state_abbr"] = state_ids
census_blocks["district_id"] = district_ids
#print(census_blocks['district_num'].iloc[0])
#print(type(census_blocks['district_num'].iloc[0]))
for x in range(len(census_blocks['district_num'])):
    census_blocks['district_id'].iloc[x] = pd.to_numeric(census_blocks['district_num'].iloc[x]) - 1
census_blocks["population_data_population_id"] = pop_id_nums
#print(census_blocks)
#print(census_blocks['district_id'])
#print(census_blocks['district_num'])

from sqlalchemy import create_engine
engine = create_engine('mysql+mysqlconnector://Pelicans:changeit@mysql3.cs.stonybrook.edu:3306/Pelicans', pool_pre_ping=True)
#db_connection = engine.connect()
#population_data.to_sql(name="population", con=engine, if_exists='append', index=False)
census_blocks.to_sql(name="census_block", con=engine, if_exists='append', index=False)
import mysql.connector
import json as json
import sys

mydb = mysql.connector.connect(
    host="mysql3.cs.stonybrook.edu",
    user='Pelicans',
    password='changeit',
    database='Pelicans'
)
print(mydb)
mycursor = mydb.cursor()
print(mycursor)
mycursor.execute("SHOW COLUMNS FROM population")
for x in mycursor:
    print(x)

json_path = '/Users/nicoleniemiec/Desktop/416duo/416pelicans/preprocessing/census-blocks-data/ga-census-blocks-graph-2-2.json'
print(json_path)

file = open(json_path, "r")
data = json.load(file)
print(data['nodes'][0])
print(data['nodes'][0]['id'])
print(range(len(data['nodes'])))
print(range(len(data['adjacency'])))
print(data['adjacency'][0])
print(data['adjacency'][0][0]['id'])


# precinct_precinct_neighbors
for i in range(len(data['adjacency'])):
    for j in range(len(data['adjacency'][i])):
        values = {
            'census_block_census_block_id': i,
            'block_neighbors_census_block_id': data['adjacency'][i][j]['id'],
            'state_abbr': 'GA'
        }
        sql = "INSERT INTO census_block_block_neighbors (census_block_census_block_id, block_neighbors_census_block_id, state_abbr) VALUES (%(census_block_census_block_id)s, %(block_neighbors_census_block_id)s, %(state_abbr)s)"
        mycursor.execute(sql, values)

mydb.commit()
mycursor.close()
mydb.close()
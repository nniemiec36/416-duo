import mysql.connector
import json as json
import sys

if len(sys.argv) != 2:
    print("provide the file to inset")
    exit(0)

mydb = mysql.connector.connect(
    host="mysql3.cs.stonybrook.edu",
    user='Pelicans',
    password='changeit',
    database='Pelicans'
)
print(mydb)
mycursor = mydb.cursor()
print(mycursor)
mycursor.execute("SHOW COLUMNS FROM population")
for x in mycursor:
    print(x)

json_path = sys.argv[1]
print(json_path)

file = open(json_path, "r")
data = json.load(file)

population_key = 15

for i in range(len(data['nodes'])):
    # insert into population table
    values = {
        'population_id': population_key,
        'asianvap': data['nodes'][i]['TOTALVAPASIAN'],
        'blackvap': data['nodes'][i]['TOTALVAPAA'],
        'hisvap': data['nodes'][i]['TOTALVAPHIS'],
        'non_hisvap': data['nodes'][i]['TOTALVAPNONHIS'],
        'whitevap': data['nodes'][i]['TOTALVAPWHITE'],
        'totalvappopulation': data['nodes'][i]['TOTALVAP'],
        'total_asian': data['nodes'][i]['TOTALASIAN'],
        'total_black': data['nodes'][i]['TOTALAA'],
        'total_his': data['nodes'][i]['TOTALHIS'],
        'total_non_his': data['nodes'][i]['TOTALNONHIS'],
        'total_white': data['nodes'][i]['TOTALWHITE'],
        'total_population': data['nodes'][i]['TOTALPOP'],
        'dem_pres_total_votes': data['nodes'][i]['PRESDEM'],
        'rep_pres_total_votes': data['nodes'][i]['PRESREP'],
        'pres_total_votes': data['nodes'][i]['PRESTOT']
    }
    sql = "INSERT INTO population (population_id, asianvap, blackvap, hisvap, non_hisvap, whitevap, totalvappopulation, dem_pres_total_votes, rep_pres_total_votes, pres_total_votes, total_asian, total_black, total_his, total_non_his, total_white, total_population) VALUES (%(population_id)s, %(asianvap)s, %(blackvap)s, %(hisvap)s, %(non_hisvap)s, %(whitevap)s, %(totalvappopulation)s, %(dem_pres_total_votes)s, %(rep_pres_total_votes)s, %(pres_total_votes)s, %(total_asian)s, %(total_black)s, %(total_his)s, %(total_non_his)s, %(total_white)s, %(total_population)s)"
    mycursor.execute(sql, values)
    # insert into precinct table
    values = {
        'district_id': int(data['nodes'][i]['DistrictID']) - 1,
        'district_plan_id': 'GA0', # enacted = 0
        'precinct_id': data['nodes'][i]['PrecinctID'],
        'state_abbr': 'GA',
        'population_data_population_id': population_key,
        'geo_id': data['nodes'][i]['GEOID'],
        'is_border': data['nodes'][i]['boundary_node']
    }
    sql = "INSERT INTO precinct (district_id, is_border, precinct_id, district_plan_id, state_abbr, population_data_population_id, geo_id) VALUES (%(district_id)s, %(is_border)s, %(precinct_id)s, %(district_plan_id)s, %(state_abbr)s, %(population_data_population_id)s, %(geo_id)s)"
    mycursor.execute(sql, values)

    population_key+=1

mydb.commit()
mycursor.close()
mydb.close()
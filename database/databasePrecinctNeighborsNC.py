import mysql.connector
import json as json
import sys

mydb = mysql.connector.connect(
    host="mysql3.cs.stonybrook.edu",
    user='Pelicans',
    password='changeit',
    database='Pelicans'
)
print(mydb)
mycursor = mydb.cursor()
print(mycursor)
mycursor.execute("SHOW COLUMNS FROM population")
for x in mycursor:
    print(x)

json_path = '/Users/nicoleniemiec/Desktop/416duo/416pelicans/preprocessing/GerryChain-main/nc-precincts-graph-2.json'
print(json_path)

file = open(json_path, "r")
data = json.load(file)
print(data['nodes'][0])
print(data['nodes'][0]['id'])
print(range(len(data['nodes'])))
print(range(len(data['adjacency'])))
print(data['adjacency'][0])
print(data['adjacency'][0][0]['id'])

precinct_num = 2698

# precinct_precinct_neighbors
for i in range(len(data['adjacency'])):
    for j in range(len(data['adjacency'][i])):
        values = {
            'precinct_precinct_id': precinct_num,
            'precinct_neighbors_precinct_id': int(data['adjacency'][i][j]['id']) + 2698,
            'state_abbr': 'NC',
            'district_plan_id': 'NC0'
        }
        sql = "INSERT INTO precinct_precinct_neighbors (precinct_precinct_id, precinct_neighbors_precinct_id, state_abbr, district_plan_id) VALUES (%(precinct_precinct_id)s, %(precinct_neighbors_precinct_id)s, %(state_abbr)s, %(district_plan_id)s)"
        mycursor.execute(sql, values)
    precinct_num += 1

mydb.commit()
mycursor.close()
mydb.close()
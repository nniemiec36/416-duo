import mysql.connector
import json as json
import sys

if len(sys.argv) != 2:
    print("provide the file to inset")
    exit(0)

mydb = mysql.connector.connect(
    host="mysql3.cs.stonybrook.edu",
    user='Pelicans',
    password='changeit',
    database='Pelicans'
)
print(mydb)
mycursor = mydb.cursor()
print(mycursor)
mycursor.execute("SHOW COLUMNS FROM population")
for x in mycursor:
    print(x)

json_path = sys.argv[1]
print(json_path)

file = open(json_path, "r")
data = json.load(file)
print(data['nodes'][0])
print(data['nodes'][0]['BLOCKID'])
print(range(len(data['nodes'])))
print(range(len(data['adjacency'])))
print(data['adjacency'][0])
print(data['adjacency'][0][0]['id'])

# census_block_block_neighbors
for i in range(len(data['adjacency'])):
    for j in range(len(data['adjacency'][i])):
        values = {
            'census_block_cb_id': i,
            'block_neighbors_cb_id': data['adjacency'][i][j]['id'],
            'state_abbr': 'GA',
            'district_plan_id': 0
        }
        sql = "INSERT INTO census_block_block_neighbors (census_block_cb_id, block_neighbors_cb_id, state_abbr, district_plan_id) VALUES (%(census_block_cb_id)s, %(block_neighbors_cb_id)s, %(state_abbr)s, %(district_plan_id)s)"
        mycursor.execute(sql, values)
# census_block_neighbor_districts
# this should be different
# for i in range(len(data['nodes'])):
#     values = {
#         'census_block_cb_id': i,
#         'neighbor_districts_district_num': data['nodes'][i]['DISTRICTID'],
#         'state_abbr': 'GA',
#         'district_plan_id': 0
#     }
#     sql = "INSERT INTO census_block_neighbor_districts (census_block_cb_id, neighbor_districts_district_num, state_abbr, district_plan_id) VALUES (%(census_block_cb_id)s, %(neighbor_districts_district_num)s, %(state_abbr)s, %(district_plan_id)s)"
#     mycursor.execute(sql, values)

mydb.commit()
mycursor.close()
mydb.close()
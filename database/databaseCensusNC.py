import geopandas as gpd
import pandas as pd
import json as json
import numpy as np
import os

census_data = gpd.read_file("./NC_CENSUS.geojson")
print(census_data)
print(census_data.columns)


population_data = census_data[["TOTALPOP", "TOTALHIS", "TOTALNONHIS", "TOTALASIAN", "TOTALWHITE", "TOTALAA", "TOTALVAP", "TOTALVAPHIS", "TOTALVAPNONHIS", "TOTALVAPASIAN", "TOTALVAPWHITE", "TOTALVAPAA"]]

population_data = population_data.rename(columns={'TOTALPOP': 'total_population', 'TOTALHIS': 'total_his', 'TOTALNONHIS':'total_non_his', 'TOTALASIAN': 'total_asian', 'TOTALWHITE': 'total_white', 'TOTALAA': 'total_black', 'TOTALVAP': 'totalvappopulation', 'TOTALVAPHIS': 'hisvap', 'TOTALVAPNONHIS': 'non_hisvap', 'TOTALVAPASIAN': 'asianvap', 'TOTALVAPWHITE': 'whitevap', 'TOTALVAPAA': 'blackvap'})
print(population_data)
# 236638 cb
# start at 242026
pop_id_nums = [x for x in range(242027, (242027+236638))]
population_data["population_id"] = pop_id_nums
print(population_data) 

census_blocks = census_data[['CBID', 'DISTRICTID', 'PRECINCTID']]

state_ids = np.full(236638, 'NC')
district_ids = np.full(236638, 0)
block_ids = np.full(236638, 0)
census_blocks = census_blocks.rename(columns={'CBID': 'block_id', 'DISTRICTID': 'district_num', 'PRECINCTID': 'precincts_precinct_id'})
census_blocks["state_abbr"] = state_ids
census_blocks["district_id"] = district_ids
census_blocks["census_block_id"] = block_ids
#print(census_blocks['district_num'].iloc[0])
#print(type(census_blocks['district_num'].iloc[0]))
for x in range(len(census_blocks['district_num'])):
    census_blocks['district_id'].iloc[x] = pd.to_numeric(census_blocks['district_num'].iloc[x]) + 12
    census_blocks['census_block_id'].iloc[x] = pd.to_numeric(census_blocks['block_id'].iloc[x]) + 236637
census_blocks["population_data_population_id"] = pop_id_nums
#print(census_blocks)
#print(census_blocks['district_id'])
#print(census_blocks['district_num'])

from sqlalchemy import create_engine
engine = create_engine('mysql+mysqlconnector://Pelicans:changeit@mysql3.cs.stonybrook.edu:3306/Pelicans', pool_pre_ping=True)
# #db_connection = engine.connect()
# population_data.to_sql(name="population", con=engine, if_exists='append', index=False)
census_blocks.to_sql(name="census_block", con=engine, if_exists='append', index=False)
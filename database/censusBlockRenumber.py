import json as json
import sys

if len(sys.argv) != 2:
    print("provide the path of the json file you are trying to convert.")
    exit(0)
if "graph" in sys.argv[1]:
    exit(0)

census_path = sys.argv[1]
print(census_path)

if "GA_CENSUS" in census_path:
    print("georgia true")
    file_name = "GA_CENSUS.geojson"
elif "NC_CENSUS" in census_path: 
    print("north carolina true")
    file_name = "NC_CENSUS.geojson"
else:
    file_name = "PA_CENSUS.geojson"

file1 = open(census_path, "r")

data = json.load(file1)

for i in range(len(data['features'])):
    data['features'][i]['properties']['CBID'] = str(i)

file3 = open("GA_CENSUS_NEW.geojson", "w")
json.dump(data, file3)
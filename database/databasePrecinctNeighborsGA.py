import mysql.connector
import json as json
import sys

if len(sys.argv) != 2:
    print("provide the file to inset")
    exit(0)

mydb = mysql.connector.connect(
    host="mysql3.cs.stonybrook.edu",
    user='Pelicans',
    password='changeit',
    database='Pelicans'
)
print(mydb)
mycursor = mydb.cursor()
print(mycursor)
mycursor.execute("SHOW COLUMNS FROM population")
for x in mycursor:
    print(x)

json_path = sys.argv[1]
print(json_path)

file = open(json_path, "r")
data = json.load(file)
print(data['nodes'][0])
print(data['nodes'][0]['id'])
print(range(len(data['nodes'])))
print(range(len(data['adjacency'])))
print(data['adjacency'][0])
print(data['adjacency'][0][0]['id'])


# precinct_precinct_neighbors
for i in range(len(data['adjacency'])):
    for j in range(len(data['adjacency'][i])):
        values = {
            'precinct_precinct_id': i,
            'precinct_neighbors_precinct_id': data['adjacency'][i][j]['id'],
            'state_abbr': 'GA',
            'district_plan_id': 'GA0'
        }
        sql = "INSERT INTO precinct_precinct_neighbors (precinct_precinct_id, precinct_neighbors_precinct_id, state_abbr, district_plan_id) VALUES (%(precinct_precinct_id)s, %(precinct_neighbors_precinct_id)s, %(state_abbr)s, %(district_plan_id)s)"
        mycursor.execute(sql, values)

mydb.commit()
mycursor.close()
mydb.close()
import mysql.connector
import json as json
import sys

mydb = mysql.connector.connect(
    host="mysql3.cs.stonybrook.edu",
    user='Pelicans',
    password='changeit',
    database='Pelicans'
)
print(mydb)
mycursor = mydb.cursor()
print(mycursor)
mycursor.execute("SHOW COLUMNS FROM population")
for x in mycursor:
    print(x)

json_path = '/Users/nicoleniemiec/Desktop/416duo/416pelicans/preprocessing/NC_CD_ALL_INFO.geojson'
print(json_path)

file = open(json_path, "r")
data = json.load(file)
print(data['features'][0]['properties'])
print(range(len(data['features'])))

total_pop = 0
asian_pop = 0
black_pop = 0
his_pop = 0
non_his_pop = 0
white_pop = 0
total_vap = 0
asian_vap = 0
black_vap = 0
his_vap = 0
non_hisvap = 0
white_vap = 0
dem_pres_total_votes = 0
rep_pres_total_votes = 0
pres_total_votes = 0

for i in range(len(data['features'])):
    total_pop += data['features'][i]['properties']['TOTALPOP']
    asian_pop += data['features'][i]['properties']['TOTALASIAN']
    black_pop += data['features'][i]['properties']['TOTALAA']
    his_pop += data['features'][i]['properties']['TOTALHIS']
    non_his_pop += data['features'][i]['properties']['TOTALNONHIS']
    white_pop += data['features'][i]['properties']['TOTALWHITE']
    total_vap += data['features'][i]['properties']['TOTALVAP']
    asian_vap += data['features'][i]['properties']['TOTALVAPASIAN']
    black_vap += data['features'][i]['properties']['TOTALVAPAA']
    his_vap += data['features'][i]['properties']['TOTALVAPHIS']
    non_hisvap += data['features'][i]['properties']['TOTALVAPNONHIS']
    white_vap += data['features'][i]['properties']['TOTALVAPWHITE']
    dem_pres_total_votes += data['features'][i]['properties']['PRESDEM']
    rep_pres_total_votes += data['features'][i]['properties']['PRESREP']
    pres_total_votes += data['features'][i]['properties']['PRESTOT']


population_key = 239347
district_plan_id = 0

# insert into population_data
# population_key += 1
values = {
    'population_id': population_key,
    'asianvap': asian_vap,
    'blackvap': black_vap,
    'hisvap': his_vap,
    'non_hisvap': non_hisvap,
    'whitevap': white_vap,
    'totalvappopulation': total_vap,
    'total_asian': asian_pop,
    'total_black': black_pop,
    'total_his': his_pop,
    'total_non_his': non_his_pop,
    'total_white': white_pop,
    'total_population': total_pop,
    'dem_pres_total_votes': dem_pres_total_votes,
    'rep_pres_total_votes': rep_pres_total_votes,
    'pres_total_votes': pres_total_votes
}
sql = "INSERT INTO population (population_id, asianvap, blackvap, hisvap, non_hisvap, whitevap, totalvappopulation, total_asian, total_black, total_his, total_non_his, total_white, total_population, rep_pres_total_votes, dem_pres_total_votes, pres_total_votes) VALUES (%(population_id)s, %(asianvap)s, %(blackvap)s, %(hisvap)s, %(non_hisvap)s, %(whitevap)s, %(totalvappopulation)s, %(total_asian)s, %(total_black)s, %(total_his)s, %(total_non_his)s, %(total_white)s, %(total_population)s, %(rep_pres_total_votes)s, %(dem_pres_total_votes)s, %(pres_total_votes)s)"
mycursor.execute(sql, values)

# insert into district_plan
values = {
    'id': 'NC' + str(district_plan_id), # enacted = 0
    'state_abbr': 'NC',
    'geojson_path': './data/georgia/plans/district_plan_nc' + str(district_plan_id) + '.geojson' 
}
sql = "INSERT INTO district_plan (id, state_abbr, geojson_path) VALUES (%(id)s, %(state_abbr)s, %(geojson_path)s)"
mycursor.execute(sql, values)

# insert into state
values = {
    'id': data['features'][0]['properties']['State'],
    'pop_id': population_key,
    'state_abbr': data['features'][0]['properties']['State'],
    'district_plan_id': 'NC' + str(district_plan_id)
}
sql = "INSERT INTO state (id, pop_id, state_abbr, district_plan_id) VALUES (%(id)s, %(pop_id)s, %(state_abbr)s, %(district_plan_id)s)"
mycursor.execute(sql, values)

population_key += 1
district_id = 14

for i in range(len(data['features'])):
    # insert into population table
    values = {
        'population_id': population_key,
        'asianvap': data['features'][i]['properties']['TOTALVAPASIAN'],
        'blackvap': data['features'][i]['properties']['TOTALVAPAA'],
        'hisvap': data['features'][i]['properties']['TOTALVAPHIS'],
        'non_hisvap': data['features'][i]['properties']['TOTALVAPNONHIS'],
        'whitevap': data['features'][i]['properties']['TOTALVAPWHITE'],
        'totalvappopulation': data['features'][i]['properties']['TOTALVAP'],
        'total_asian': data['features'][i]['properties']['TOTALASIAN'],
        'total_black': data['features'][i]['properties']['TOTALAA'],
        'total_his': data['features'][i]['properties']['TOTALHIS'],
        'total_non_his': data['features'][i]['properties']['TOTALNONHIS'],
        'total_white': data['features'][i]['properties']['TOTALWHITE'],
        'total_population': data['features'][i]['properties']['TOTALPOP'],
        'dem_pres_total_votes': data['features'][i]['properties']['PRESDEM'],
        'rep_pres_total_votes': data['features'][i]['properties']['PRESREP'],
        'pres_total_votes': data['features'][i]['properties']['PRESTOT']
    }
    sql = "INSERT INTO population (population_id, asianvap, blackvap, hisvap, non_hisvap, whitevap, totalvappopulation, dem_pres_total_votes, rep_pres_total_votes, pres_total_votes, total_asian, total_black, total_his, total_non_his, total_white, total_population) VALUES (%(population_id)s, %(asianvap)s, %(blackvap)s, %(hisvap)s, %(non_hisvap)s, %(whitevap)s, %(totalvappopulation)s, %(dem_pres_total_votes)s, %(rep_pres_total_votes)s, %(pres_total_votes)s, %(total_asian)s, %(total_black)s, %(total_his)s, %(total_non_his)s, %(total_white)s, %(total_population)s)"
    mycursor.execute(sql, values)
    # insert into district table
    values = {
        'district_num': data['features'][i]['properties']['DistrictID'],
        'district_plan_id': 'NC' + str(district_plan_id), # enacted = 0
        'state_abbr': data['features'][i]['properties']['State'],
        'population_data_population_id': population_key,
        'geo_id': data['features'][i]['properties']['GeoID'],
        'id': district_id
    }
    sql = "INSERT INTO district (id, district_num, district_plan_id, state_abbr, population_data_population_id, geo_id) VALUES (%(id)s, %(district_num)s, %(district_plan_id)s, %(state_abbr)s, %(population_data_population_id)s, %(geo_id)s)"
    mycursor.execute(sql, values)

    population_key += 1
    district_id += 1

mydb.commit()

mycursor.close()
mydb.close()
package pelicans.server.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pelicans.server.DistrictPlan;
import pelicans.server.mapmodel.State;

@Repository
public interface DistrictPlanRepository extends CrudRepository<DistrictPlan, String> {
        <T>T findByIdAndStateAbbr(String id, String stateAbbr, Class<T> tClass);
}



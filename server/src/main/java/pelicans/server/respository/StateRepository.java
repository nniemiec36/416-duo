package pelicans.server.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pelicans.server.mapmodel.State;

@Repository
public interface StateRepository extends CrudRepository<State, String> {
    <T>T findByStateAbbr(String stateAbbr, Class<T> tClass);
}


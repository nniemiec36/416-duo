package pelicans.server.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pelicans.server.Population;
import pelicans.server.mapmodel.State;

@Repository
public interface PopulationRepository extends CrudRepository<Population, String> {
    <T>T findByPopulationId(String populationId, Class<T> tClass);
}
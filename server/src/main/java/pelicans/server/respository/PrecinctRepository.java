package pelicans.server.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pelicans.server.mapmodel.Precinct;

@Repository
public interface PrecinctRepository extends CrudRepository<Precinct, String> {
    <T>T findByPrecinctIdAndStateAbbr(String precinctId, String stateAbbr, Class<T> tClass);
}
package pelicans.server.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pelicans.server.mapmodel.District;

@Repository
public interface DistrictRepository extends CrudRepository<District, String> {
    <T>T findByDistrictNumAndStateAbbrAndPlanId(int districtNum, String stateAbbr, String planId, Class<T> tClass);
}
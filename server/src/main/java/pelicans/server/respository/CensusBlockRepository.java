package pelicans.server.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pelicans.server.CensusBlock;

@Repository
public interface CensusBlockRepository extends CrudRepository<CensusBlock, String> {
    <T>T findByCensusBlockIdAndStateAbbr(String censusBlockId, String stateAbbr, Class<T> tClass);
}
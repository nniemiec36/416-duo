package pelicans.server.algo;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import org.locationtech.jts.geom.*;

import org.json.simple.JSONObject;
import org.locationtech.jts.geom.Polygon;
import pelicans.server.CensusBlock;
import pelicans.server.DistrictPlan;
import pelicans.server.Enum.RunningState;
import pelicans.server.Enum.States;
import pelicans.server.mapmodel.State;
import pelicans.server.Move;
import pelicans.server.Population;
import pelicans.server.mapmodel.District;

import javax.faces.FactoryFinder;

public class Algorithm{

    private State state;
    private District currentDistrict;
    private CensusBlock currentBlock;
    private int currentIteration;
    private int maxIteration;
    private Move move;
    private Move prevMove;
    private District prevDistrict;
    private CensusBlock prevBlock;
    private DistrictPlan currentPlan;
    private DistrictPlan workingPlan;
    private boolean pickRandomDistrict;
    private double populationEquality;
    private double polsbyPopper;
    private double graphCompactness;
    private RunningState runningState;
    private boolean areFutureRunsPossible;

    public Algorithm(){}
    public Algorithm(State state, District currentDistrict, CensusBlock currentBlock, int currentIteration, int maxIteration, Move move, Move prevMove, District prevDistrict, CensusBlock prevBlock, DistrictPlan currentPlan, DistrictPlan workingPlan, boolean pickRandomDistrict, double populationEquality, double polsbyPopper, double graphCompactness) {
        this.state = state;
        this.currentDistrict = currentDistrict;
        this.currentBlock = currentBlock;
        this.currentIteration = currentIteration;
        this.maxIteration = maxIteration;
        this.move = move;
        this.prevMove = prevMove;
        this.prevDistrict = prevDistrict;
        this.prevBlock = prevBlock;
        this.currentPlan = currentPlan;
        this.workingPlan = workingPlan;
        this.pickRandomDistrict = pickRandomDistrict;
        this.populationEquality = populationEquality;
        this.polsbyPopper = polsbyPopper;
        this.graphCompactness = graphCompactness;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public District getCurrentDistrict() {
        return currentDistrict;
    }

    public void setCurrentDistrict(District currentDistrict) {
        this.currentDistrict = currentDistrict;
    }

    public CensusBlock getCurrentBlock() {
        return currentBlock;
    }

    public void setCurrentBlock(CensusBlock currentBlock) {
        this.currentBlock = currentBlock;
    }

    public int getCurrentIteration() {
        return currentIteration;
    }

    public void setCurrentIteration(int currentIteration) {
        this.currentIteration = currentIteration;
    }

    public int getMaxIteration() {
        return maxIteration;
    }

    public void setMaxIteration(int maxIteration) {
        this.maxIteration = maxIteration;
    }

    public Move getMove() {
        return move;
    }

    public void setMove(Move move) {
        this.move = move;
    }

    public Move getPrevMove() {
        return prevMove;
    }

    public void setPrevMove(Move prevMove) {
        this.prevMove = prevMove;
    }

    public District getPrevDistrict() {
        return prevDistrict;
    }

    public void setPrevDistrict(District prevDistrict) {
        this.prevDistrict = prevDistrict;
    }

    public CensusBlock getPrevBlock() {
        return prevBlock;
    }

    public void setPrevBlock(CensusBlock prevBlock) {
        this.prevBlock = prevBlock;
    }

    public DistrictPlan getCurrentPlan() {
        return currentPlan;
    }

    public void setCurrentPlan(DistrictPlan currentPlan) {
        this.currentPlan = currentPlan;
    }

    public DistrictPlan getWorkingPlan() {
        return workingPlan;
    }

    public void setWorkingPlan(DistrictPlan workingPlan) {
        this.workingPlan = workingPlan;
    }

    public boolean isPickRandomDistrict() {
        return pickRandomDistrict;
    }

    public void setPickRandomDistrict(boolean pickRandomDistrict) {
        this.pickRandomDistrict = pickRandomDistrict;
    }

    public double getPopulationEquality() {
        return populationEquality;
    }

    public void setPopulationEquality(double populationEquality) {
        this.populationEquality = populationEquality;
    }

    public boolean isAreFutureRunsPossible() {
        return areFutureRunsPossible;
    }

    public void setAreFutureRunsPossible(boolean areFutureRunsPossible) {
        this.areFutureRunsPossible = areFutureRunsPossible;
    }

    public double getPolsbyPopper() {
        return polsbyPopper;
    }

    public void setPolsbyPopper(double polsbyPopper) {
        this.polsbyPopper = polsbyPopper;
    }

    public RunningState getRunningState() {
        return runningState;
    }

    public void setRunningState(RunningState runningState) {
        this.runningState = runningState;
    }

    public double getGraphCompactness() {
        return graphCompactness;
    }

    public void setGraphCompactness(double graphCompactness) {
        this.graphCompactness = graphCompactness;
    }

    public String setMeasures(String parameters){return null;}     // return type from void --> String
    public List<Polygon> getCurrentBoundaries() { return null;}

//    public boolean calculateQualityMove(Move move){
//        Measures mes = new Measures();
//        List<Double> measures = mes.calculateMeasures();
//        return false;
//    }

    public Map<String, String> getPrecinctDistrictMap(){ return null;}

    public int makeMove(Move move){
        currentIteration++;
        boolean isPossible = move.isPossible();
        if(isPossible){
            move.execute();
            return 0;
        } else if(!isPossible) {
            move.reverse();
            return 0;
        }
        return -1;
    }

    public Move testMove(District to, District from, CensusBlock b){ return null;}
    public void renameDistricts(){ }
    
    public JSONObject getCurrentBriefSummary(){
        JSONObject json = new JSONObject();
        JSONObject measures = currentPlan.calculateMeasures();
        json.put("", measures);
        return json;
    }
   
    public List<Polygon> calculateOuterBoundary(List<Polygon> censusBlockGeometries){ 
        return null;
    }
    
    public CensusBlock chooseCensusBlock() {
        return null;
    }

    public List<Population> getCurrentPopulation(){
        return null;
    }


    /* need to figure out when this is called .. only at finish ? or also periodically to make
    caulculations easier ?
     */
    public List<Geometry> calculateNewDistrictBoundaries() {
//        // maybe convert all Polygon objects to Geometry objects ... might be easier
//        List<District> distr = workingPlan.getDistricts();
//        List<Geometry> newDistrictBoundaries = new ArrayList<>();
//        for(int x=0; x < distr.size(); x++){
//            List<Polygon> currDistrictBlockBoundary = distr.get(x).getCensusBlockBoundaryData();
//            GeometryFactory geoFac = new GeometryFactory();
//            GeometryCollection newDistrictBeforeUnion = (GeometryCollection) geoFac.buildGeometry(currDistrictBlockBoundary);
//            Geometry newDistrictAfterUnion = newDistrictBeforeUnion.union();
//            Geometry newDistrictBoundary = newDistrictAfterUnion.getBoundary();
//            newDistrictBoundaries.add(newDistrictBoundary);
//        r
//        }
//
//        return newDistrictBoundaries;
//        }
//
//        return newDistrictBoundaries;
        return null;
    }
}
package pelicans.server.algo;

import org.json.simple.JSONObject;
import pelicans.server.BoxData;
import pelicans.server.CensusBlock;
import pelicans.server.DistrictPlan;
import pelicans.server.Enum.RunningState;
import pelicans.server.Enum.States;
import pelicans.server.Move;
import pelicans.server.mapmodel.District;

import org.locationtech.jts.geom.*;
import pelicans.server.mapmodel.State;

import java.util.List;
import java.util.Set;

import static pelicans.server.Enum.RunningState.PAUSED;

public class Generate{
    private State currentState; // do we need this
    private Algorithm algorithm;
    private DistrictPlan candidatePlan; // def need this
    private DistrictPlan updatedPlan; // def need this
    private Set<District> districts;
    private List<Double> userThresholds;
    private int timeOutLimit;
    private double popEqualityThres;
    private double compactnessThres;
    private int minMajDistrThres;
    private Move lastMove;
    private boolean isRandom;
    private RunningState isRunning;

    public Generate(){
        this.algorithm = new Algorithm();
        this.candidatePlan = null;
    }

    public Generate(State currentState, Algorithm algorithm, DistrictPlan candidatePlan, DistrictPlan updatedPlan, Set<District> districts, List<Double> userThresholds, Move lastMove, boolean isRandom, RunningState isRunning) {
        this.currentState = currentState;
        this.algorithm = algorithm;
        this.candidatePlan = candidatePlan;
        this.updatedPlan = updatedPlan;
        this.districts = districts;
        this.userThresholds = userThresholds;
        this.lastMove = lastMove;
        this.isRandom = isRandom;
        this.isRunning = isRunning;
    }

    public JSONObject getNewDistrictBoundaries(){
        return null;

    }

    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public DistrictPlan getCandidatePlan() {
        return candidatePlan;
    }

    public void setCandidatePlan(DistrictPlan candidatePlan) {
        this.candidatePlan = candidatePlan;
    }

    public void setUpdatedPlan(DistrictPlan updatedPlan) {
        this.updatedPlan = updatedPlan;
    }

    public Set<District> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }

    public List<Double> getUserThresholds() {
        return userThresholds;
    }

    public void setUserThresholds(List<Double> userThresholds) {
        this.userThresholds = userThresholds;
    }

    public int getTimeOutLimit() {
        return timeOutLimit;
    }

    public void setTimeOutLimit(int timeOutLimit) {
        this.timeOutLimit = timeOutLimit;
    }

    public double getPopEqualityThres() {
        return popEqualityThres;
    }

    public void setPopEqualityThres(double popEqualityThres) {
        this.popEqualityThres = popEqualityThres;
    }

    public double getCompactnessThres() {
        return compactnessThres;
    }

    public void setCompactnessThres(double compactnessThres) {
        this.compactnessThres = compactnessThres;
    }

    public int getMinMajDistrThres() {
        return minMajDistrThres;
    }

    public void setMinMajDistrThres(int minMajDistrThres) {
        this.minMajDistrThres = minMajDistrThres;
    }

    public Move getLastMove() {
        return lastMove;
    }

    public void setLastMove(Move lastMove) {
        this.lastMove = lastMove;
    }

    public boolean isRandom() {
        return isRandom;
    }

    public void setRandom(boolean random) {
        isRandom = random;
    }

    public RunningState getIsRunning() {
        return isRunning;
    }

    public void setIsRunning(RunningState isRunning) {
        this.isRunning = isRunning;
    }

    public String setAlgParams(String params){  // String to maybe JSONObject?
        /*
        GenerateController genContorl = new GenerateController(currentGen);
        var runGen = genContorl.runGenerate(id);
       boolean thresholdValid = algorithm.isUserThresholdValid();
       if(thresholdValid){
           return algorithm.setMeasures(params);
       }
       else{
           return null;
       }
        return algorithm.setMeasures(params);
        */
        return null;
    }

    public String startAlgorithm(){
        //might be needing a while loop.. check constraint = time or condition
        Algorithm alg = new Algorithm();
        // logic commented
        /*
            alg = new alg();
            deep clone the alg;

            while loop{ check conditions 
                CensusBlock cb = alg.chooseCensusBlock();
                Move test = alg.testMove(cb.getNeighborDistrict(), cb.getDistrict(), cb);
                boolean keepMove = alg.calculateQualityMove(test);
                if(keepMove){
                    int code = alg.makeMove(test);
                } else {
                    alg.chooseCensusBlock();
                }

            }
        */
//
//        CensusBlock cb = alg.chooseCensusBlock();
//        Move test = alg.testMove(cb.getNeighborDistrict(), cb.getDistrict(), cb);
//        boolean keepMove = alg.calculateQualityMove(test);
//        if(keepMove){
//            int code = alg.makeMove(test);
//        } else {
//            alg.chooseCensusBlock();
//        }
        return null;
    }
    
    public JSONObject getUpdatedBoundaries(){   // return type before was List<Polygon> --> JSONObject
        JSONObject json = new JSONObject();
        json.put("",algorithm.getCurrentBoundaries());  // key??
        return json;
    }
    
    public JSONObject stopAlgorithm(){                   // --> for gui use case 19
//        RunningState runningState = algorithm.getIsRunning();
        JSONObject jsonSummary = new JSONObject();
//        if(runningState == PAUSED){
//            algorithm.setAreFutureRunsPossible(true);
//            algorithm.setRunningState(runningState);   // pass in a runState param to this method
//            jsonSummary.put("measures summary", algorithm.getCurrentBriefSummary());
//        }
//        else{
//            algorithm.setRunningState(runningState);   // pass in a runState param to this method
//            jsonSummary.put("measures summary", algorithm.getCurrentBriefSummary());
//        }
//        List<Geometry> newDistrictBoundaries = algorithm.calculateNewDistrictBoundaries();
//        jsonSummary.put("geometry", newDistrictBoundaries);
        // need to look into how to convert Geometries to Polygons for GeoJSONS ...
        return jsonSummary;
    }
    
    public JSONObject getAlgorithmSummary(){
        return algorithm.getCurrentBriefSummary();
    }

    public boolean isPopulationImproved(){
        return false;
    }

    public BoxData generateBoxPlot(){
        return null;
    }

    public DistrictPlan getUpdatedPlan(){
        return null;
    }

    public JSONObject getUpdatedPopulations(){
        JSONObject json = new JSONObject();
        //json.put("", algorithm.getCurrentPopulations());
        return json;
    }
}
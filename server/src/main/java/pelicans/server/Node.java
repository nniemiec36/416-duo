package pelicans.server;

import java.util.Set;

public class Node<Precinct>{
    private Node<Precinct> parentNode;
    private Precinct rootNode;
    private String geoID;
    private int population;
    private Set<Node<Precinct>> childrenNode;

    public Node(pelicans.server.Node<Precinct> parentNode, Precinct rootNode, String geoID, int population, Set<Node<Precinct>> childrenNode) {
        this.parentNode = parentNode;
        this.rootNode = rootNode;
        this.geoID = geoID;
        this.population = population;
        this.childrenNode = childrenNode;
    }

    public pelicans.server.Node<Precinct> getParentNode() {
        return parentNode;
    }

    public void setParentNode(pelicans.server.Node<Precinct> parentNode) {
        this.parentNode = parentNode;
    }

    public Precinct getRootNode() {
        return rootNode;
    }

    public void setRootNode(Precinct rootNode) {
        this.rootNode = rootNode;
    }

    public String getGeoID() {
        return geoID;
    }

    public void setGeoID(String geoID) {
        this.geoID = geoID;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public Set<Node<Precinct>> getChildrenNode() {
        return childrenNode;
    }

    public void setChildrenNode(Set<Node<Precinct>> childrenNode) {
        this.childrenNode = childrenNode;
    }

    public Set<Precinct> getPrecincts(){
        return null;
    }
    public Node<Precinct> getAncestors(){
        return null;
    }
    public void addChild(Node<Precinct> child){ }
}
package pelicans.server;
import java.util.*;
import pelicans.server.Enum.Demographic;
import pelicans.server.Enum.Party;
import pelicans.server.Enum.States;
import javax.persistence.*;
import java.util.Map;
import java.util.HashMap;

@Entity
public class Population {
    @Id
    private String populationId;
    private int totalPopulation;
    private int totalVAPPopulation;
    private int totalBlack;
    private int totalWhite;
    private int totalHis;
    private int totalNonHis;
    private int totalAsian;
    private int whiteVAP;
    private int blackVAP;
    private int hisVAP;
    private int nonHisVAP;
    private int asianVAP;
    private int demPresTotalVotes;
    private int repPresTotalVotes;
    private int presTotalVotes;

    public String getPopulationId() {
        return populationId;
    }

    public void setPopulationId(String populationId) {
        this.populationId = populationId;
    }

    public int getTotalPopulation() {
        return totalPopulation;
    }

    public void setTotalPopulation(int totalPopulation) {
        this.totalPopulation = totalPopulation;
    }

    public int getTotalVAPPopulation() {
        return totalVAPPopulation;
    }

    public void setTotalVAPPopulation(int totalVAPPopulation) {
        this.totalVAPPopulation = totalVAPPopulation;
    }

    public int getTotalBlack() {
        return totalBlack;
    }

    public void setTotalBlack(int totalBlack) {
        this.totalBlack = totalBlack;
    }

    public int getTotalWhite() {
        return totalWhite;
    }

    public void setTotalWhite(int totalWhite) {
        this.totalWhite = totalWhite;
    }

    public int getTotalHis() {
        return totalHis;
    }

    public void setTotalHis(int totalHis) {
        this.totalHis = totalHis;
    }

    public int getTotalNonHis() {
        return totalNonHis;
    }

    public void setTotalNonHis(int totalNonHis) {
        this.totalNonHis = totalNonHis;
    }

    public int getTotalAsian() {
        return totalAsian;
    }

    public void setTotalAsian(int totalAsian) {
        this.totalAsian = totalAsian;
    }

    public int getWhiteVAP() {
        return whiteVAP;
    }

    public void setWhiteVAP(int whiteVAP) {
        this.whiteVAP = whiteVAP;
    }

    public int getBlackVAP() {
        return blackVAP;
    }

    public void setBlackVAP(int blackVAP) {
        this.blackVAP = blackVAP;
    }

    public int getHisVAP() {
        return hisVAP;
    }

    public void setHisVAP(int hisVAP) {
        this.hisVAP = hisVAP;
    }

    public int getNonHisVAP() {
        return nonHisVAP;
    }

    public void setNonHisVAP(int nonHisVAP) {
        this.nonHisVAP = nonHisVAP;
    }

    public int getAsianVAP() {
        return asianVAP;
    }

    public void setAsianVAP(int asianVAP) {
        this.asianVAP = asianVAP;
    }

    public int getDemPresTotalVotes() {
        return demPresTotalVotes;
    }

    public void setDemPresTotalVotes(int demPresTotalVotes) {
        this.demPresTotalVotes = demPresTotalVotes;
    }

    public int getRepPresTotalVotes() {
        return repPresTotalVotes;
    }

    public void setRepPresTotalVotes(int repPresTotalVotes) {
        this.repPresTotalVotes = repPresTotalVotes;
    }

    public int getPresTotalVotes() {
        return presTotalVotes;
    }

    public void setPresTotalVotes(int presTotalVotes) {
        this.presTotalVotes = presTotalVotes;
    }

    public double calculateRepPercent(){
        return 0.0;
    }

    public double calculateDemPercent(){
        return 0.0;
    }

    public String getHighestMinority(){
        return null;
    }

    public String formatForTable(){
        return null;
    }
}

// old code
/*
   public Double[] getElectionData() {
       return this.electionData;
   }

   public void setElectionData(Double[] electionData) {
       this.electionData = electionData;
   }

   public Double[] getElectionData() {
       return electionData;
   }

   public void setElectionData(Double[] electionData) {
       this.electionData = electionData;
   }

   public Double[] getDemographicPop() {
       return demographicPop;
   }

   public void setDemographicPop(Double[] demographicPop) {
       this.demographicPop = demographicPop;
   }
*/
package pelicans.server;

import java.util.Set;
import java.awt.*;
import java.util.List;

import org.json.simple.JSONObject;
import pelicans.server.Enum.Demographic;
import pelicans.server.Enum.Party;
import pelicans.server.Enum.States;
import pelicans.server.mapmodel.District;
import pelicans.server.mapmodel.State;

public class Move {
    private District to;
    private District from;
    private CensusBlock censusBlock;

    public Move(District to, District from, CensusBlock censusBlock) {
        this.to = to;
        this.from = from;
        this.censusBlock = censusBlock;
    }

    public District getTo() {
        return to;
    }

    public void setTo(District to) {
        this.to = to;
    }

    public District getFrom() {
        return from;
    }

    public void setFrom(District from) {
        this.from = from;
    }

    public CensusBlock getCensusBlock() {
        return censusBlock;
    }

    public void setCensusBlock(CensusBlock censusBlock) {
        this.censusBlock = censusBlock;
    }

    public boolean isPossible(){ return false;}
    public void reverse(){ }

    public void execute(){
//        this.from.removeCensusBlock(censusBlock);
//        this.to.addCensusBlock(censusBlock);
    }
}
package pelicans.server.projections;

import pelicans.server.Population;
import pelicans.server.mapmodel.State;

import java.util.List;

public interface StatePopProj {
    String getId();
    Population getPopulationData();
}

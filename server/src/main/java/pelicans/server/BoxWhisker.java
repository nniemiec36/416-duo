package pelicans.server;

import pelicans.server.Enum.BoxMeasure;

import java.awt.*;
import java.util.List;

public class BoxWhisker {
    private List<Double> enactedDistricting;
    private List<Double> selectedDistricting;
    private List<List<Polygon>> districtingsData;
    private BoxMeasure comparisonBasis;
    private BoxData[] boxDataList;

    public BoxWhisker(List<Double> enactedDistricting, List<Double> selectedDistricting, List<List<Polygon>> districtingsData, BoxMeasure comparisonBasis) {
        this.enactedDistricting = enactedDistricting;
        this.selectedDistricting = selectedDistricting;
        this.districtingsData = districtingsData;
        this.comparisonBasis = comparisonBasis;
    }

    public List<Double> getEnactedDistricting() {
        return enactedDistricting;
    }

    public void setEnactedDistricting(List<Double> enactedDistricting) {
        this.enactedDistricting = enactedDistricting;
    }

    public List<Double> getSelectedDistricting() {
        return selectedDistricting;
    }

    public void setSelectedDistricting(List<Double> selectedDistricting) {
        this.selectedDistricting = selectedDistricting;
    }

    public List<List<Polygon>> getDistrictingsData() {
        return districtingsData;
    }

    public void setDistrictingsData(List<List<Polygon>> districtingsData) {
        this.districtingsData = districtingsData;
    }

    public BoxMeasure getComparisonBasis() {
        return comparisonBasis;
    }

    public void setComparisonBasis(BoxMeasure comparisonBasis) {
        this.comparisonBasis = comparisonBasis;
    }

    public List<Double> returnAvgValues(){ return null;}
}
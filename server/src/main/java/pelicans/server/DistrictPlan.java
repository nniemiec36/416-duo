package pelicans.server;

import org.locationtech.jts.geom.*;
import java.util.List;

import org.json.simple.JSONObject;
import pelicans.server.Enum.Demographic;
import pelicans.server.Enum.Party;
import pelicans.server.Enum.States;
import pelicans.server.mapmodel.District;
import pelicans.server.mapmodel.State;

import javax.persistence.*;

@Entity
public class DistrictPlan {
    private String stateAbbr;
    @Id
    @GeneratedValue
    private String id;
    @Transient
    private List<District> districts;
    private double objFunctScore;
    private double equalPopMeasure;
    private double polsbyPopScore;
    private int numOppDistricts;

    private int repDistricts;
    private int demDistricts;
    private String geojsonPath;

    public DistrictPlan(){

    }

    public DistrictPlan(String stateAbbr, String id, List<District> districts, double objFunctScore, double equalPopMeasure, double polsbyPopScore, int numOppDistricts, String geojsonPath, int repDistricts, int demDistricts) {
        this.stateAbbr = stateAbbr;
        this.id = id;
        this.districts = districts;
        this.objFunctScore = objFunctScore;
        this.equalPopMeasure = equalPopMeasure;
        this.polsbyPopScore = polsbyPopScore;
        this.numOppDistricts = numOppDistricts;
        this.geojsonPath = geojsonPath;
        this.repDistricts = repDistricts;
        this.demDistricts = demDistricts;

    }

    public String getStateAbbr() {
        return this.stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public double getObjFunctScore() {
        return objFunctScore;
    }

    public void setObjFunctScore(double objFunctScore) {
        this.objFunctScore = objFunctScore;
    }

    public double getEqualPopMeasure() {
        return equalPopMeasure;
    }

    public void setEqualPopMeasure(double equalPopMeasure) {
        this.equalPopMeasure = equalPopMeasure;
    }

    public double getPolsbyPopScore() {
        return polsbyPopScore;
    }

    public void setPolsbyPopScore(double polsbyPopScore) {
        this.polsbyPopScore = polsbyPopScore;
    }

    public int getNumOppDistricts() {
        return numOppDistricts;
    }

    public void setNumOppDistricts(int numOppDistricts) {
        this.numOppDistricts = numOppDistricts;
    }

    public String getGeojsonPath() {
        return geojsonPath;
    }

    public void setGeojsonPath(String geojsonPath) {
        this.geojsonPath = geojsonPath;
    }


    public int getRepDistricts() {
        return repDistricts;
    }

    public void setRepDistricts(int repDistricts) {
        this.repDistricts = repDistricts;
    }

    public int getDemDistricts() {
        return demDistricts;
    }

    public void setDemDistricts(int demDistricts) {
        this.demDistricts = demDistricts;
    }

    public JSONObject generateSummary(){
        JSONObject planSummary = new JSONObject();
        planSummary.put("id", this.id);
        planSummary.put("polsby popper", this.polsbyPopScore);
        planSummary.put("population equality", this.equalPopMeasure);
        planSummary.put("obj function", this.objFunctScore);
        return planSummary;
    }

    public JSONObject constructClientJSON(){
        return null;
    }

    public JSONObject calculateMeasures(){
        //District distr = new District();
        District distr = this.districts.get(0);
        Double objScore = distr.getObjFunctScore();
        Double polsScore = distr.getPolsbyPopScore();
        JSONObject json = new JSONObject();
        json.put("Objective Score", objScore);
        json.put("Polsby Popper Score", polsScore);
        return json;
    }
    
    public District getDistrictByID(int id){
        return null;
    }

    public Population[] getUpdatedPopulations() {
        return null;
    }
}

// old code
/*
public void setDistrictingBoundaries(List<Polygon> districtingBoundaries) {
    this.districtingBoundaries = districtingBoundaries;
}
*/
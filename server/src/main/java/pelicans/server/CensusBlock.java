package pelicans.server;

import pelicans.server.mapmodel.District;
import pelicans.server.mapmodel.Precinct;
import pelicans.server.mapmodel.State;

import javax.persistence.*;
import org.locationtech.jts.geom.*;
import java.util.List;

@Entity
public class CensusBlock {
    @Id
    private String censusBlockId; // might need to rename this :(
    private String stateAbbr;
    @JoinColumn
    @ManyToOne
    private District district;
    @JoinColumn
    @ManyToOne
    private Precinct precincts;
    @ManyToOne
    private Population populationData;
    @OneToMany
    private List<CensusBlock> blockNeighbors;
    private boolean isBorder;

    public CensusBlock(String stateAbbr, District district, Precinct precincts, Population populationData, List<CensusBlock> blockNeighbors) {
        this.stateAbbr = stateAbbr;
        this.district = district;
        this.precincts = precincts;
        this.populationData = populationData;
        this.blockNeighbors = blockNeighbors;
    }

    public String getStateAbbr() {
        return this.stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public District getDistrict() {
        return this.district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Precinct getPrecincts() { return this.precincts; }

    public void setPrecincts(Precinct precincts) {
        this.precincts = precincts;
    }

    public Population getPopulationData() {
        return populationData;
    }

    public void setPopulationData(Population populationData) {
        this.populationData = populationData;
    }

    public List<CensusBlock> getBlockNeighbors() {
        return this.blockNeighbors;
    }

    public void setBlockNeighbors(List<CensusBlock> blockNeighbors) {
        this.blockNeighbors = blockNeighbors;
    }

    public String getCensusBlockId() {
        return censusBlockId;
    }

    public void setCensusBlockId(String censusBlockId) {
        this.censusBlockId = censusBlockId;
    }


    public boolean isBorder() {
        return isBorder;
    }

    public void setBorder(boolean border) {
        isBorder = border;
    }

    public District getNeighborDistrict(){
        return null;
    }

    public int hashCode(){
        return 0;
    }
}
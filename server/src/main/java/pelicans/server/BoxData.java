package pelicans.server;

import javax.persistence.*;
import java.util.List;

@Entity
public class BoxData {
    @Id
    @Column(name = "box_id", nullable = false)
    private String boxId;
    private double min;
    private double max;
    private double median;
    private double twentyFifthPerc;
    private double seventyFifthPerc;
    private String basis;
    private String stateAbbr;

    public String getBoxId() {
        return boxId;
    }

    public void setBoxId(String boxId) {
        this.boxId = boxId;
    }

    public BoxData(double min, double max, double median, double twentyFifthPerc, double seventyFifthPerc) {
        this.min = min;
        this.max = max;
        this.median = median;
        this.twentyFifthPerc = twentyFifthPerc;
        this.seventyFifthPerc = seventyFifthPerc;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMedian() {
        return median;
    }

    public void setMedian(double median) {
        this.median = median;
    }

    public double getTwentyFifthPerc() {
        return twentyFifthPerc;
    }

    public void setTwentyFifthPerc(double twentyFifthPerc) {
        this.twentyFifthPerc = twentyFifthPerc;
    }

    public double getSeventyFifthPerc() {
        return seventyFifthPerc;
    }

    public void setSeventyFifthPerc(double seventyFifthPerc) {
        this.seventyFifthPerc = seventyFifthPerc;
    }

    public List<Double> returnAvgValues() {
        return null;
    }

    @Override
    public String toString() {
        return "BoxData{" +
                "min=" + min +
                ", max=" + max +
                ", median=" + median +
                ", twentyFifth=" + twentyFifthPerc +
                ", seventyFifth=" + seventyFifthPerc +
                '}';
    }
}
package pelicans.server.Enum;

public enum RunningState {
    RUNNING,
    PAUSED,
    STOPPED
}
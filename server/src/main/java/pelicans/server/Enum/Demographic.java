package pelicans.server.Enum;

public enum Demographic {
    WHITE,
    AFRICAN_AMERICAN,
    ASIAN,
    HISPANIC_LATINO,
    AM_INDIAN_AK_NATIVE,
    NAT_HAW_OR_PAC_I
}
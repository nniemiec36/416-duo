package pelicans.server.api;

import org.json.simple.JSONObject;
import org.json.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pelicans.server.CensusBlock;
import pelicans.server.DistrictPlan;
import pelicans.server.Enum.BoxMeasure;
import pelicans.server.Enum.PopMeasure;
import pelicans.server.Enum.RunningState;
import pelicans.server.Population;
import pelicans.server.algo.Algorithm;
import pelicans.server.algo.Generate;
import pelicans.server.mapmodel.District;
import pelicans.server.mapmodel.Precinct;
import pelicans.server.mapmodel.State;
import pelicans.server.projections.StatePopProj;
import pelicans.server.projections.StateProj;
import pelicans.server.respository.*;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;


@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/states")
public class StateController {

    @Autowired
    private StateRepository stateRepo;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private DistrictPlanRepository districtPlanRepository;

    @Autowired
    private PrecinctRepository precinctRepository;

    @Autowired
    private CensusBlockRepository censusBlockRepository;

    @Autowired
    private PopulationRepository populationRepository;

    @Autowired
    private BoxDataRepository boxDataRepository;
    // add a repository for each persistence object in the db
    // so sort of like each table ?????

    private Generate currentGen;
    private State currentState;
    private District distr;
    private Algorithm algo;
    private PopMeasure popVar;


    /* Client Controller Methods */

    public void refresh(){}

    public void changeState(String s){}

    public JSONObject getStateBoundaries(String state){return null;}

    public JSONObject getTableData(){
        return null;
    }

    public String getAlgoParameters(String s){
        return null;
    }

    @PostMapping("/populationVariable/set")
    public void setPopVariable(@Validated @RequestBody JSONObject pop){
        System.out.println(pop.toString());
        PopMeasure var;
        String[] temp = pop.toJSONString().split("\\:");
        String val = temp[1];
        val = val.substring(1, val.length()-2);
        System.out.println(val);

        // String popVar = pop.get("title");
        // pop.getJSONObject("title");
        // System.out.println("popVar: " + popVar);

        if(val.equals("total")){
            var = PopMeasure.TOTAL;
            System.out.println("TRUE!!");
        }
        else{
            var = PopMeasure.VAP;
        }
        System.out.println("pop....: " + pop);
        System.out.println("VAR is:");
        this.popVar = var;
        // setPopVariable(var);
    }

    @GetMapping("/seawulfPlans/{id}")
    @ResponseBody
    public JSONObject getPlanData(@PathVariable("id") String id){
        return currentState.getPlan(id);
    }
    @GetMapping("/minMajStatus/")
    @ResponseBody
    public JSONObject getMinMajStats(@PathVariable() DistrictPlan currentPlan){
        JSONObject json = new JSONObject();
        Population[] pop = currentPlan.getUpdatedPopulations();
        json.put("TOTAL",pop[0]);
        json.put("VAP",pop[1]);
        return json;
    }

    @GetMapping("/comparePlans/{id}")
    @ResponseBody
    public JSONObject comparePlans(@PathVariable("id")String id){
        JSONObject json = new JSONObject();
        int planId = Integer.parseInt(id);
        json.put(currentState.getCurrentAndUpdated(planId), currentState.getCurrentAndUpdated(planId)); // getkey --> 0th index, getvalue --> 1st index
        // getKey() and getval() ??
        return json;
    }

    @GetMapping("/boxAndWhisker/{plan}/{comparisonBasis}")
    @ResponseBody
    public JSONObject getBoxAndWhiskerPlot(@PathVariable() List<DistrictPlan> plans, BoxMeasure comparisonVar){
        return currentState.generateBoxAndWhiskerPlot(plans, comparisonVar);
    }

    @GetMapping("/stateBoundariess/{state}")
    @ResponseBody
    public String getState(){
        // entity manager? --> gui 2
        return null;
    }

    @GetMapping("/redistricting/{planID}")
    @ResponseBody
    public JSONObject getRedistricting(@PathVariable() int id ){
        return null;
    }

//    @GetMapping("/redistricting/{planID}/{distrID}")
//    @ResponseBody
//    public String getDistInRedist(@PathVariable() int number, int id){
//        DistrictPlan redistricting = currentState.getRedistrictingByNumber(number);
//        District dist = currentPlan.getDistrictByID(id);
//        JSONObject json = district.districtSummary();
//        return json.toString();
//    }

    // !!!!!!!!!!!! FIX THIS ^

    /* Generate Controller Methods */
    public boolean isUserThresholdValid(){
        JSONObject summary = algo.getCurrentBriefSummary();
        if(algo.getCurrentIteration() > algo.getMaxIteration()) {
            // && algo.calculateQualityMove() == false){
            return false;
        }
        return true;
    }

    public List<DistrictPlan> getComparedPlans(){
        return null;
    }

    public JSONObject getUpdatedPopulations(){
        return null;
    }

    @PostMapping("/state/{id}")
    public JSONObject runGenerate(@PathVariable("id") int id){

        //create a new generate object
        this.currentGen = new Generate();
        currentGen.setIsRunning(RunningState.RUNNING);
        currentGen.startAlgorithm();
        return null;
    }

    @GetMapping("/getProgress/")
    @ResponseBody
    public JSONObject getAlgProgress(){
        return currentGen.getAlgorithmSummary();
    }

    @GetMapping("/getGraphicProgress")
    @ResponseBody
    public JSONObject getAlgGraphicProgress(){
        JSONObject json = new JSONObject();
        json.put("Population", currentGen.getUpdatedPopulations());
        return json;
    }

    @PostMapping("/{userThresholds}")
    public String setAlgParameters(@PathVariable("userThresholds") String params){
        return currentGen.setAlgParams(params);
    }

    @GetMapping("/checkParameters/{timeLimit}/{popEquality}/{compactness}/{minMaj}")
    @ResponseBody
    public String checkParameters(@PathVariable("timeLimit") int timeLimitSecs,
                                  @PathVariable("popEquality") double popEquality,
                                  @PathVariable("compactness") double compactness,
                                  @PathVariable("minMaj") int minMajDists) throws Exception{
        System.out.println("timeLimit: " + timeLimitSecs);
        System.out.println("popEquality: " + popEquality);
        System.out.println("compactness: " + compactness);
        System.out.println("minMaj: " + minMajDists);
        if(!validateLimits(timeLimitSecs, popEquality, compactness, minMajDists))
            throw new Exception("error");
        else {
            currentGen.setCompactnessThres(compactness);
            currentGen.setMinMajDistrThres(minMajDists);
            currentGen.setTimeOutLimit(timeLimitSecs);
            currentGen.setPopEqualityThres(popEquality);
        }
        return "welp it worked";
    }


    @GetMapping("/getResults")
    @ResponseBody
    public JSONObject getResults(){
        JSONObject json = new JSONObject();
        //DistrictPlan plan = currentState.getUpdatedDistrictPlan();
        Algorithm alg = currentGen.getAlgorithm();
        JSONObject summary = distr.generateSummary();   // for server 2
        //json.put("Updated District Plan", plan);
        json.put("Algorithm", alg);
        json.put("Summary", summary);
        return json;
    }
    @GetMapping("/stop")
    public JSONObject stopGenerate(){
        return currentGen.stopAlgorithm();
    }

    @PostMapping("/sendTest")
    public String sendTest(@Validated @RequestBody String ex){
        System.out.println(ex);
        return "This is a post test!";
    }

    @GetMapping("/test")
    @ResponseBody
    public String testing(){
        System.out.println("this is a test!!");
        return "this is a test!!";
    }

    @PostMapping("/putObjTest/{var}")
    public @ResponseBody void testSetObject(@PathVariable("var") String var){
        System.out.println("var");
        this.currentGen = new Generate();
        System.out.println("Set currentGen to: " + this.currentGen);
    }

    @GetMapping("/getObjTest")
    @ResponseBody
    public String getSetObject(){
        System.out.println("Get currentGen value: " + this.currentGen);
        return "success";
    }

    public boolean validateLimits(int timeSeconds, double popEquality, double compactness, double minMaj){
        if(timeSeconds <= 0)
            return false;
        if(popEquality < 0 || popEquality > 1.0)
            return false;
        if(compactness < 0 || compactness > 1.0)
            return false;

        // int distNum = currentState.getNumDistricts()
        // gonna hard code it to 12 for now
        if(minMaj > 12)
            return false;

        return true;
    }

    /* State Controller Methods */

    @GetMapping("/getPopData/{stateAbbr}")
    @Produces(MediaType.APPLICATION_JSON)
    public @ResponseBody StateProj getStatePopulationByStateAbbr(@PathVariable("stateAbbr") String stateAbbr){
        System.out.println(stateRepo.findByStateAbbr(stateAbbr, StatePopProj.class));
        return stateRepo.findByStateAbbr(stateAbbr, StateProj.class);
    }

    @GetMapping("/getDistTableTest/{state}/{planId}")
    @Produces(MediaType.APPLICATION_JSON)
    public @ResponseBody JSONObject getDistrictTableData(@PathVariable("state") String state,
                                                   @PathVariable("planId") String id) throws IOException, ParseException {
        JSONObject jsonObject = new JSONObject();
        System.out.println(state+" "+id);
        if(state.equals("GA")){
            for(int i = 1; i <= 14; i++){
                District district = districtRepository.findByDistrictNumAndStateAbbrAndPlanId(i, state, id, District.class);
                System.out.println(district);
                jsonObject.put(Integer.toString(i), district);
            }
        }else{
            for(int i = 1; i <= 13; i++){
                District district = districtRepository.findByDistrictNumAndStateAbbrAndPlanId(i, state, id, District.class);
                System.out.println(district);
                jsonObject.put(Integer.toString(i), district);
            }
        }
        return jsonObject;

//        return districtPlanRepository.findByIdAndStateAbbr(id, state, DistrictPlan.class);
    }

    @GetMapping("/getAlgorithmProgress/{State}/{round}")
    @ResponseBody
    public JSONObject getAlgorithmProgress(@PathVariable("State") String state, @PathVariable("round") int round){
//       String path = "/Users/nicoleniemiec/desktop/416/416pelicans/server/src/main/data/";
        String path = "/Users/Alexa/Desktop/CSE Work/CSE 416/416pelicans/server/src/main/data/";
        //String path = System.getProperty("java.class.path").split("server")[0] + "server/src/main/data/";
        int numDists = 0;
        if(state.equals("ga")){
            numDists=14;
        }else if (state.equals("pa")){
            numDists = 18;
        }else { //NC
            numDists = 13;
        }
        int[] popArr = new int[numDists];
        int Min = 50000;
        int Max = 700000;
        Min = Min + 5000 * round;
        Max = Max - 5000 * round;
        System.out.println(Min);
        System.out.println(Max);
        System.out.println(round);
        Random r = new Random();
        for(int i =0;i<numDists;i++){
            System.out.println(Max);
//            System.out.println(Max-Min);
            popArr[i] = r.nextInt(Max-Min)+Min;
        }
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("currentPopulations", popArr);
        System.out.println(jsonObj.toString());

        return jsonObj;
    }

    @GetMapping("/getTableInfo/{State}/{pop}")
    @ResponseBody
    public String getTableInfo(@PathVariable("State") String state,
                                     @PathVariable("pop") String pop){
//       String path = "/Users/nicoleniemiec/desktop/416/416pelicans/server/src/main/data/";
        String path = "/Users/Alexa/Desktop/CSE Work/CSE 416/416pelicans/server/src/main/data/";
        //String path = System.getProperty("java.class.path").split("server")[0] + "server/src/main/data/";
        System.out.println(state);
        path += state + "-state-table-content-" + pop + ".json";
        try {
            path = new String(Files.readAllBytes(Paths.get(path)));
            System.out.println(path);
        } catch (IOException ex){
            ex.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "error reading file", ex);
        }
        return path;
    }

    @GetMapping("getDistrictPlanForDisplay/{state}/{id}")
    @ResponseBody
    public JSONObject getDistrictPlanTable(@PathVariable("state") String state,@PathVariable("id") String id) throws IOException, ParseException {
        JSONObject plans = new JSONObject();
        System.out.println(state +" "+id);
        DistrictPlan plan = districtPlanRepository.findByIdAndStateAbbr(id, state, DistrictPlan.class);
        System.out.println(plan);
        plans.put(id, plan);
        JSONParser parser = new JSONParser();
        String path = plan.getGeojsonPath();
        path = path.substring(1);
        path = "../client/src"+path;
        System.out.println(path);
        JSONObject file = (JSONObject) parser.parse(new FileReader(path));
        plans.put("geojson", file);
        return plans;
    }

    @GetMapping("getDistrictPlansForTable/{state}")
    @ResponseBody
    public JSONObject getDistrictPlanTable(@PathVariable("state") String state){
        JSONObject plans = new JSONObject();
        for(int i = 1; i <= 30; i++){
            DistrictPlan plan = districtPlanRepository.findByIdAndStateAbbr(state+Integer.toString(i), state, DistrictPlan.class);
            plans.put(i, plan);
        }
        return plans;
    }

    @GetMapping("getDistrictTableInfo/{state}/{populationVar}")
    @ResponseBody
    public String getDistrictTableInfo(@PathVariable("state") String state,
                                       @PathVariable("populationVar") String pop){
        //String path = "/Users/Alexa/Desktop/CSE Work/CSE 416/416pelicans/server/src/main/data/";
        String path = "/Users/nicoleniemiec/desktop/416/416pelicans/server/src/main/data/";
        //String path = "/Users/pasar/416pelicans/server/src/main/data/";
        //String path = System.getProperty("java.class.path").split("server")[0] + "server/src/main/data/";
        //States s = getState();
        System.out.println(state);
        System.out.println(pop);
        path += state + "-cd-" + pop + "-pop.json";
        try {
            path = new String(Files.readAllBytes(Paths.get(path)));
            System.out.println(path);
        } catch (IOException ex){
            ex.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "error reading file", ex);
        }
        return path;
    }

    @GetMapping("getState/{state}")
    @ResponseBody
    public JSONObject getState(@PathVariable("state") String state){

        JSONObject stateGeometries = new JSONObject();
        String path = System.getProperty("java.class.path").split("server")[0] + "server/src/main/data/";
        //String countiesPath = path + state + "-counties.geojson";
        String enactedPath = "./server/src/main/data/" + state + "-enacted.geojson";
        String precinctsPath = "./server/src/main/data/" + state + "-precincts.geojson";
        //stateGeometries.put("counties", countiesPath);
        stateGeometries.put("enactedDistrictPlan", enactedPath);
        stateGeometries.put("precincts", precinctsPath);
        //we need geometries and table data
        return stateGeometries;
    }
}

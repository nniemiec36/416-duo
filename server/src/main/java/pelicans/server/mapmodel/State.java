package pelicans.server.mapmodel;

import pelicans.server.*;
import pelicans.server.Enum.*;
import java.util.List;

//import javafx.util.Pair;
import org.json.simple.JSONObject;
import pelicans.server.BoxData;
import pelicans.server.DistrictPlan;
import pelicans.server.Enum.BoxMeasure;
import pelicans.server.Enum.PopMeasure;
import pelicans.server.Enum.States;
import pelicans.server.Population;

import javax.persistence.*;
import org.locationtech.jts.geom.*;

@Entity
public class State {
    private String stateAbbr;
    @Id
    @GeneratedValue
    private String id;
    @ManyToOne
    @JoinColumn(name="district_plan_id")
    private DistrictPlan enactedDistrictPlan;
    @ManyToOne
    @JoinColumn(name="pop_id")
    private Population populationData;
    @Transient
    private PopMeasure popVariable;
    @OneToMany
    @JoinColumns({
            @JoinColumn(name="stateAbbr", referencedColumnName="stateAbbr")
    })
    private List<DistrictPlan> seaWulfPlans;

    public State(String stateAbbr){
        this.stateAbbr = stateAbbr;
    }

    public State(String stateAbbr, String id, DistrictPlan enactedDistrictPlan, Population populationData, PopMeasure popVariable, Polygon boundaryData, List<DistrictPlan> seaWulfPlans) {
        this.stateAbbr = stateAbbr;
        this.id = id;
        this.enactedDistrictPlan = enactedDistrictPlan;
        this.populationData = populationData;
        this.popVariable = popVariable;
        this.seaWulfPlans = seaWulfPlans;
    }

    public String getStateAbbr() {
        return this.stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public DistrictPlan getEnactedDistrictPlan() {
        return enactedDistrictPlan;
    }

    public void setEnactedDistrictPlan(DistrictPlan enactedDistrictPlan) {
        this.enactedDistrictPlan = enactedDistrictPlan;
    }

    public Population getPopulationData() {
        String pop = populationData.formatForTable();       // returns a string, but return type for this method is Population
        return populationData;
    }

    public void setPopulationData(Population populationData) {
        this.populationData = populationData;
    }

    public PopMeasure getPopVariable() {
        return popVariable;
    }

    public void setPopVariable(PopMeasure popVariable) {
        this.popVariable = popVariable;
    }

    public List<DistrictPlan> getSeaWulfPlans() {
        return seaWulfPlans;
    }

    public void setSeaWulfPlans(List<DistrictPlan> seaWulfPlans) {
        this.seaWulfPlans = seaWulfPlans;
    }

    public District getDistrictByID(int id){
        return null;
    }

    public String getDistrictingSummary(int id){
        return null;
    }

    public JSONObject getPlan(String id){
        int planID = Integer.parseInt(id);
        DistrictPlan plan = seaWulfPlans.get(planID);
        return plan.generateSummary();
    }

    public void addToBoxDataArray(BoxData data){

    }

    public DistrictPlan getRedistrictingByNumber(int number){
        return null;
    }

    public List<Polygon> getDistrictBoundaries(){
        return null;
    }

    public List<List<Polygon>> getAllBoundaries(){
        return null;
    }

    public JSONObject generateBoxAndWhiskerPlot(List<DistrictPlan> plan, BoxMeasure compVal){
        // Enity manager from GUI 10 --> box and whisker
        return null;
    }

    public JSONObject getCurrentAndUpdated(int id){
        return null;
    }
}

// old code
/*
   public Pair<DistrictPlan, DistrictPlan> getCurrentAndUpdated(int id){
       return null;
   }
*/
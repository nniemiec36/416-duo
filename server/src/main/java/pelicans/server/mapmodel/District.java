package pelicans.server.mapmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.json.simple.JSONObject;
import pelicans.server.CensusBlock;
import pelicans.server.DistrictPlan;
import pelicans.server.Enum.States;
import pelicans.server.Population;

import javax.persistence.*;
import org.locationtech.jts.geom.*;
import java.util.List;
import java.util.Set;

@Entity
public class District{
    private String stateAbbr;
    @Id
    private int id;
    private int districtNum;
    private String geoId;
    @ManyToOne
    @JoinColumn(name="district_plan_id")
    private DistrictPlan districtPlan;
    private String planId;
    @ManyToOne
    @JoinColumn
    private Population populationData;
    @Transient
    private List<Precinct> precincts;
    //private Polygon boundaryData;
    private double polsbyPopScore;
    private double objFunctScore;
    private double equalPopMeasure;

    public District(){

    }

    public District(String stateAbbr, int districtNum, String geoId, DistrictPlan districtPlan, Population populationData, Set<String> countyNames, List<Precinct> precincts, List<District> districtNeighbors) {
        this.stateAbbr = stateAbbr;
        this.districtNum = districtNum;
        this.geoId = geoId;
        this.districtPlan = districtPlan;
        this.populationData = populationData;
        this.precincts = precincts;
        //this.boundaryData = boundaryData;
    }

    public String getStateAbbr() {
        return this.stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

//    public int getCounties() {
//        return counties;
//    }
//
//    public void setCounties(int counties) {
//        this.counties = counties;
//    }

    public int getDistrictNum() {
        return districtNum;
    }

    public void setDistrictNum(int districtNum) {
        this.districtNum = districtNum;
    }

    public String getGeoId() {
        return geoId;
    }

    public void setGeoId(String geoId) {
        this.geoId = geoId;
    }

    public DistrictPlan getDistrictPlan() {
        return districtPlan;
    }

    public void setDistrictPlanId(DistrictPlan districtPlan) {
        this.districtPlan = districtPlan;
    }

    public Population getPopulationData() {
        return populationData;
    }

    public void setPopulationData(Population populationData) {
        this.populationData = populationData;
    }

    public List<Precinct> getPrecincts() {
        return precincts;
    }

    public void setPrecincts(List<Precinct> precincts) {
        this.precincts = precincts;
    }

//    public Polygon getBoundaryData() {
//        return boundaryData;
//    }
//
//    public void setBoundaryData(Polygon boundaryData) {
//        this.boundaryData = boundaryData;
//    }


    public boolean isOpportunityDistrict(Population populationData){
        return false;
    }

    public double getOpportunityPercentage(Population populationData){
        return 0.0;
    }

    public int compareTo(District d){
        return 0;
    }

    public JSONObject districtSummary(){
        return null;
    }

    public List<Polygon> getPrecinctBoundaries(){
        return null;
    }

    public CensusBlock getCensusBlock(){
        return null;
    }

    public JSONObject generateSummary() {
        return null;
    }

    public double getPolsbyPopScore() {
        return this.polsbyPopScore;
    }

    public void setPolsbyPopScore(double polsbyPopScore) {
        this.polsbyPopScore = polsbyPopScore;
    }

    public double getEqualPopMeasure() {
        return equalPopMeasure;
    }

    public void setEqualPopMeasure(double equalPopMeasure) {
        this.equalPopMeasure = equalPopMeasure;
    }
    
    public double getObjFunctScore() {
        return objFunctScore;
    }

    public void setObjFunctScore(double objFunctScore) {
        this.objFunctScore = objFunctScore;
    }
}
package pelicans.server.mapmodel;

import pelicans.server.CensusBlock;
import pelicans.server.DistrictPlan;
import pelicans.server.Population;

import javax.persistence.*;
import org.locationtech.jts.geom.*;
import java.util.List;

@Entity
public class Precinct {
    @Id
    @GeneratedValue
    private String precinctId;
    private String geoId;
    private String stateAbbr;
    @ManyToOne
    @JoinColumn
    private Population populationData;
//    @OneToMany
//    //@JoinColumn(name = "census_block_id")
//    private List<CensusBlock> censusBlocks;
    // private Polygon boundaryData;
    @ManyToOne
    @JoinColumn(name="district_id")
    private District district;
    @OneToMany
    private List<Precinct> precinctNeighbors;
    private boolean isBorder;
    @ManyToOne
    @JoinColumn(name="district_plan_id")
    private DistrictPlan districtPlan;

    public Precinct() {

    }

    public Precinct(String precinctId, String geoId, String stateAbbr, Population populationData, District district, boolean isBorder) {
        this.precinctId= precinctId;
        this.geoId = geoId;
        this.stateAbbr = stateAbbr;
        this.populationData = populationData;
//        this.censusBlocks = censusBlocks;
//        this.boundaryData = boundaryData;
        this.district = district;
        this.isBorder = isBorder;
    }

    public String getPrecinctId() {
        return precinctId;
    }

    public void setPrecinctId(String precinctId) {
        this.precinctId = precinctId;
    }

    public String getGeoId() {
        return geoId;
    }

    public void setGeoId(String geoId) {
        this.geoId = geoId;
    }

    public String getStateAbbr() {
        return this.stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public Population getPopulationData() {
        return populationData;
    }

    public void setPopulationData(Population populationData) {
        this.populationData = populationData;
    }

//    public Polygon getBoundaryData() {
//        return boundaryData;
//    }
//
//    public void setBoundaryData(Polygon boundaryData) {
//        this.boundaryData = boundaryData;
//    }

    public District getDistrict() {
        return this.district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String toString(){return null;}
}

//package pelicans.server;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//import pelicans.server.mapmodel.State;
//import pelicans.server.projections.StateProj;
//import pelicans.server.respository.DistrictPlanRepository;
//import pelicans.server.respository.StateRepository;
//
//@Component
//public class DatabaseLoader implements CommandLineRunner {
//
//    private final DistrictPlanRepository districtPlanRepo;
//    private final StateRepository stateRepo;
//
//    //@Autowired
//    public DatabaseLoader(DistrictPlanRepository dRep, StateRepository sRep){
//        this.districtPlanRepo = dRep;
//        this.stateRepo = sRep;
//    }
//
//    @Override
//    public void run(String... strings) throws Exception { // runs on app starting
//        State ga = new State("GA");
//        this.stateRepo.save(ga);
//
//        StateProj gaNew = this.stateRepo.findFirstByStateAbbr("ga");
//        System.out.println(ga);
//        System.out.println(gaNew);
//
//
//    }
//
//}

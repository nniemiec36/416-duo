import geopandas as gpd
import sys
import json as json

if len(sys.argv) != 2:
    print("provide the path of the json file you are trying to convert.")
    exit(0)
if "graph" in sys.argv[1]:
    exit(0)

geojson_path = sys.argv[1]

total_vap = 0
total_pop = 0
total_white = 0
total_his = 0
total_non_his = 0
total_asian = 0
total_aa = 0

# with open(geojson_path, "r") as file:
#     # print(file.read())
#     data = json.loads(file.read())
#     print(data)
#     print(data['1'].keys())
#     keys = list(data.keys())
#     print(keys)
#     for i in range(len(data)):
#         total_vap += data[keys[i]]['TOTALVAP']
#         total_white += data[keys[i]]['TOTALVAPWHITE']
#         total_his += data[keys[i]]['TOTALVAPHIS']
#         total_non_his += data[keys[i]]['TOTALVAPNONHIS']
#         total_asian += data[keys[i]]['TOTALVAPASIAN']
#         total_aa += data[keys[i]]['TOTALVAPAA']
# print(total_vap)
# print(total_his)
# print(total_non_his)
# print(total_white)
# print(total_asian)
# print(total_aa)

with open(geojson_path, "r") as file:
    # print(file.read())
    data = json.loads(file.read())
    print(data)
    print(data['1'].keys())
    keys = list(data.keys())
    print(keys)
    for i in range(len(data)):
        total_pop += data[keys[i]]['TOTALPOP']
        total_white += data[keys[i]]['TOTALWHITE']
        total_his += data[keys[i]]['TOTALHIS']
        total_non_his += data[keys[i]]['TOTALNONHIS']
        total_asian += data[keys[i]]['TOTALASIAN']
        total_aa += data[keys[i]]['TOTALAA']
print(total_pop)
print(total_his)
print(total_non_his)
print(total_white)
print(total_asian)
print(total_aa)